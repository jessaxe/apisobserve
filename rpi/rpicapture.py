#!/usr/bin/env python

import datetime
import numpy as np
import multiprocessing as mp
from picamera import PiCamera
from picamera.array import PiRGBAnalysis


class Consumer():
    '''
    Class for processing frames in child processes
    '''
    def __init__(self, queue, process_fn):
        '''
        Args:
            q -  mp.JoinableQueue instance
            process_fn - function to apply to each frame. Should accept two
                parameters:
                    timestamp - datetime.datetime object
                    frame - numpy ndarray (image to process)
        '''
        self.queue = queue
        self.process_fn = process_fn
        self.poison_pill = 'PP'
        self.run()

    def run(self):
        while True:
            msg = self.queue.get()
            if msg == self.poison_pill:
                self.queue.task_done()
                break
            else:
                self.process_fn(msg)
                self.queue.task_done()

        return None


class FrameDistributor(PiRGBAnalysis):
    '''
    Provides rapid BGR capture and distributes frames for processing by
    multiple sub-processes
    '''
    def __init__(self, *args, process_fn=process_frame, processes=4, **kwargs):
        super().__init__(*args, **kwargs)
        '''
        Args:
            process_fn - passed to Consumer()
            processes - number of child processes to spawn (defaults to 4)
        '''
        self.queue = mp.JoinableQueue()
        self.poison_pill = 'PP'
        self.processes = [mp.Process(
            target=Consumer, args=(self.queue, process_fn)) for i in range(
                processes)]
        for process in self.processes:
            process.start()

    def __exit__(self, type, value, traceback):
        for process in self.processes:
            self.queue.put(self.poison_pill)

        self.queue.join()
        for process in self.processes:
            process.terminate()

        super().__exit__(self, type, value, traceback)

    def analyze(frame):
        '''
        Called every time a frame is read. Puts a message on self.queue which
        is a tuple (timestamp, frame), where timestamp is the current time as
        a datetime.datetime object.
        '''
        timestamp = datetime.datetime.now()
        # We should probably do a conversion to greyscale here to save memory.
        # e.g. frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # or just select a single colour channel: frame = frame[..., 0]
        self.queue.put(timestamp, frame)


def process_frame(timestamp, frame):
    '''
    Processes a single frame. (Placeholder -- put frame processing procedure
    in this function)
    Args:
        timestamp - datetime.datetime object
        frame - numpy ndarray (image to process)
    '''
    pass


def main():
    with PiCamera() as camera, FrameDistributor() as frame_distributor:
        camera.resolution = (1640, 1232)
        camera.framerate = 5
        camera.start_recording(frame_distributor, format='bgr')
        camera.wait_recording(30)  # Determines number of seconds to record for

if __name__ == '__main__':
    main()
