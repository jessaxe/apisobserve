#!/usr/bin/env python

from setuptools import setup
from Cython.Build import cythonize

setup(name='apisobserve',
      version='0.1',
      description='',
      url='https://jessaxe@bitbucket.org/jessaxe/apisobserve.git',
      author='Jesse Rudolf Wallace',
      author_email='jesse.rudolf.wallace@gmail.com',
      packages=['apisobserve'],
      ext_modules=cythonize('apisobserve/tracklet_distance.pyx'),
      zip_safe=False)
