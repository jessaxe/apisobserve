# Replaces binnedcap and drawtracks (which depend on OpenCV). Depends on
# imageio with ffmpeg plugin.

import cairo
import imageio
import numpy as np

import collections
import datetime as dt
from pathlib import Path


def idx_from_datetime(datetime, starttime, fps=5):
    '''
    Calculates the frame index based on datetime, starttime and the fps
    Args:
        datetime - time of desired index (dt.datetime)
        starttime - time of start of videos (dt.datetime)
        fps - framerate of the underlying data (frames per second)
    '''
    delta = datetime - starttime
    idx = int(delta.total_seconds() * fps)

    return idx


def read_logfile(log_file, video_directory=None):
    '''
    Reads filenames and frame numbers from a log file created with a
    MultiVideoReader instance.
    Args:
        log_file - output log file of MultiVideoCapture instance containing
            video file paths and number of frames
        video_directory - optional argument to change the directory where video
            files are located. (By default, use entire path from log_file)

    Returns:
        filenames - paths to video files
        frame numbers - number of frames in each video
    '''
    with open(log_file, 'r') as f:
        f.readline()
        filenames, frame_numbers = zip(
            *(l.rstrip('frames\n').split('\t') for l in f.readlines()))

    if video_directory is not None:
        filenames = list(filenames)
        for i in range(len(filenames)):
            filenames[i] = str(Path(video_directory) / Path(filenames[i]).name)

    return filenames, frame_numbers


class MultiVideoReader():
    '''
    Encapsulates imageio.Reader to handle multiple sequential video files
    '''
    def __init__(self, *filenames, file_extension='.h264', log_file=None):
        '''
        Args:
            filenames - Paths to video files (sequential order). Videos are
                assumed to follow on from eachother without a gap.
            file_extension - all files in filenames must have this extension
            log_file - optional path to a output log file
        '''
        self.filenames = filenames
        self.file_extension = file_extension
        self.log_file = log_file
        if self.log_file is not None:
            with open(self.log_file, 'w') as log_file:
                log_file.write(dt.datetime.now().strftime(
                    'Started log at %Y-%m-%d %H:%M:%S\n'))
        self._check_filenames()
        self.current_file = self.filenames[0]
        self.reader = imageio.get_reader(self.current_file, format='ffmpeg')
        self.file_idx = 0
        self.tot_fr_idx = 0  # 1-starting index -added to when first frame read
        self.file_fr_idx = 0  # 1-starting index
        self.width, self.height = self.reader.get_meta_data()['size']

    def _check_filenames(self):
        '''
        Check if each filename in self.filenames a real path to an extant file.
        Also check that each file has the correct extension
        (as per self.file_extension)
        Raises:
            FileNotFoundError - if a file doesn't exist
            ValueError - if a file has the wrong extension
        '''
        for filename in self.filenames:
            if Path(filename).is_file():
                if filename[-len(self.file_extension):] == self.file_extension:
                    continue
                else:
                    raise ValueError(
                        "%s does not match the file extension '%s'" % (
                            filename, self.file_extension))
            else:
                raise FileNotFoundError('%s does not exist.' % filename)

        return None

    def _check_dimensions(self):
        '''
        Check that the dimensions of the current video match the rest.
        Raises:
            ValueError
        '''
        width, height = self.reader.get_meta_data()['size']
        if (self.width, self.height) == (width, height):
            pass
        else:
            raise ValueError(
                ('Dimensions of %s (%d, %d) do not match' +
                 'previous files (%d, %d).') % (
                    self.current_file, width, height, self.width, self.height))

        return None

    def _next_file(self):
        '''
        Closes self.reader and opens the next video file (if there is one)
        Returns:
            ret - bool, True if sequence has not yet ended
        '''
        if self.log_file is not None:
            with open(self.log_file, 'a') as log_file:
                log_file.write('%s\t%dframes\n' %
                               (self.current_file, self.file_fr_idx - 1))
        self.file_idx += 1
        self.file_fr_idx = 0
        if self.file_idx < len(self.filenames):
            self.current_file = self.filenames[self.file_idx]
            self.reader.close()
            self.reader = imageio.get_reader(self.current_file,
                                             format='ffmpeg')
            self._check_dimensions()
            return True
        else:
            self.reader.close()
            return False

    def read(self):
        '''
        Grabs, decodes and returns the next frame in the video sequence
        Returns:
            ret - bool indicating read success
            frame - numpy ndarray
        '''
        try:
            frame = self.reader.get_next_data()
        except imageio.core.CannotReadFrameError:
            if self._next_file():
                frame = self.reader.get_next_data()
            else:
                return False, None

        self.tot_fr_idx += 1
        self.file_fr_idx += 1
        return True, frame

    def close(self):
        '''
        Close the current reader
        '''
        self.reader.close()

        return None

    def release(self):
        '''
        Alias for self.close()
        '''
        return self.close()


class BinnedVideoReader(MultiVideoReader):
    '''
    Loads frames into bins for batch processing
    '''
    def __init__(self, *filenames, bin_size=300, **kwargs):
        '''
        Args:
            filenames - Paths to video files (sequential order). Videos are
                assumed to follow on from eachother without a gap.
            bin_size - number of frames to store at once
            **kwargs - passed to MultiVideoCapture().__init__
        '''
        if isinstance(bin_size, int) is False:
            raise ValueError('bin_size must be of type int')

        super().__init__(*filenames, **kwargs)
        self.bin_size = bin_size
        self.img_array = np.empty((self.bin_size, self.height, self.width),
                                  dtype=np.uint8)
        self.running = True

    def read(self):
        '''
        Do not use this function. It will cause unexpected results.
        '''
        warnings.warn(
            '''Do not directly call read() on BinnedMaskedVideoCapture
            objects. This will result in unexpected behaviour. Instead, use
            read_frames(), which calls super().read() instead.''')
        print()
        return super().read()

    def read_frames(self, return_array=True):
        '''
        Grabs, decodes, converts to greyscale and stores the next self.bin_size
        frames in the video sequence. Frames are stored in self.img_array.
        Args:
            return_array - if False, None is returned
        Returns:
            [img_array] - numpy.ndarray, array of images (f, h, w)
        Raises:
            NotImplementedError - Cannot currently handle the case where an
                entire video is spanned by this method
        '''
        assert self.running, 'End of file sequence has been reached.'
        for i in range(self.bin_size):
            ret, img = super().read()
            if ret is True:
                self.img_array[i, ...] = np.mean(img, axis=2).astype(np.uint8)
            else:
                self.running = False
                break

        if return_array is True:
            return self.img_array
        else:
            return None


class IndexedMultiVideoReader(MultiVideoReader):
    '''
    Provides methods for revisiting specific locations in videos previously
    processed
    '''
    def __init__(self, filenames, frame_numbers):
        '''
        Args:
            filenames - paths to video files
            frame numbers - number of frames in each video
        '''
        file_extension = Path(filenames[0]).suffix

        self.frame_numbers = np.array(
            [int(frame_num) for frame_num in frame_numbers]) + 1
        self.cumul_fr_n = np.cumsum(self.frame_numbers)

        self.tot_fr_idx = -1

        super().__init__(*filenames, file_extension=file_extension,
                         log_file=None)

    def set_fr_idx(self, fr_idx):
        '''
        Jump to a specific frame index
        Args:
            fr_idx - int index to jump to
        '''
        if fr_idx < 0:
            raise IndexError('Index must be positive')
        elif fr_idx >= self.cumul_fr_n[-1]:
            raise IndexError('Index ({:d}) out of range ({:d})'.format(
                fr_idx, self.cumul_fr_n[-1]))

        # Open correct file
        file_idx = np.where(self.cumul_fr_n > fr_idx)[0][0]
        self.file_idx = file_idx
        self.current_file = self.filenames[self.file_idx]
        self.reader.close()
        self.reader = imageio.get_reader(self.current_file, format='ffmpeg')
        self._check_dimensions()

        # Jump to correct frame
        self.tot_fr_idx = fr_idx
        if self.file_idx == 0:
            self.file_fr_idx = self.tot_fr_idx
        else:
            self.file_fr_idx = (self.tot_fr_idx -
                                self.cumul_fr_n[self.file_idx - 1] + 1)

        for i in range(self.file_fr_idx):
            self.reader.get_next_data()

        return None


class ArrayVideoReader():
    '''
    Provides the same interface as IndexedMultiVideoReader, but works on an
    array of cameras
    '''
    def __init__(self, offsets_file, log_files, video_directories=None):
        '''
        Args:
            offsets_file - path to file containing offsets of each camera in
                the array
            log_files - list of paths to the log files of each camera
            video_directories - optional argument to change the directory where
                files are located. (By default, use entire path from log_file)
        '''
        offsets = []
        with open(offsets_file, 'r') as f:
            for line in f:
                offset = [int(s) for s in line.split()[-2:]]
                offsets.append(offset)

        self.offsets = np.array(offsets)

        if self.offsets.shape[0] != len(log_files):
            raise ValueError('Expected {:d} log files, got {:d}'.format(
                self.offsets.shape[0], len(log_files)))

        if video_directories is not None:
            assert len(log_files) == len(video_directories)

        filenames_list, framenumbers_list = [], []
        for i in range(len(log_files)):
            filenames, frame_numbers = read_logfile(
                log_files[i], video_directory=(None if \
                    video_directories is None else video_directories[i]))
            filenames_list.append(filenames)
            framenumbers_list.append(frame_numbers)

        self.readers = [IndexedMultiVideoReader(
            filenames_list[i], framenumbers_list[i]) for i in \
                range(len(log_files))]

        sizes = np.array([[r.width, r.height] for r in self.readers])

        self.width = np.amax(self.offsets[:, 1] + sizes[:, 0])
        self.height = np.amax(self.offsets[:, 0] + sizes[:, 1])

        self.tot_fr_idx = -1

    def set_fr_idx(self, fr_idx):
        '''
        Jump to a specific frame index
        Args:
            fr_idx - int index to jump to
        '''
        self.tot_fr_idx = fr_idx
        for reader in self.readers:
            reader.set_fr_idx(fr_idx)

        return None

    def read(self):
        '''
        Grabs, decodes and returns the next frame in each video sequence,
        then combines into a single image
        Returns:
            ret - bool indicating read success
            frame - numpy ndarray
        '''
        combined_frame = np.zeros((self.height, self.width, 3), dtype=np.uint8)
        i = 0
        for reader in self.readers:
            ret, frame = reader.read()
            if ret is False:
                return False, None

            idx00 = self.offsets[i, 0]
            idx01 = idx00 + frame.shape[0]
            idx10 = self.offsets[i, 1]
            idx11 = idx10 + frame.shape[1]
            combined_frame[idx00:idx01, idx10:idx11, :] = frame
            i += 1

        self.tot_fr_idx += 1

        return True, combined_frame

    def close(self):
        for reader in self.readers:
            reader.close()

        return None


class TagBuffer(dict):
    '''
    Class for storing tag location tails
    '''
    def add_tag(self, tag):
        '''
        Add a tag to the buffer
        Args:
            tag - a tracklets.tracklet_dtype value
        '''
        if tag['tag_id'] in self:
            self[tag['tag_id']].appendleft(tag)
        else:
            self[tag['tag_id']] = collections.deque([tag])

    def remove_old(self, fr_idx):
        '''
        Remove last tag in each track if it is older than fr_idx
        Args:
            fr_idx - int
        '''
        for tag_id in set(self):
            tag = self[tag_id].pop()
            if tag['fr_idx'] >= fr_idx:
                self[tag_id].append(tag)

            if len(self[tag_id]) == 0:
                self.pop(tag_id)


class TagDrawer():
    '''
    Class for drawing tag detections
    '''
    def __init__(self, reader, track_loader, out_path, tail_length=5, fps=5,
                 out_size=(1640, 1232)):
        '''
        Args:
            reader - ArrayVideoReader instance
            track_loader - trackproc.FrTrackLoader instance
        '''
        self.reader = reader
        self.track_loader = track_loader
        self._reinit(out_path, tail_length, fps, out_size)

    def _reinit(self, out_path, tail_length, fps, out_size):
        self.tail_length = tail_length
        self.fps = fps
        self.out_size = out_size

        self.tag_buffer = TagBuffer()

        w, h = out_size
        if out_path[-4:] == '.mp4':
            self.writer = imageio.get_writer(
                out_path, format='ffmpeg', fps=self.fps,
                output_params=['-s', '{:d}x{:d}'.format(w, h)]
            )
            self.dw = self.reader.width - self.reader.width % 16
            self.dh = self.reader.height - self.reader.height % 16
        else:
            self.writer = imageio.get_writer(out_path)
            self.dw = self.reader.width
            self.dh = self.reader.height

        #self.scale_factor = min(w / self.reader.width, h / self.reader.height)
        self.a_channel = np.ones((self.reader.height, self.reader.width, 1),
                                 dtype=np.uint8) * 255

    def draw_tails(self, frame):
        '''
        Draw tails on frame based on information in self.tag_buffer
        Args:
            frame - image to draw on (with colour-depth 3)
        '''
        a_frame = np.concatenate((frame, self.a_channel), axis=-1)
        surface = cairo.ImageSurface.create_for_data(
            a_frame, cairo.Format.ARGB32, *a_frame.shape[1::-1]
        )
        cr = cairo.Context(surface)
        cr.set_source_rgb(0., 1., 1.)
        cr.set_line_width(3)

        for tag_id in self.tag_buffer:
            tail = self.tag_buffer[tag_id]
            new_tail = True
            for tag in tail:
                if new_tail:
                    cr.move_to(tag['x'], tag['y'])
                    new_tail = False
                else:
                    cr.line_to(tag['x'], tag['y'])

        cr.stroke()
        surface.finish()

        frame[...] = a_frame[:, :, :3]

        return None

    def draw_tags(self, frame, tags):
        '''
        Draws tag annotations on frame
        Args:
            frame - image to draw on
            tags - tracklets.tracklet_dtype array containing tags to draw
        '''
        a_frame = np.concatenate((frame, self.a_channel), axis=-1)
        surface = cairo.ImageSurface.create_for_data(
            a_frame, cairo.Format.ARGB32, *a_frame.shape[1::-1]
        )
        cr = cairo.Context(surface)
        cr.set_line_width(3)

        for tag in tags:
            cr.move_to(tag['x'] + tag['r'], tag['y'])
            cr.arc(tag['x'], tag['y'], tag['r'], 0., 2 * np.pi)
            cr.set_source_rgb(1., 0., 0.)
            cr.stroke()

            if np.isfinite(tag['angle']):
                cr.move_to(tag['x'], tag['y'])
                cr.line_to(
                    tag['x'] + tag['r'] * np.sin(np.pi * tag['angle'] / 180),
                    tag['y'] - tag['r'] * np.cos(np.pi * tag['angle'] / 180)
                )
                cr.set_source_rgb(0., 0., 1.)
                cr.stroke()

            cr.set_source_rgb(0., 1., 0.)
            cr.move_to(tag['x'] + tag['r'], tag['y'])
            cr.set_font_size(40.)
            cr.show_text('%d' % tag['tag_id'])

        surface.finish()

        frame[...] = a_frame[:, :, :3]

        return None

    def next_frame(self):
        '''
        Load next frame, draw annotations on it, and reencode.
        Returns:
            ret - True if success, False if read failed (EOF)
        '''
        ret, frame = self.reader.read()
        if ret is False:
            return ret

        # Select track data for this frame
        tags = self.track_loader[self.reader.tot_fr_idx]

        # Update self.tag_buffer
        if self.tail_length > 0:
            for tag in tags:
                self.tag_buffer.add_tag(tag)

            self.tag_buffer.remove_old(
                self.reader.tot_fr_idx - self.tail_length
            )

        # Draw tails
        self.draw_tails(frame)

        # Draw tags
        self.draw_tags(frame, tags)

        # Write frame
        # resized = cv2.resize(frame, self.out_size, self.scale_factor,
        #                      self.scale_factor, interpolation=cv2.INTER_AREA)
        # self.writer.write(resized)
        self.writer.append_data(frame[:self.dh,:self.dw,:])

        return ret

    def close(self):
        '''
        Closes self.reader and self.writer
        '''
        self.reader.close()
        self.writer.close()

        return None

    def new_writer(self, out_path, tail_length=None, fps=None, out_size=None):
        '''
        Reinitialises without restarting video reader/track loader
        '''
        if tail_length is None:
            tail_length = self.tail_length

        if fps is None:
            fps = self.fps

        if out_size is None:
            out_size = self.out_size

        self.writer.close()
        self._reinit(out_path, tail_length, fps, out_size)
