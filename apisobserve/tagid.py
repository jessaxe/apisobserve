#!/usr/bin/env python

import argparse
import cv2
import itertools
import numpy as np
import os
from scipy import signal, stats
from skimage import draw, measure, io
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from .tracklets import detection_dtype
import warnings


tag_ids_written = 0


def annotate_frame(img, tag_list, lbp_centroids=None):
    '''
    Annotate video frame with tag ids
    Args:
        img - greyscale (or 3-channel) video frame
        tag_list - output from TagDetector().detect_and_identify
    Returns:
        annotated_img - BGR image, same size as img.
    '''
    if len(img.shape) == 2:
        annotated_img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    elif len(img.shape) == 3:
        annotated_img = img.copy()
    else:
        raise ValueError('Image must be greyscale or of depth 3')

    if tag_list is None:
        return annotated_img

    for x, y, r, a, tag_id, q in tag_list:
        cv2.circle(annotated_img, (x, y), r, (255, 0, 0), thickness=3)
        cv2.line(annotated_img, (x, y),
                 (int(x + r * np.sin(np.pi * a / 180)),
                  int(y - r * np.cos(np.pi * a / 180))),
                 (0, 0, 255),
                 thickness=3)
        cv2.putText(annotated_img, '%d' % tag_id, (x + r, y),
                    cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0),
                    thickness=3)
        cv2.putText(annotated_img, '%f' % q, (x + r, y + r),
                    cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0),
                    thickness=3)

    if lbp_centroids is not None:
        for row, col in lbp_centroids:
            y = int(row)
            x = int(col)
            cv2.line(annotated_img, (x - 10, y - 10), (x + 10, y + 10),
                     (0, 255, 255), thickness=3)
            cv2.line(annotated_img, (x + 10, y - 10), (x - 10, y + 10),
                     (0, 255, 255), thickness=3)

    return annotated_img


def fast_reducing_lbp(img):
    '''
    Calculates local binary pattern image from img, and outputs an image
    with linear dimensions half that of img. Uses 8-connectivity 1-radius
    Args:
        img - 2D array (dtype np.uint8)
    Return:
        lbp_img - local binary pattern image (dtype np.uint8)
    '''
    c_img = img[1:-1:2, 1:-1:2]
    ud_img = img[::2, 1:-1:2]
    lr_img = img[1:-1:2, ::2]
    da_img = img[::2, ::2]

    lbp_img = np.zeros(c_img.shape, dtype=np.uint8)
    #  Calculate local binary pattern
    lbp_img += (lr_img[:, 1:] >= c_img).astype(np.uint8)  # right
    lbp_img += np.left_shift(  # up-right
            (da_img[:-1, 1:] >= c_img).astype(np.uint8), 1)
    lbp_img += np.left_shift(  # up
            (ud_img[:-1, :] >= c_img).astype(np.uint8), 2)
    lbp_img += np.left_shift(  # up-left
            (da_img[:-1, :-1] >= c_img).astype(np.uint8), 3)
    lbp_img += np.left_shift(  # left
            (lr_img[:, :-1] >= c_img).astype(np.uint8), 4)
    lbp_img += np.left_shift(  # down-left
            (da_img[1:, :-1] >= c_img).astype(np.uint8), 5)
    lbp_img += np.left_shift(  # down
            (ud_img[1:, :] >= c_img).astype(np.uint8), 6)
    lbp_img += np.left_shift(  # down-right
            (da_img[1:, 1:] >= c_img).astype(np.uint8), 7)

    #  Determine dis-uniformity
    disuniform_img = np.bitwise_or(
        np.right_shift(lbp_img, 1),  np.left_shift(lbp_img, 7))
    disuniform_img = np.bitwise_xor(disuniform_img, lbp_img)

    #  Determine sum
    sum_img = np.zeros_like(lbp_img)
    for i in range(8):
        sum_img += np.bitwise_and(np.right_shift(lbp_img, i), 0x01)
        sum_img += np.left_shift(
            np.bitwise_and(np.right_shift(disuniform_img, i), 0x01), 4)

    # Finalise
    lbp_img[...] = np.bitwise_and(sum_img, 0x0F)
    lbp_img[np.right_shift(sum_img, 4) > 2] = 9

    return lbp_img


def resize_lbp(img):
    '''
    Takes an image the size of the output of fast_reducing_lbp and resizes it
    to the original image size.
    Args:
        img - output of fast_reducing_lbp (or another image of the same size)
    Returns:
        resized_img - image with shape matching the original image
    '''
    doubled = np.repeat(img, 2, axis=0)
    doubled = np.repeat(doubled, 2, axis=1)

    resized_img = np.empty((doubled.shape[0] + 2, doubled.shape[1] + 2),
                           dtype=img.dtype)
    resized_img[1:-1, 1:-1] = doubled

    # Fix borders
    resized_img[0, 1:-1] = doubled[0, ...]
    resized_img[-1, 1:-1] = doubled[-1, ...]
    resized_img[1:-1, 0] = doubled[..., 0]
    resized_img[1:-1, -1] = doubled[..., -1]

    # Fix corners
    resized_img[0, 0] = doubled[0, 0]
    resized_img[0, -1] = doubled[0, -1]
    resized_img[-1, 0] = doubled[-1, 0]
    resized_img[-1, -1] = doubled[-1, -1]

    return resized_img


def lbp_histogram(lbp_img, ksize, depth=10):
    '''
    Calculate the local histogram for an local binary pattern image.
    Args:
        lbp_img - image with lbp labels
        ksize - histogram is calculated using a box filter
        depth - number of unique labels in lbp_img
            (default 10 - corresponding to output of fast_reducing_lbp)
    Returns:
        hist_img - local histogram for each pixel
    '''
    if len(lbp_img.shape) != 2:
        raise ValueError('Must be 2-dimensional input image')
    if isinstance(ksize, int) is False:
        raise ValueError('ksize must be an integer')
    elif ksize % 2 == 0:
        raise ValueError('ksize must be odd')

    hist_img = np.empty(lbp_img.shape + (depth,), dtype=np.uint16)
    hist_kernel_v = np.ones((ksize, 1), dtype=np.uint16)
    hist_kernel_h = np.transpose(hist_kernel_v)
    for i in range(depth):
        hist_img[..., i] = (lbp_img == i).astype(np.uint16)
        hist_img[..., i] = cv2.filter2D(hist_img[..., i],
                                        cv2.CV_16U, hist_kernel_v)
        hist_img[..., i] = cv2.filter2D(hist_img[..., i],
                                        cv2.CV_16U, hist_kernel_h)

    return hist_img


class ReferenceTagDrawer():
    '''
    Provides methods for calculating  pixel coordinates of tag features.
    '''
    def __init__(self):
        side_length = 8 / np.sqrt(777)
        tb_length = 6 / np.sqrt(777)
        hex_height = 8 * np.sqrt(3 / 777)
        hex_width = tb_length + side_length
        self.scaled_rotation_circle_r = np.sqrt(97 / 777)

        hex_xs = 0.5 * np.array(
            [[-hex_width, -tb_length, tb_length, hex_width]])
        self.scaled_xs = (tb_length + 0.5 * side_length) * \
            np.arange(-2, 2.1).reshape((5, 1)) + hex_xs

        hex_ys = 0.5 * np.array(
            [[-hex_height, 0, hex_height]])
        self.scaled_ys = hex_height * \
            np.arange(-1.5, 1.6, step=0.5).reshape((7, 1)) + hex_ys

        self.hex_order = np.array(
            [[3, 3],
             [2, 2],
             [1, 3],
             [2, 4],
             [4, 2],
             [3, 1],
             [2, 0],
             [1, 1],
             [0, 2],
             [0, 4],
             [1, 5],
             [2, 6],
             [3, 5],
             [4, 4]], dtype=int)

        self.vertex_order = np.array(
            [[3, 1],
             [2, 0],
             [1, 0],
             [0, 1],
             [1, 2],
             [2, 2]], dtype=int)

    def _get_xs_ys(self, tag_img_shape):
        '''
        Args:
            tag_img_shape - shape of tag image
        Returns:
            xs, ys - coordinate arrays with same shape as
                self.scaled_xs and self.scaled_ys respectively
        '''
        xs = 0.5 * tag_img_shape[1] * (1 + self.scaled_xs)
        ys = 0.5 * tag_img_shape[0] * (1 + self.scaled_ys)

        return xs, ys

    def get_polygon(self, tag_img_shape, hex_idx, xs_ys=None):
        '''
        Args:
            tag_img_shape - shape of tag image
            hex_idx - index of hexagon to return
            xs_ys - tuple (output of self._get_xs_ys). This is provided for
                efficiency and to eliminate recalculation. Default (None),
                self._get_xs_ys will be called.
        Returns:
            x, y - vertex coordinate arrays
        '''
        if xs_ys is None:
            xs_ys = self._get_xs_ys(tag_img_shape)

        hex_col, hex_row = self.hex_order[hex_idx]

        vertex_xs = xs_ys[0][hex_col, :]
        vertex_ys = xs_ys[1][hex_row, :]

        x = vertex_xs[self.vertex_order[:, 0]]
        y = vertex_ys[self.vertex_order[:, 1]]

        return x, y

    def get_mask(self, tag_img_shape):
        '''
        Args:
            tag_img_shape - shape of tag image
        Returns:
            mask_array - values correspont to index of hexagon. Background set
                to 255, borders set to 254, nonhex parts of tag set to 127
        '''
        mask_array = np.ones(tag_img_shape, dtype=np.uint8) * 255
        # Draw circle
        # rr, cc = draw.ellipse(tag_img_shape[1] / 2, tag_img_shape[0] / 2,
        #                       tag_img_shape[1] / 2, tag_img_shape[0] / 2,
        #                       shape=tag_img_shape)
        # mask_array[rr, cc] = 127

        # Draw hexagons
        xs, ys = self._get_xs_ys(tag_img_shape)
        for i in range(14):
            x, y = self.get_polygon(tag_img_shape, i, (xs, ys))
            rr, cc = draw.polygon(y, x, tag_img_shape)
            mask_array[rr, cc] = i
            rr, cc = draw.polygon_perimeter(y, x, tag_img_shape)
            mask_array[rr, cc] = 254

        return mask_array


class TagDetector():
    '''
    Class for detecting honeybee tags for identification.
    '''
    def __init__(self, dp=2, minDist=50, param1=50, param2=80, minRadius=35,
                 maxRadius=50, tag_identifier=None, **kwargs):
        '''
        Args:
            dp, min_dist, param1, param2, minRadius & maxRadius all passed to
                cv2.HoughCircles
            tag_identifier - TagIdentifier object. By default, initialise a new
                tag_identifier with default parameters
        '''
        self.dp = dp
        self.minDist = minDist
        self.param1 = param1
        self.param2 = param2
        self.minRadius = minRadius
        self.maxRadius = maxRadius
        if tag_identifier is None:
            self.tag_identifier = TagIdentifier(**kwargs)
        else:
            self.tag_identifier = tag_identifier

    def _get_tag_images(self, img, locations):
        return [img[y - r: y + r, x - r: x + r]
                for x, y, r in locations.astype(int)]

    def _filter_locations(self, img_shape, locs):
        '''
        Hough Circle detection can detect tags which are only partially in
        the frame. These are useless for identification, so here we filter them
        out.
        Args:
            img_shape - tuple of length 2
            locs - first element of array returned by cv2.HoughCircles
        '''
        locs_mask = (locs[:, 0] - locs[:, 2] > 0) * \
                    (locs[:, 0] + locs[:, 2] < img_shape[1]) * \
                    (locs[:, 1] - locs[:, 2] > 0) * \
                    (locs[:, 1] + locs[:, 2] < img_shape[0])

        return locs[np.where(locs_mask)[0], :]

    def detect(self, img, ret=['locations', 'tag_images']):
        '''
        Args:
            img - greyscale image of tag detection
            ret - list of optional return values
        Returns:
            tags_detected - bool, True if any tags are detected
            [locations] - array containing x, y, radius of tags
            [tag_images] - list of tag images
        '''
        circles = cv2.HoughCircles(
            img, cv2.HOUGH_GRADIENT, self.dp, self.minDist, param1=self.param1,
            param2=self.param2, minRadius=self.minRadius,
            maxRadius=self.maxRadius)

        if circles is None:
            return (False,) + tuple(None for i in range(len(ret)))
        else:
            filtered_locs = self._filter_locations(img.shape, circles[0])
            ret_dict = {'locations': (lambda img, locs: locs),
                        'tag_images': self._get_tag_images}
            return (True,) + tuple(ret_dict[ret_val](img, filtered_locs)
                                   for ret_val in ret)

    def detect_and_identify(self, img):
        '''
        Args:
            img - greyscale image of tag detection
        Returns:
            tag_list - list of tag positions and ids. Each element is a tuple
                containing the following:
                    x - x-coordinate of tag
                    y - y-coordinate of tag
                    r - radius of tag
                    a - angle of tag in degrees, where 0 is facing to the right
                    tag_id - tag id
                    q - quality of tag alignment
        '''
        ret, locations, tag_imgs = self.detect(img)
        if ret is False:
            return []

        tag_list = []
        for i in range(len(tag_imgs)):
            self.tag_identifier.identify(tag_imgs[i])
            if self.tag_identifier.is_fit:
                x = int(locations[i, 0] +
                        self.tag_identifier.best_similitude[0])
                y = int(locations[i, 1] +
                        self.tag_identifier.best_similitude[1])
                r = int(locations[i, 2] *
                        self.tag_identifier.best_similitude[3])
                a = self.tag_identifier.best_similitude[2]
                tag_id = self.tag_identifier.best_id
                q = self.tag_identifier.best_quality
                tag_list.append((x, y, r, a, tag_id, q))

        return tag_list

    def annotate_frame(self, img, tag_list):
        warnings.warn('Use function annotate_frame instead of this method.',
                      DeprecationWarning)
        return annotate_frame(img, tag_list)


class LBPTagDetector():
    '''
    Class to perform local binary pattern based detection of tags.
    '''
    def __init__(self, hist_radius=20, thresh_val=0.11, tag_mean_hist=None,
                 learning_rate=0.01):
        '''
        '''
        self.depth = 10
        self.hist_radius = hist_radius
        self.hist_kernel_v = np.ones((2 * hist_radius + 1, 1),
                                     dtype=np.float32) / (2 * hist_radius + 1)
        self.hist_kernel_h = np.transpose(self.hist_kernel_v)
        smth_krnl = signal.gaussian(2 * hist_radius + 1,
                                    hist_radius / 2, sym=True)
        smth_krnl /= smth_krnl.sum()
        self.smooth_kernel_v = smth_krnl.reshape(smth_krnl.shape + (1,))
        self.smooth_kernel_h = smth_krnl.reshape((1,) + smth_krnl.shape)

        self.thresh_val = thresh_val
        if tag_mean_hist is None:
            self.tag_mean_hist = np.ones((self.depth,), dtype=np.float32) / \
                self.depth
        elif tag_mean_hist.shape == (self.depth,):
            self.tag_mean_hist = tag_mean_hist
        else:
            raise ValueError('''tag_mean_hist should be a 1D array of length
                             self.depth or None''')

        self.learning_rate = learning_rate
        self.bins = np.arange(-0.5, self.depth, 1)

    def update_tag_mean_hist(self, tag_list):
        '''
        Update self.tag_mean_hist and self.tags_learned by analysing tag
        detections from a TagDetector()
        Args:
            tag_list - a list of tag information, with the first entries of
                each element being x, y, r;
                    x - x-coordinate
                    y - y-coordinate
                    r - radius of tag detection
                The tag_list should correspond to the currently loaded image
        Returns:
            None
        '''
        mask = np.zeros(self.lbp_img.shape, dtype=bool)
        n_tags = 0
        for tag in tag_list:
            n_tags += 1
            x, y, r = tag[:3]
            rr, cc = draw.circle(y // 2, x // 2, r // 2,
                                 shape=mask.shape)
            mask[rr, cc] = True

        if n_tags > 0:
            hist, bin_edges = np.histogram(self.lbp_img[mask], bins=self.bins,
                                           density=True)
            prop = (1 - self.learning_rate ** n_tags) / \
                (1 - self.learning_rate)
            self.tag_mean_hist = self.tag_mean_hist * (1 - prop) + hist * prop

        return None

    def load_img(self, img, lbp_img=None):
        '''
        Load an image, and calculate the LBP of each pixel
        Args:
            img - greyscale image of same shape as previous images loaded
            lbp_img - optionally pass the lbp image to this function, if it
                has already been computed for another purpose
        Returns:
            None (stored in self.lbp_img)
        Raises:
        '''
        self.input_shape = img.shape
        if lbp_img is None:
            self.lbp_img = fast_reducing_lbp(img)
        else:
            self.lbp_img = lbp_img

        self.diff_img = np.empty(self.lbp_img.shape, dtype=np.float32)
        self.load_img = self._load_img  # Initialisation complete

        return None

    def _load_img(self, img, lbp_img=None):
        '''
        Subsequent calls of self.load_img go here
        '''
        if img.shape == self.input_shape:
            if lbp_img is None:
                self.lbp_img = fast_reducing_lbp(img)
            else:
                self.lbp_img = lbp_img
        else:
            raise ValueError('''An LBPTagDetector instance can only handle
                             a single image size (reinitialise to fix)''')

        return None

    def calculate_histograms(self):
        '''
        Calculate the difference image by comparing local histograms, and store
        in self.diff_img.
        '''
        self.diff_img[...] = 0
        for i in range(self.depth):
            mask = (self.lbp_img == i).astype(np.float32)
            mask = cv2.filter2D(mask, cv2.CV_32F, self.hist_kernel_v)
            mask = cv2.filter2D(mask, cv2.CV_32F, self.hist_kernel_h)
            self.diff_img += np.abs(self.tag_mean_hist[i] - mask)

        self.diff_img *= 0.5

        return None

    def analyse_diff_img(self):
        '''
        Analyses self.diff_img to detect tags
        Returns:
            tag_positions (row, col) of tag detections
        '''
        self.smoothed_diff = cv2.filter2D(self.diff_img, cv2.CV_32F,
                                          self.smooth_kernel_v)
        self.smoothed_diff = cv2.filter2D(self.smoothed_diff, cv2.CV_32F,
                                          self.smooth_kernel_h)
        tag_mask = self.smoothed_diff < self.thresh_val
        labels, num = measure.label(tag_mask, return_num=True)
        tag_positions = []
        for label in range(1, num + 1):
            rr, cc = np.where(labels == label)
            best_pos = np.argmin(self.smoothed_diff[rr, cc])
            tag_positions.append(((rr[best_pos] + 1) * 2,
                                  (cc[best_pos] + 1) * 2))

        return tag_positions

    def detect(self, img, tag_list=None, lbp_img=None):
        '''
        Load an image, calculate local binary patterns and detect tags.
        Args:
            img - greyscale image of same shape as previous images loaded
            tag_list - output of TagDetector().detect_and_identify(). If set,
                update_tag_mean_hist will be called
            lbp_img - optionally pass the lbp image to this function, if it
                has already been computed for another purpose
        Returns:
            locations - locations of tag detections
        '''
        self.load_img(img, lbp_img=lbp_img)
        if tag_list is not None and self.learning_rate != 0:
            self.update_tag_mean_hist(tag_list)
        self.calculate_histograms()

        return self.analyse_diff_img()


class TagIdentifier():
    '''
    Class for the identification of honeybee thoracic tags.
    '''
    def __init__(self, iterations=2,
                 n_samples=[3, 3, 12, 1],
                 initial_search_widths=[0.06, 0.06, 180, 0.],
                 initial_similitude_params=[0., 0., 0., 1.05],
                 otsu_range=(0, 256),
                 image_size=(50, 50),
                 scale_fn_params=(0.015726, 0.429926),
                 tag_dataset_dir=None):
        '''
        KWArgs:
            iterations - number of searches to compute
            The following are presented as the form (xt, yt, rotation, scale):
                n_samples - Number of samples each parameter to
                    take at each iteration. Total computation time will be
                    O(iterations * product(n_samples))
                initial_search_widths - initial half-widths of the space to
                    search each parameter in.
                initial_similitude_params - initial parameters (search centre)
            image_size - tag images will be resized to this size for
                identification. A square is preferable.
        '''
        self.iterations = iterations
        self.n_samples = np.array(n_samples, dtype=int)
        self.initial_search_widths = np.array(initial_search_widths,
                                              dtype=np.float64)
        self.initial_similitude_params = initial_similitude_params
        self.image_size = image_size

        tag_drawer = ReferenceTagDrawer()
        self.mask_array = tag_drawer.get_mask(self.image_size)
        pixel_idxs = []
        n_pixels = []
        for i in range(14):
            pixel_idxs.append(np.where(self.mask_array == i))
            n_pixels.append(len(pixel_idxs[-1][0]))
        self.bsize = min(n_pixels)
        rr = np.concatenate([pixel_idxs[i][0][:self.bsize] for i in range(14)])
        cc = np.concatenate([pixel_idxs[i][1][:self.bsize] for i in range(14)])

        self.rrcc = rr, cc
        self.id_template = (2 ** (13 - np.arange(14))).astype(np.uint16)
        self.scale_fn_params = scale_fn_params

        if tag_dataset_dir is None:
            self.filter_tag_detection = self.return_true
        else:
            self.fit_tag_appearance_model(tag_dataset_dir)

    def return_true(self):
        return True

    def load_tag_img(self, tag_img):
        '''
        Loads tag image and initialises for processing.
        Args:
            tag_img - greyscale cropped tag image
        Returns:
            None
        '''
        self.tag_img = cv2.resize(tag_img, self.image_size,
                                  interpolation=cv2.INTER_AREA)
        self.thr_val, self.thr_img = cv2.threshold(
            self.tag_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        self.search_widths = np.array(self.initial_search_widths,
                                      dtype=np.float64)
        self.search_widths[:2] *= self.tag_img.shape[::-1]

        self.n_similitudes_applied = 0
        self.best_quality = 0.
        self.best_similitude = self.initial_similitude_params
        self.is_fit = False

        return None

    def apply_similitude(self, params):
        '''
        Apply a given similitude to self.thr_img
        Args:
            params - parameters of similitude to apply to selfl.thr_img
                xt, yt, rot, scale
        Returns:
            quality
            id_number
            params
            transformed_img
        '''
        xt, yt, rot, scale = params
        trans_matrix = cv2.getRotationMatrix2D(
            (0.5 * self.thr_img.shape[1] + xt,
             0.5 * self.thr_img.shape[0] + yt), rot, scale)
        trans_matrix[:, 2] -= xt, yt
        transformed_img = cv2.warpAffine(
            self.thr_img, trans_matrix, self.thr_img.shape[::-1],
            flags=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT,
            borderValue=127)
        id_number, quality = self.id_transformed_tag(transformed_img)
        # quality = self.alignment_quality(id_number, bin_proportions)

        return quality, id_number, params, transformed_img

    def update_best(self, quality, id_number, params, transformed_img):
        '''
        Side Effects:
            self.iterations += 1
            If quality is better than self.quality, this function sets:
                self.best_quality
                self.best_transformed_img
                self.best_id
                self.best_similitude
        '''
        self.n_similitudes_applied += 1
        if quality > self.best_quality:
            self.best_quality = quality
            self.best_id = id_number
            self.best_transformed_img = transformed_img
            self.best_similitude = params

        return None

    def id_transformed_tag(self, rotated_tag):
        '''
        Identify tag from binarised geometrically transformed image.
        Args:
            rotated_tag - binarised, geometrically transformed image of tag
        Returns:
            id_number - integer id number of bee
            quality - between 0.0 and 1.0
        '''
        bin_proportions = np.mean(
            rotated_tag[self.rrcc].reshape((14, self.bsize)), axis=1)
        tag_id_bin = bin_proportions > 127
        if np.array_equal(tag_id_bin[:5], np.array([1, 0, 0, 1, 0],
                          dtype=bool)):
            quality = 1 - \
                max(np.max(255 - bin_proportions[tag_id_bin]).astype(int),
                    np.max(bin_proportions[~tag_id_bin]).astype(int)) / 255
            id_number = np.sum(self.id_template[tag_id_bin], dtype=np.uint16)
        else:
            id_number = 0
            quality = 0.0

        return id_number, quality

    def alignment_quality(self, id_number, bin_proportions):
        '''
        Assess the quality of a given alignment / tag identification
        Args:
            id_number, bin_proportions - output of id_rotated_tag
        Returns:
            quality - between 0.0 and 1.0
        '''
        tag_id_bin = (np.fromstring(bin(id_number + 2 ** 14)[3:],
                      dtype=np.uint8) - ord('0')).astype(bool)
        if np.array_equal(tag_id_bin[:6], np.array([1, 0, 0, 1, 0, 1],
                          dtype=bool)):
            quality = 1 - \
                max(np.max(255 - bin_proportions[tag_id_bin]).astype(int),
                    max(bin_proportions[~tag_id_bin]).astype(int)) / 255
            return quality
        else:
            return 0.0

    def get_tag_appearance(self):
        '''
        Get salient features of self.tag_img for constructing tag appearance
        model (or comparing to tag appearance model)
        Returns:
            appearance - 1D feature vector
        '''
        mask = self.thr_img.astype(bool)
        h_score = np.mean(self.tag_img[mask]) - np.mean(self.tag_img[~mask])
        lbp = fast_reducing_lbp(self.tag_img).flatten()
        lbp_hist = np.bincount(lbp, minlength=10) / lbp.shape[0]

        appearance = np.empty((11,), dtype=np.float64)
        appearance[0] = h_score
        appearance[1:] = lbp_hist

        return appearance

    def fit_tag_appearance_model(self, tag_dataset_dir):
        '''
        Fits a LinearDiscriminantAnalysis model of tag appearance
        Args:
            tag_dataset_dir - path to directory with the following structure:
                -|nontagimg------|*.png
                 |tagimg---------|*.png
        '''
        nontags = io.imread_collection(
            os.path.join(tag_dataset_dir, 'nontagimg/*.png'),
            conserve_memory=True)
        tags = io.imread_collection(
            os.path.join(tag_dataset_dir, 'tagimg/*.png'),
            conserve_memory=True)

        data_list = []
        labels_list = []
        for nontag in nontags:
            self.load_tag_img(nontag)
            data_list.append(self.get_tag_appearance())
            labels_list.append(-1)

        for tag in tags:
            self.load_tag_img(tag)
            data_list.append(self.get_tag_appearance())
            labels_list.append(1)

        X = np.stack(data_list, axis=0)
        y = np.array(labels_list)

        self.tag_appearance_model = LinearDiscriminantAnalysis().fit(X, y)

    def filter_tag_detection(self):
        '''
        Determines if tag detecion is erroneous based on a model of tag
        appearance.
        Returns:
            True - self.tag_img is a tag
            False - self.tag_img is not a tag
        '''
        appearance = self.get_tag_appearance()
        label = self.tag_appearance_model.predict(appearance)
        if label == 1:
            return True
        else:
            return False

    def identify(self, tag_img, ret=False):
        '''
        Identify a tag
        Args:
            tag_img - greyscale cropped tag image
        Returns:
            None - output stored in object attributes, namely;
                self.best_quality
                self.best_transformed_img
                self.best_id
                self.best_similitude
            if ret is True, these values are returned
            (except self.best_transformed_img)
        '''
        self.load_tag_img(tag_img)
        if self.filter_tag_detection() is False:
            return None
        for i in range(self.iterations):
            widths = self.search_widths / (self.n_samples ** i)
            xt_p, yt_p, rot_p, scale_p = self.best_similitude

            x_trans = np.linspace(xt_p - widths[0], xt_p + widths[0],
                                  num=self.n_samples[0])

            y_trans = np.linspace(yt_p - widths[1], yt_p + widths[1],
                                  num=self.n_samples[1])

            rots = np.linspace(rot_p - widths[2], rot_p + widths[2],
                               num=self.n_samples[2], endpoint=False)

            scales = np.linspace(scale_p - widths[3], scale_p + widths[3],
                                 num=self.n_samples[3])

            similitudes = itertools.product(x_trans, y_trans, rots, scales)

            for similitude in similitudes:
                quality, id_number, params, transformed_img = \
                    self.apply_similitude(similitude)
                self.update_best(quality, id_number, params, transformed_img)

            # If no possible alignments are found after the first iteration,
            # give up.
            if self.best_quality == 0:
                break

        if self.best_quality > 0:
            self.is_fit = True

        if ret is True:
            if self.best_quality > 0:
                return (self.best_quality, self.best_id, self.best_similitude)
            else:
                return 0., 0, [0., 0., 0., 0.]

        return None

    def identify_from_file(self, tag_file, out_file):
        '''
        Loads a tag image (in .npy format) and identifies it.
        Args:
            tag_file - path to tag file
            out_file - path to output file
        Returns:
            None
        '''
        tag_img = np.load(tag_file)
        self.identify(tag_img)

        if self.is_fit:
            with open(out_file, 'a') as f:
                f.write('%s, %d, %f, %f\n' % (tag_file, self.best_id,
                                              self.best_quality,
                                              self.best_similitude[2]))
        os.remove(tag_file)

        return None

    def identify_and_output(self, tag_img, out_file, frame_idx, x, y, r):
        '''
        Identifies tag, then returns the output string to to be appended to the
        output file.
        Args:
            tag_img - greyscale image of tag to identify
            out_file - returned with output
            frame_idx - index of frame tag_img was taken from
            x, y - coordinates of centre of tag
            r - radius of tag
        Returns:
            out_file - same as input
            append_str - string to append to out_file
        '''
        self.initial_similitude_params[3] = \
            self.scale_fn_params[0] * r + self.scale_fn_params[1]
        self.identify(tag_img)

        if self.is_fit:
            append_str = format_output(
                frame_idx, x, y, r, self.best_id, self.best_quality,
                self.best_similitude[0], self.best_similitude[1],
                self.best_similitude[2], self.best_similitude[3])
        else:
            append_str = ''

        return out_file, append_str

    def identify_and_save(self, tag_img, out_file, frame_idx, x, y, r):
        '''
        Same as identify_from_file, except tag_img is passed directly.
        Args:
            tag_img - greyscale image to identify
            out_file - path to output file
        '''
        self.identify(tag_img)

        if self.is_fit:
            append_str = '%d\t%d\t%d\t%d\t%d\t%f\t%f\n' % (
                frame_idx, x, y, r, self.best_id,
                self.best_quality, self.best_similitude[2])
            append_to_file(out_file, append_str)

        return None

    def get_fit_img(self, img=None):
        if img is None:
            img = self.best_transformed_img.copy()
        else:
            img = img.copy()
        img[self.mask_array == 254] = 127
        return img


def format_output(frame_idx, x, y, r, tag_id, quality, x_trans, y_trans,
                  angle, scale):
    '''
    Formats ouput for writing to file
    '''
    append_str = '%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\t%f\t%f\n' % (
        frame_idx, x, y, r, tag_id, quality, x_trans, y_trans, angle, scale)

    return append_str


def append_to_file(out_file, append_str=None):
    '''
    Append append_str to out_file
    Args:
        out_file - string containing path to file
        append_str - string to append to file. If append_str is None, out_file
            is assumed to be a tuple containing both arguments
    '''
    global tag_ids_written
    tag_ids_written += 1
    if append_str is None:
        with open(out_file[0], 'a') as f:
            f.write(out_file[1])
    else:
        with open(out_file, 'a') as f:
            f.write(append_str)
    return None


def process_error_callback(err_exp):
    raise err_exp


class IdentificationManager():
    '''
    Class to manage background processing of tag identification.
    '''
    def __init__(self, out_file, tag_detector=None, n_processes=1,
                 block=False, queue_maxsize=None, **kwargs):
        '''
        Args:
            out_file - path of output tag ID csv file
            tag_detector - optional TagDetector instance (by default, makes a
                new TagDetector with default parameters)
            n_processes - number of processes the identify method can spawn
            block - boolean flag indicating whether to block while processing
                tag ids
        '''
        self.out_file = out_file
        self.block = block
        with open(self.out_file, 'w') as f:
            f.write('fr_idx\tx\ty\tr\ttag_id\tquality\tx_trans\ty_trans' +
                    '\tangle\tscale\n')

        if tag_detector is None:
            self.tag_detector = TagDetector(**kwargs)
        else:
            self.tag_detector = tag_detector

        self.tag_identifier = self.tag_detector.tag_identifier

        self.n_processes = n_processes

        self.pool = Pool(processes=self.n_processes, maxsize=queue_maxsize)
        self.n_tags_detected = 0

    def detect(self, img, frame_idx):
        '''
        Detect potential tags and save them in self.tag_dir
        Args:
            img - greyscale image to use for tag detection
            frame_idx - index of frame
        Returns:
            None - if self.block is False
            tag_list - if self.block is True
        '''
        tags_detected, locations, tag_images = \
            self.tag_detector.detect(img, ['locations', 'tag_images'])

        if tags_detected is True and self.block is False:
            self.n_tags_detected += locations.shape[0]
            for i in range(locations.shape[0]):
                x, y, r = locations[i, :].astype(int)
                tag_image = tag_images[i]

                self.pool.apply_async(
                    self.tag_identifier.identify_and_output,
                    args=(tag_image, self.out_file, frame_idx, x, y, r),
                    callback=append_to_file,
                    error_callback=process_error_callback)

        elif tags_detected is True and self.block is True:
            self.n_tags_detected += locations.shape[0]
            locations_int = locations.astype(int)

            res_list = self.pool.starmap(self.tag_identifier.identify, zip(
                tag_images, itertools.cycle([True])))

            tag_list = []

            for i in range(len(res_list)):
                x, y, r = locations_int[i, :]
                tag_id = res_list[i][1]
                quality = res_list[i][0]
                x_trans = res_list[i][2][0]
                y_trans = res_list[i][2][1]
                angle = res_list[i][2][2]
                scale = res_list[i][2][3]

                if tag_id > 0:
                    tag_list.append((x, y, r, angle, tag_id, quality))
                    append_str = format_output(
                        frame_idx, x, y, r, tag_id, quality,
                        x_trans, y_trans, angle, scale)
                    append_to_file(self.out_file, append_str)

            return tag_list

        return None


def parse_args():
    parser = argparse.ArgumentParser(description='''Identifies tags from a
                                     video and saves them in a csv file.''')

    parser.add_argument('-i', '--inputvideo', required=True, help='''Input
                        video file.''')
    parser.add_argument('-o', '--outputfile', required=True, help='''Output
                        text file. (Will be overwritten).''')
    parser.add_argument('-d', '--displayvideo', action='store_true',
                        help='''Visualise tag detections. Slows down program
                        significantly.''')
    parser.add_argument('-s', '--savevideo', required=False, default='',
                        help='''Save display video with h264 encoding to this
                        path. Has no effect if -d/--displayvideo flag not
                        set.''')
    parser.add_argument('-q', '--queuemaxsize', required=False, type=int,
                        default=0, help='''Specify the maximum size for
                        the tag identification queue. By default, None.
                        This has no effect if -d/--displayvideo is set.''')
    parser.add_argument('--learnscale', required=False, type=int, default=0,
                        metavar='n', help='''Learn scale parameter from first
                        n tag detections. If 0, use predefined scale
                        parameter.''')
    parser.add_argument('--skipframes', required=False, type=int, default=0,
                        help='''Skip n frames at beginning of video.''',
                        metavar='n')

    return parser.parse_args()


def learn_scale_params(video_file, n_tags=1000, scale_samples=7,
                       scale_search_width=0.15, TEMP_FILE='_scale_learn_temp',
                       ret_lbp=False, **kwargs):
    '''
    Learns appropriate scale parameter for tag identification.
    Args:
        video_file - path to video (or VideoCapture like object)
        n_tags - number of tag detections to base learned parameters off
        scale_samples -
        scale_search_width - radius of scale parameter search space
        ret_lbp - also return lbp_hist
        **kwargs - passed to IdentificationManager()
    Returns:
        slope, intercept - parameters of linear regression or scale vs. radius
        [lbp_hist] - learned local binary pattern histogram
    '''
    global tag_ids_written
    print('Learning scale parameters...', end='')

    if 'initial_search_widths' not in kwargs:
        kwargs['initial_search_widths'] = [0.06, 0.06, 180, scale_search_width]

    if 'n_samples' not in kwargs:
        kwargs['n_samples'] = [3, 3, 12, scale_samples]

    identification_manager = IdentificationManager(
        TEMP_FILE, block=ret_lbp,
        queue_maxsize=32, scale_fn_params=(0., 1.), **kwargs)

    if isinstance(video_file, str):
        cap = cv2.VideoCapture(video_file)
    else:
        cap = video_file
    ret, frame = cap.read()

    if ret_lbp is True:
        lbp_hist = np.zeros((10,))

    while ret is True:
        frame_gr = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        tag_list = identification_manager.detect(frame_gr, 0)
        if ret_lbp is True:
            lbp_img = fast_reducing_lbp(frame_gr)
            if tag_list is not None:
                for tag in tag_list:
                    x, y, r = tag[:3]
                    x = int(x / 2)
                    y = int(y / 2)
                    r = int(r / 2)
                    rr, cc = draw.circle(y, x, r, shape=lbp_img.shape)
                    lbp_hist += np.bincount(lbp_img[rr, cc], minlength=10)

        print('\rLearning scale parameters... %.0f%%' %
              (100 * tag_ids_written / n_tags),
              end='')
        if identification_manager.n_tags_detected >= n_tags:
            break
        ret, frame = cap.read()

    identification_manager.pool.close()
    identification_manager.pool.join()

    cap.release()

    print('\rLearning scale parameters... Done (%d tags)' % tag_ids_written)
    tag_ids_written = 0

    detections = np.loadtxt(TEMP_FILE, dtype=detection_dtype, skiprows=1)
    os.remove(TEMP_FILE)

    slope, intercept, r_val, p_val, stderr = stats.linregress(
        detections['r'], detections['scale'])
    print('Learned scale fn: Scale = %f x r + %f' % (slope, intercept))

    if ret_lbp is True:
        return slope, intercept, lbp_hist / lbp_hist.sum()
    else:
        return slope, intercept


def main():
    global tag_ids_written

    args = parse_args()

    # Learn scale parameter
    kwargs = {}
    if args.learnscale > 0:
        kwargs['scale_fn_params'] = learn_scale_params(args.inputvideo,
                                                       n_tags=args.learnscale)

    cap = cv2.VideoCapture(args.inputvideo)
    i = 0
    if args.skipframes > 0:
        for i in range(args.skipframes):
            ret, frame = cap.read()

    identification_manager = IdentificationManager(
        args.outputfile, block=args.displayvideo,
        queue_maxsize=args.queuemaxsize, **kwargs)

    save_video = False
    if args.displayvideo is True:
        cv2.namedWindow('annotated', cv2.WINDOW_NORMAL)
        if len(args.savevideo) > 0:
            save_video = True
            writer = cv2.VideoWriter(
                args.savevideo, cv2.VideoWriter_fourcc(*'X264'), 5,
                (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                 int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))

    i -= 1
    while True:
        i += 1
        ret, frame = cap.read()
        if ret is False:
            break

        frame_gr = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        tag_list = identification_manager.detect(frame_gr, i)

        print(' Frames read: %d Tags detected/identified: %d/%d' %
              (i, identification_manager.n_tags_detected, tag_ids_written),
              end='\r')

        if args.displayvideo is True:
            annotated_img = annotate_frame(frame, tag_list)
            if save_video is True:
                writer.write(annotated_img)
            cv2.imshow('annotated', annotated_img)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('q'):
                break

    print()
    cap.release()
    if save_video is True:
        writer.release()

    identification_manager.pool.close()
    identification_manager.pool.join()

if __name__ == '__main__':
    main()
