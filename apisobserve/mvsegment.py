#!/usr/bin/env python

import argparse
import gzip
from multiprocessing import Pool
import numpy as np
from scipy.ndimage import filters
import time

VERBOSE = False
LAST_VPRINT = None

motion_dtype = np.dtype([
        ('x', np.int8),
        ('y', np.int8),
        ('sad', np.uint16)
    ])


def load_mvs(path, frame_shape=(972, 1296)):
    '''
    Load a motion vector file into a numpy array.
    Args:
        path - string containing path of motion vector file
        frame_shape - tuple of ints containing frame size (height, width)
    Returns:
        motion_data - array of shape (n_frames, motion_height, motion_width),
            all calculated from frame_shape and the length of the motion data.
    '''
    global motion_dtype
    motion_height = (frame_shape[0] + 15) // 16
    motion_width = (frame_shape[1] + 15) // 16
    vprint('Loading motion vector data')
    motion_data = np.fromfile(path, dtype=motion_dtype)
    n_frames = motion_data.shape[0] // (motion_height * (motion_width + 1))
    vprint('Reshaping')
    motion_data = motion_data.reshape(
        (n_frames, motion_height, motion_width + 1))[..., : -1]
    return motion_data


def segment_sad(motion_data, sad_thresh, max_time):
    '''
    Segment nurses based on motion vector data, including SAD heuristic
    Args:
        motion_data - raw motion data in a numpy array of type motion_dtype
        sad_thresh - upper threshold to relate frame segmentations to previous
                    frame segmentations
        max_time - maximum number of iterations for SAD heuristic
    Returns:
        segmentation
    '''
    mask = motion_data['x'].astype(bool) + \
        motion_data['y'].astype(bool)
    sad_mask = ((motion_data['sad'] < sad_thresh) * ~mask)

    old_mask = mask.copy()
    iteration = 0
    while True:
        mask[1:, ...][sad_mask[1:, ...]] += old_mask[:-1, ...][sad_mask[1:, ...]]
        mask[:-1, ...][sad_mask[:-1, ...]] += old_mask[1:, ...][sad_mask[:-1, ...]]
        n_updated = np.sum(mask != old_mask)
        print('iteration %d, %d updated' % (iteration, n_updated))
        iteration += 1
        if n_updated == 0:
            break
        else:
            old_mask[...] = mask[...]
            sad_mask *=  ~mask
        if iteration == max_time:
            break

    return mask


def segment(motion_data, size=(5, 5, 5), mode='median'):
    '''
    Segment nurses based on motion vector data.
    Args:
        motion_data - raw motion data in a numpy array of type motion_dtype
        size - size of median kernel to filter speed data with
            (frames, height, width)
    Returns:
        segmentation
    '''
    mask = motion_data['x'].astype(bool) + motion_data['y'].astype(bool)
    ref_frames = np.where(np.all(motion_data['sad'] == 0, axis=(1, 2)))
    mask[ref_frames[0], :, :] = True
    mask[ref_frames[0][:-1] + 1, :, :] = True
    mask[ref_frames[0][1:] - 1, :, :] = True
    if mode == 'median':
        vprint('Filtering mask')
        filter_intermediate = np.empty(mask.shape, dtype=np.float32)
        filters.uniform_filter(mask, size=size, output=filter_intermediate)
        mask[...] = filter_intermediate >= 0.5

    if mode == 'max':
        vprint('Filtering mask')
        mask = filters.maximum_filter(mask, size=size, footprint=footprint)
    return mask


def save_segmentation(segmentation, path, compresslevel=9):
    '''
    Compress and save segmentation data to a file. The first 12 bytes of the
    (decompressed) file contain the shape of the segmentation array as
    unsigned 32-bit integers.
    Args:
        segmentation - boolean array to save
        path - str containing path of file to write to
        compresslevel - compression level passed to gzip
    Returns:
        None
    '''
    assert len(segmentation.shape) == 3, 'Segmentation must be 3-dimensional'
    vprint('Packing bytes')
    shape_bytes = np.array(segmentation.shape, dtype=np.uint32).tostring()
    segm_bytes = np.packbits(segmentation).tostring()
    vprint('Compressing and saving')
    with gzip.open(path, 'wb', compresslevel=compresslevel) as gz_file:
        gz_file.write(shape_bytes + segm_bytes)

    return None


def load_segmentation(path):
    '''
    Decompress and load segmentation data from file. The first 12 bytes of the
    (decompressed) file must contain the shape of the segmentation array as
    unsigned 32-bit integers.
    Args:
        path - path of compressed file
    Returns:
        segmentation - boolean array
    '''
    with gzip.open(path, 'rb') as gz_file:
        decompressed = gz_file.read()

    shape = tuple(np.fromstring(decompressed[:12], dtype=np.uint32))
    data_length = shape[0] * shape[1] * shape[2]
    segmentation = np.unpackbits(
        np.fromstring(decompressed[12:], dtype=np.uint8))[:data_length]

    return segmentation.reshape(shape).astype(bool)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Verbose output')
    parser.add_argument('-i', metavar='path', type=str, nargs='+', required=True,
                        help='''Input motion vector file''')
    parser.add_argument('-o', metavar='path', type=str, nargs='*',
                        help='''Output compressed segmentation file''')
    parser.add_argument('-fs', '--frameshape', metavar=['height', 'width'],
                        nargs=2, type=int, required=False, default=[1232, 1640],
                        help='''Shape of video frames. The shape of the
                        segmentation array is calculated from this.''',
                        dest='fs')
    parser.add_argument('-mf', '--medianfiltersize', type=int, required=False,
                        metavar=['frames', 'height', 'width'], nargs=3,
                        default=[5, 5, 5], help='''Size of median filter to
                        filter speed data. (For calculating segmentation).''',
                        dest='mf')
    parser.add_argument('--mode', type=str, required=False, default='median',
                        help='Filter mode. Default median. can be set to max')
    parser.add_argument('--sad_thresh', type=np.uint16, required=False,
                        default=0, help='''Threshold to use for SAD
                        segmentation''')
    parser.add_argument('--sad_maxtime', type=int, required=False,
                        default=-1, help='Maximum time for SAD segmentation')
    return parser.parse_args()


def vprint(*args, **kwargs):
    '''
    Returns print(*args, **kwargs) if VERBOSE flag is set to True.
    Else, returns None
    '''
    global VERBOSE
    global LAST_VPRINT
    if VERBOSE is True:
        kwargs['end'] = '\t'
        current_time = time.time()
        if LAST_VPRINT is not None:
            print('%ssec' % (current_time - LAST_VPRINT))
        LAST_VPRINT = current_time
        return print(*args, **kwargs)
    elif VERBOSE is False:
        return None
    else:
        raise RuntimeError('VERBOSE flag is not set (must be bool)')


def to_loop(args, idx):
    '''
    Should be placed in for loop in main() can be called asynchronously
    Args:
        args - output of argparse.ArgumentParser().parse_args()
        idx - index of file
    Returns:
        None
    '''
    # Load movement file
    mv_array = load_mvs(args.i[idx], frame_shape=tuple(args.fs))

    # Segment worker bees
    if args.mode == 'sad':
        segmentation = segment_sad(mv_array, args.sad_thresh,
            args.sad_maxtime)
    else:
        segmentation = segment(mv_array, size=tuple(args.mf),
            mode=args.mode)

    # Save segmentation
    save_segmentation(segmentation, args.o[idx], compresslevel=9)

    return None


def main():
    args = parse_args()
    global VERBOSE
    VERBOSE = args.verbose

    if args.o is None:
        args.o = [args.i[idx][:-6] + 'segm' for idx in range(len(args.i))]

    assert len(args.o) == len(args.i), '''must have same number of inputs and
        outputs.'''

    process_pool = Pool()
    process_pool.starmap_async(
        to_loop, [(args, idx) for idx in range(len(args.i))])
    process_pool.close()
    process_pool.join()

    vprint('')
    print()

if __name__ == '__main__':
    main()
