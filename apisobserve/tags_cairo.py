#!/usr/bin/env python

import cairocffi as cr
import numpy as np
import itertools


def draw_hex(ctx, rgb, sl, w=1.0):
    xs = [-.5 * (1. + w) * sl, -.5 * w * sl, .5 * w * sl, .5 * (1. + w) * sl]
    ys = [-np.sqrt(.75) * sl, 0., np.sqrt(0.75) * sl]
    ctx.move_to(xs[3], ys[1])
    ctx.line_to(xs[2], ys[0])
    ctx.line_to(xs[1], ys[0])
    ctx.line_to(xs[0], ys[1])
    ctx.line_to(xs[1], ys[2])
    ctx.line_to(xs[2], ys[2])
    ctx.line_to(xs[3], ys[1])
    ctx.set_source_rgba(*tuple(rgb))
    ctx.fill()
    return None


def get_trans_array(mid_rows):
    '''
    Produce translation array for tag drawing
    Args:
        mid_rows - number of cells stacked on top of eachother in the middle
                     _     _
                   _/ \     |
              _  _/ \_/ \_  |
             |  / \_/ \_/ \ |
        side_|  \_/ \_/ \_/ |_mid
        rows |  / \_/ \_/ \ | rows
             |_ \_/ \_/ \_/ |
                  \_/ \_/   |
                    \_/    _|
        In this case, side_rows is 2 and mid_rows is 4.
        side_rows is calculated from mid_rows
    Returns:
        trans_array
    '''
    assert isinstance(mid_rows, int) and mid_rows > 0

    n_rings = mid_rows // 2 + mid_rows % 2
    diag_steps = np.arange(1, n_rings + 1, dtype=int) - (mid_rows % 2)
    vert_steps = np.arange(0, n_rings, dtype=int)

    trans_list = [[1 - mid_rows % 2, 0]]
    for ring in range(n_rings):
        if mid_rows % 2 == 0:
            if ring == 0:
                end_trans = [[2, -2]]
                last_diag = 0
            elif ring % 2 == 1:
                end_trans = [[1, -1]]
                last_diag = diag_steps[ring]
            else:
                end_trans = [[1, -2]]
                last_diag = diag_steps[ring]
        else:
            end_trans = [[1, -1]]
            last_diag = diag_steps[ring]

        ring_trans_list = \
            [[0, -2]] * (vert_steps[ring] // 2) + \
            [[-1, -1]] * diag_steps[ring] + \
            [[-1, 1]] * diag_steps[ring] + \
            [[0, 2]] * vert_steps[ring] + \
            [[1, 1]] * diag_steps[ring] + \
            [[1, -1]] * last_diag + \
            [[0, -2]] * (vert_steps[ring] // 2) + \
            end_trans

        trans_list.extend(ring_trans_list)

    trans_array = np.array(trans_list)
    # Fix last translation to recentre.
    trans_array[-1] = -np.sum(trans_array[:-1], axis=0)

    return trans_array.astype(np.float64)


def draw_tag(ctx, rgb0, rgb1, sl, w, trans_array, tag_number):
    xy_trans = np.array([(w + .5) * sl, np.sqrt(.75) * sl])

    translations = trans_array * xy_trans

    id_str = "{0:0>14b}".format(tag_number)[-14:]

    i = 0
    ctx.translate(translations[0, 0], translations[0, 1])
    for translation in translations[1:]:
        if id_str[i] == '0':
            draw_hex(ctx, rgb0, sl, w)
        elif id_str[i] == '1':
            draw_hex(ctx, rgb1, sl, w)
        ctx.translate(translation[0], translation[1])
        i += 1

    # ctx.translate(translations[-1, 0], translations[-1, 1])
    return None


def draw_half_box(ctx, rgb, bleed=0.6):
    ctx.arc(0., 0., bleed, 5 * np.pi / 4, 7 * np.pi / 4)
    ctx.line_to(0, 0)
    ctx.set_source_rgb(*rgb)
    ctx.fill()

    return None


def draw_dot_matrix(ctx, bleed=0.6, dot_size=0.015, num=15):
    old_linewidth = ctx.get_line_width()

    bleed_sq = bleed ** 2

    for x, y in itertools.product(
            np.linspace(-bleed, bleed,
                        num=num, endpoint=False), repeat=2):
        if x ** 2 + y ** 2 < bleed_sq:
            ctx.rectangle(x - dot_size, y - dot_size, 2 * dot_size, 2 * dot_size)

    ctx.fill()

    ctx.set_line_width(old_linewidth)
    return None


def main():
    circle_colour = (0, 0, 0)
    tag_wh = 0.11811 * 72.0  # points
    gap_wh = .23  # tags
    edge_wh = 3.  # tags
    surface_w = (1. + gap_wh) * tag_wh * 16 + 2 * edge_wh * tag_wh
    surface_h = (1. + gap_wh) * tag_wh * 64 + 2 * edge_wh * tag_wh

    diameter_surface_draw_list = [(0., cr.PDFSurface, 'pdf', True, True, False),
                                  (3., cr.PDFSurface, 'pdf', False, False, False),
                                  (3., cr.PDFSurface, 'pdf', True, True, False)]

    for tag_diameter, cr_surface, fmt, draw_bits, draw_orentation, draw_dots in \
            diameter_surface_draw_list:
        filename = 'tag_designs/jesse_bee_tags%.1f%s%s.%s' % \
            (tag_diameter, ['notags', 'tags'][draw_bits],
             ['nodots', 'dots'][draw_dots], fmt)
        print('Producing file %s' % filename)
        surface = cr_surface(filename, surface_w, surface_h)
        ctx = cr.Context(surface)

        ctx.set_source_rgb(*circle_colour)
        ctx.set_line_width(0.07)

        ctx.scale(tag_wh, tag_wh)

        ##  # Draw border
        ##  ctx.move_to(0, 0.5 * edge_wh)
        ##  ctx.line_to(2 * edge_wh + 32 * (1 + gap_wh), 0.5 * edge_wh)
        ##  ctx.stroke()
        ##  ctx.move_to(1.5 * edge_wh + 32 * (1 + gap_wh), 0)
        ##  ctx.line_to(1.5 * edge_wh + 32 * (1 + gap_wh),
        ##              2 * edge_wh + 49 * (1 + gap_wh))
        ##  ctx.stroke()
        ##  ctx.move_to(2 * edge_wh + 32 * (1 + gap_wh),
        ##              1.5 * edge_wh + 49 * (1 + gap_wh))
        ##  ctx.line_to(0,
        ##              1.5 * edge_wh + 49 * (1 + gap_wh))
        ##  ctx.stroke()
        ##  ctx.move_to(0.5 * edge_wh, 2 * edge_wh + 49 * (1 + gap_wh))
        ##  ctx.line_to(0.5 * edge_wh, 0)
        ##  ctx.stroke()

        ##  # Draw orientation arrow
        ##  ctx.move_to(0.1 * edge_wh, 0.2 * edge_wh)
        ##  ctx.line_to(0.25 * edge_wh, 0.05 * edge_wh)
        ##  ctx.line_to(0.4 * edge_wh, 0.2 * edge_wh)
        ##  ctx.move_to(0.25 * edge_wh, 0.05 * edge_wh)
        ##  ctx.line_to(0.25 * edge_wh, 0.45 * edge_wh)
        ##  ctx.stroke()

        ##  ctx.move_to(0.6 * edge_wh, 0.7 * edge_wh)
        ##  ctx.line_to(0.75 * edge_wh, 0.55 * edge_wh)
        ##  ctx.line_to(0.9 * edge_wh, 0.7 * edge_wh)
        ##  ctx.move_to(0.75 * edge_wh, 0.55 * edge_wh)
        ##  ctx.line_to(0.75 * edge_wh, 0.95 * edge_wh)
        ##  ctx.stroke()

        ctx.translate(edge_wh + 0.5 * (1. + gap_wh),
                      edge_wh + 0.5 * (1. + gap_wh))

        trans_array = get_trans_array(4)
        hex_sidelength = np.sqrt(16 / 777) * 0.8
        hex_width_scaled = 0.75

        ##  # Draw row of crosses and border cross marks
        ##  for j in range(32):
        ##      ctx.move_to(0, -edge_wh - 0.5 * (1 + gap_wh))
        ##      ctx.line_to(0, -gap_wh - 0.5)
        ##      ctx.stroke()
        ##      ctx.move_to(0, 49 * (1 + gap_wh) - 0.5)
        ##      ctx.line_to(0, 48.5 * (1 + gap_wh) + edge_wh)
        ##      ctx.stroke()

        ##      ctx.move_to(-0.5, 0)
        ##      ctx.line_to(0.5, 0)
        ##      ctx.move_to(0, -0.5)
        ##      ctx.line_to(0, 0.5)
        ##      ctx.stroke()
        ##      ctx.translate(1. + gap_wh, 0.)
        ##  ctx.translate(-32. * (1. + gap_wh), 1. + gap_wh)

        ##  first_tag_numbers = [2 ** 13 + 2 ** 10 + 2 ** 8,
        ##                       2 ** 13 + 2 ** 10 + 2 ** 8,
        ##                       2 ** 13 + 2 ** 10,
        ##                       2 ** 13 + 2 ** 10]
        ##  first_tag_number_idx = 0
        ##  first_tag_number = first_tag_numbers[first_tag_number_idx]
        ##  tag_number = first_tag_number
        tag_number = 2 ** 13 + 2 ** 10 + 2 ** 8
        for i in range(64):
            ##  # Draw border cross marks
            ##  ctx.move_to(-edge_wh - 0.5 * (1 + gap_wh), 0)
            ##  ctx.line_to(-gap_wh - 0.5, 0)
            ##  ctx.stroke()
            ##  ctx.move_to(32 * (1 + gap_wh) - 0.5, 0)
            ##  ctx.line_to(31.5 * (1 + gap_wh) + edge_wh, 0)
            ##  ctx.stroke()
            # Draw tags
            for j in range(16):
                if draw_orentation:
                    # Orientation Marker
                    draw_half_box(ctx, (1, 0, 0))
                    ctx.set_source_rgb(*circle_colour)
                if draw_dots:
                    # Dot matrix
                    draw_dot_matrix(ctx)
                if tag_diameter > 0:
                    # Circle outline
                    ctx.arc(0., 0., tag_diameter / 6., 0., np.pi * 2)
                    ctx.set_source_rgb(*circle_colour)
                    ctx.stroke()
                if draw_bits:
                    # Tag design
                    draw_tag(ctx, (0, 0, 0), (1, 1, 1), hex_sidelength,
                             hex_width_scaled, trans_array, tag_number)
                    ctx.set_source_rgb(*circle_colour)
                ctx.translate(1. + gap_wh, 0.)
                tag_number += 1
                ##  if tag_number == first_tag_number + 256:
                ##      first_tag_number_idx += 1
                ##      first_tag_number = first_tag_numbers[first_tag_number_idx]
                ##      tag_number = first_tag_number
            if i % 2 == 0:
                ctx.translate(-15.5 * (1. + gap_wh), 1. + gap_wh)
            else:
                ctx.translate(-16.5 * (1. + gap_wh), 1. + gap_wh)
            ##  if tag_number == first_tag_number:
            ##      # Draw reference line
            ##      ctx.move_to(-edge_wh - 0.5 * (1 + gap_wh), 0)
            ##      ctx.line_to(31.5 * (1 + gap_wh) + edge_wh, 0)
            ##      ctx.stroke()
            ##      for j in range(32):
            ##          ctx.move_to(0, -0.5)
            ##          ctx.line_to(0, 0.5)
            ##          ctx.stroke()
            ##          ctx.translate(1. + gap_wh, 0.)
            ##      ctx.translate(-32. * (1. + gap_wh), 1. + gap_wh)

            ##  elif i % 2 == 1:
            ##      # Draw row of crosses
            ##      for j in range(32):
            ##          ctx.move_to(-0.5, 0)
            ##          ctx.line_to(0.5, 0)
            ##          ctx.move_to(0, -0.5)
            ##          ctx.line_to(0, 0.5)
            ##          ctx.stroke()
            ##          ctx.translate(1. + gap_wh, 0.)
            ##      ctx.translate(-32. * (1. + gap_wh), 1. + gap_wh)

        surface.finish()


if __name__ == '__main__':
    main()
