#!/usr/bin/env python

import cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import hann
from scipy.ndimage.filters import gaussian_filter1d


class CombAligner():
    def __init__(self, img=None):
        if img is not None:
            self.set_img(img)
        self.normaliser = cv2.createCLAHE()
        self.aligned = False

    def _normalise(self):
        '''
        Perform Contrast Limited Adaptive Histogram Equalization (CLAHE) on
        self.img and normalise to a float64 with range -1 to +1.
        Output stored in self.norm.
        Returns:
            None
        '''
        self.norm = self.normaliser.apply(self.img).astype(np.float64) / \
            128 - 1
        self.dim = self.img.shape[::-1]
        return None

    def _means(self):
        '''
        Calculate mean intensity of self.norm along both axes and stores in
        self.mean.
        Returns:
            None
        '''
        self.mean = tuple(self.norm.mean(axis=i) for i in (0, 1))
        return None

    def _windowed_means(self):
        '''
        Applies Hann window to self.mean along both axes to reduce edge
        effects, and stores in self.windowed.
        Returns:
            None
        '''
        self.windowed = tuple(hann(self.dim[i]) * self.mean[i] for i in (0, 1))
        return None

    def _rffts(self):
        '''
        Calculates Fast Fourier Transform (FFT) on self.windowed along each
        axis (real data), and stores output in self.rfft.
        Returns:
            None
        '''
        self.rfft = tuple(np.fft.rfft(self.windowed[i], n=(self.dim[i] * 2))
                          for i in (0, 1))
        return None

    def _autocorrs(self):
        '''
        Calculates autocorrelation of self.windowed by taking the inverse FFT
        of self.rfft along each axis and multiplying itself by its complex
        conjugate. The first local maximum of the real component will be at the
        periodicity of the image along teh axis. Stores in self.autocorr.
        Returns:
            None
        '''
        self.autocorr = tuple(np.fft.ifft(self.rfft[i] *
                              self.rfft[i].conjugate(),
                              n=(self.dim[i] * 2))[:self.dim[i]]
                              for i in (0, 1))
        return None

    def _periods(self):
        '''
        Finds first local maximum of the real component of self.autocorr along
        each axis. This corresponds to the periodicity of the image along each
        axis respectively. Stores in self.period.
        Returns:
            None
        '''
        period = [None, None]
        for i in (0, 1):
            min_period = np.where(self.autocorr[i].real[:-1] <
                                  self.autocorr[i].real[1:])[0][0] + 1
            period[i] = np.where(
                    self.autocorr[i].real[min_period:-1] >
                    self.autocorr[i].real[min_period + 1:])[0][0] + min_period

        self.period = tuple(period)
        return None

    def _phase_graphs(self, std_dev=3):
        '''
        First step in calculating phase. Produces an average bin across each
        axis, and smooths the resulting function with a Gaussian kernel.
        Stores average bin in self.phase_graph and the smoothed average bin in
        self.phase_graph_smooth.
        Args:
            std_dev - standard deviation of Gaussian kernel
        Returns:
            None
        '''
        amp = [None, None]
        smoothed = [None, None]
        for i in (0, 1):
            reps = self.dim[i] // self.period[i]
            pre_amp = np.reshape(self.mean[i][:reps * self.period[i]],
                                 (reps, self.period[i])).copy()
            amp[i] = pre_amp.mean(0)
            smoothed[i] = gaussian_filter1d(amp[i], std_dev, mode='wrap')

        self.phase_graph = tuple(amp)
        self.phase_graph_smooth = tuple(smoothed)
        return None

    def _phases(self):
        '''
        Calculates phases of each axis. Stores in self.phase
        Returns:
            None
        '''
        self.phase = tuple(self.phase_graph_smooth[i].argmax() for i in (0, 1))
        return None

    def _vote_template(self):
        '''
        Produces voting template for comb alignment. Stores in
        self.vote_template.
        Returns:
            None
        '''
        odd_rows = np.where((np.arange(self.dim[1]) - self.phase[1]) %
                            (self.period[1] * 2) > self.period[1])

        self.vote_template = np.ones(self.dim[1], dtype=np.int8)
        self.vote_template[odd_rows] = -1
        return None

    def _cw_means(self):
        '''
        Takes one pixel column of self.img for each column containing cell wall
        and produces the mean pixel column for odd-numbered cell wall columns
        and even-numbered cell wall columns respectively. Stores in
        self.odd_cw_mean and self.even_cw_mean.
        Returns:
            None
        '''
        odd_idx = np.arange(self.phase[0], self.dim[0], self.period[0] * 2)
        odd_cols = self.norm[:, odd_idx]
        self.odd_cw_mean = odd_cols.mean(axis=1)

        even_idx = np.arange(self.phase[0] + self.period[0], self.dim[0],
                             self.period[0] * 2)
        even_cols = self.norm[:, even_idx]
        self.even_cw_mean = even_cols.mean(axis=1)
        return None

    def _h_phase_votes(self):
        '''
        Vote for right- or left-alignment. Store votes in self.h_phase_votes.
        Returns:
            None
        '''
        self.h_phase_votes = ((self.odd_cw_mean - self.even_cw_mean) *
                              self.vote_template) > 0
        return None

    def _right_aligned(self):
        '''
        Sets self.right_aligned:

        False first full row of cells is left-aligned
        True first full row of cells is right-aligned

        top row left-aligned:
                           #---------------------------#
            not            |   |     |     |     |     |
            considered --> |  / \   / \   / \   /      |
                           | /   \ /   \ /   \ /       |
            top row    --> ||     |     |     |    ... |
                           ||     |     |     |        |
                           | \   / \   / \   / \       |
                           |  \ /   \ /   \ /   \      |
            2nd row    --> |   |     |     |     | ... |
                           |   |     |     |     |     |
                 .         |  / \   / \   / \   /      |
                 .         | /   \ /   \ /   \ /       |
                 .         ||     |     |     |        |
                           |            .              |
                           |            .              |
                           |            .              |
                           #---------------------------#

        top row right-aligned:
                           #---------------------------#
            not            ||     |     |     |        |
            considered --> | \   / \   / \   / \       |
                           |  \ /   \ /   \ /   \      |
            top row    --> |   |     |     |     | ... |
                           |   |     |     |     |     |
                           |  / \   / \   / \   /      |
                           | /   \ /   \ /   \ /       |
            2nd row    --> ||     |     |     |    ... |
                           ||     |     |     |        |
                 .         | \   / \   / \   / \       |
                 .         |  \ /   \ /   \ /   \      |
                 .         |   |     |     |     |     |
                           |            .              |
                           |            .              |
                           |            .              |
                           #---------------------------#
        '''
        self.right_aligned = self.h_phase_votes.sum() < self.dim[1] / 2
        return None

    def _rows_cols(self):
        '''
        Locates cell rows and cell columns. Stores rows in self.rows, left-
        aligned columns in self.cols_l, and right-aligned columns in
        self.cols_r.
        Returns:
            None
        '''
        self.rows = np.arange(self.phase[1], self.dim[1] + 1, self.period[1])
        self.cols_l = np.arange(self.phase[0], self.dim[0] + 1,
                                self.period[0] * 2)
        self.cols_r = np.arange(self.phase[0] + self.period[0],
                                self.dim[0] + 1, self.period[0] * 2)
        return None

    def _boxes(self):
        '''
        Creates a nx4 array containing cell-bounding boxes, where n is the
        number of complete cells discovered. Stores in self.boxes.
        Returns:
            None
        '''
        row_height = self.period[0] * 2
        row_offset = 0

        la_row = self.right_aligned
        boxes = []
        for row_idx in range(len(self.rows)):
            row = self.rows[row_idx] + row_offset
            la_row = not la_row

            if la_row:
                cols = self.cols_l
            else:
                cols = self.cols_r

            for col in range(len(cols[:-1])):
                if row + row_height <= self.dim[1]:
                    box = np.array([cols[col], row, cols[col + 1],
                                   row + row_height])
                    boxes.append(box)

        self.boxes = np.vstack(boxes)
        return None

    def _upscale_boxes(self, upscale_factor):
        '''
        Increases the size of self boxes by upscale_factor, in order to ensure
        they enclose their corresponding cell.
        Args:
            upscale_factor - upscale factor for cell bounding boxes
        '''
        edge_pos_diff = int(upscale_factor * self.period[0])
        self.boxes[:, :2] -= edge_pos_diff
        self.boxes[:, 2:] += edge_pos_diff
        return None

    def _local_adjustment(self, rel_search_w, rel_filter_h, std_dev):
        '''
        Performs per-box local adjustment on self.boxes to ensure each box
        completely encloses its cell.
        Operates in place on self.boxes
        Args:
            rel_search_w - width of search box relative to box width
            rel_filter_h - height of search box relative to box width
            std_dev - standard deviation for Gaussian filter
        Returns:
            None
        '''
        search_w = int(self.period[0] * rel_search_w)
        filter_h = int(self.period[0] * rel_filter_h)

        b = self.boxes.copy()

        # Determine slice coordiantes for sampling image
        s_row_mid = b[:, 1] + (b[:, 3] - b[:, 1]) // 2
        tb_col_mid = b[:, 0] + (b[:, 2] - b[:, 0]) // 2

        l_coords = np.stack((s_row_mid - filter_h, s_row_mid + filter_h,
                             b[:, 0] - search_w, b[:, 0] + search_w),
                            axis=-1)

        r_coords = np.stack((s_row_mid - filter_h, s_row_mid + filter_h,
                             b[:, 2] - search_w, b[:, 2] + search_w),
                            axis=-1)

        t_coords = np.stack((b[:, 1] - search_w, b[:, 1] + search_w,
                             tb_col_mid - filter_h, tb_col_mid + filter_h),
                            axis=-1)

        b_coords = np.stack((b[:, 3] - search_w, b[:, 3] + search_w,
                             tb_col_mid - filter_h, tb_col_mid + filter_h),
                            axis=-1)

        # Fix edge effects
        pad_width = max(search_w, filter_h - self.period[0])
        padded = np.pad(self.norm, pad_width,
                        mode='constant', constant_values=np.nan)
        l_coords += pad_width
        r_coords += pad_width
        t_coords += pad_width
        b_coords += pad_width

        # Sample image, smooth and calculate adjustment
        for i in range(self.boxes.shape[0]):
            l_signal = gaussian_filter1d(
                    np.mean(padded[l_coords[i, 0]: l_coords[i, 1],
                                   l_coords[i, 2]: l_coords[i, 3]],
                            axis=0), std_dev)

            r_signal = gaussian_filter1d(
                    np.mean(padded[r_coords[i, 0]: r_coords[i, 1],
                                   r_coords[i, 2]: r_coords[i, 3]],
                            axis=0), std_dev)

            lr_sig = np.nanmean(np.stack((l_signal, r_signal), axis=0), axis=0)
            h_adjustment = np.argmax(lr_sig) - search_w

            t_signal = gaussian_filter1d(
                    np.mean(padded[t_coords[i, 0]: t_coords[i, 1],
                                   t_coords[i, 2]: t_coords[i, 3]],
                            axis=1), std_dev)

            b_signal = gaussian_filter1d(
                    np.mean(padded[b_coords[i, 0]: b_coords[i, 1],
                                   b_coords[i, 2]: b_coords[i, 3]],
                            axis=1), std_dev)

            tb_sig = np.nanmean(np.stack((t_signal, b_signal), axis=0), axis=0)
            v_adjustment = np.argmax(tb_sig) - search_w

            # Apply adjustment
            adjustment = np.array((h_adjustment, v_adjustment) * 2, dtype=int)
            self.boxes[i] += adjustment

    def _snap_boxes(self):
        '''
        Moves boxes which are outside image limits back into image.
        Returns:
            None
        '''
        x, y = self.dim

        self.boxes[:, [0, 2]] -= \
            self.boxes[:, [0, 0]] * (self.boxes[:, [0, 0]] < 0)
        self.boxes[:, [0, 2]] -= \
            (self.boxes[:, [2, 2]] - x) * (self.boxes[:, [2, 2]] > x)
        self.boxes[:, [1, 3]] -= \
            self.boxes[:, [1, 1]] * (self.boxes[:, [1, 1]] < 0)
        self.boxes[:, [1, 3]] -= \
            (self.boxes[:, [3, 3]] - y) * (self.boxes[:, [3, 3]] > y)

    def align_cells(self, img=None,
                    std_dev=1, upscale_factor=0.1,
                    rel_search_w=0.34, rel_filter_h=0.14):
        '''
        Process background image to find complete comb cells.
        Args:
            img - new image to process
            std_dev - standard deviation for gaussian smoothing. Default 1.
            upscale_factor - upscale factor for cell bounding boxes
            rel_search_w - width of search box relative to box width
                           for making local, per-box adjustment
            rel_filter_h - height of search box relative to box width
                           for making local, per-box adjustment
        Returns:
            None
        '''
        if img is not None:
            self.set_img(img)
        self._normalise()
        self._means()
        self._windowed_means()
        self._rffts()
        self._autocorrs()
        self._periods()
        self._phase_graphs(std_dev=std_dev)
        self._phases()
        self._vote_template()
        self._cw_means()
        self._h_phase_votes()
        self._right_aligned()
        self._rows_cols()
        self._boxes()
        self._upscale_boxes(upscale_factor / 2)
        self._snap_boxes()
        self._local_adjustment(rel_search_w, rel_filter_h, std_dev)
        self._upscale_boxes(upscale_factor / 2)
        self._snap_boxes()
        self.aligned = True
        return None

    def process(self, *args, **kwargs):
        '''
        Deprecated alias for align_cells
        '''
        return self.align_cells(*args, **kwargs)

    def set_img(self, img):
        '''
        Set image. self.align_cells() should then be called.
        Args:
            img - grayscale image
        Returns:
            None
        '''
        assert isinstance(img, np.ndarray), \
            'Invalid image. Must be ndarray'
        assert img.dtype == np.uint8, 'Invalid image. Must be np.uint8.'
        if len(img.shape) == 2:
            self.img = img
        elif len(img.shape) == 3:
            self.img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        else:
            raise ValueError('Invalid image. Must be depth 1 or 3.')
        self.aligned = False
        return None

    def imread(self, path):
        '''
        Load image from path. self.align_cells() should then be called.
        Args:
            path - path of background image file
        Returns:
            None
        '''
        img = cv2.imread(path)
        img_gr = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        self.set_img(img_gr)
        return None

    def get_cell(self, cell_idx, image=None, copy=False):
        '''
        Crops out and returns single cell image.
        Args:
            cell_idx - index of cell to extract
            image - image to extract from. Default None (use self.img)
            copy - if True, return a copy instead
        Returns:
            cell_img - cropped image of cell
        '''
        assert self.aligned, 'Try running .align_cells()'
        if image is None:
            image = self.img
        box = self.boxes[cell_idx]
        cell_img = image[box[1]:box[3], box[0]:box[2]]

        if copy is True:
            return cell_img.copy()
        else:
            return cell_img

    def get_cropped_images(self, image=None, copy=False):
        '''
        Crops out all cells and returns a list of images by iteratively calling
        self.get_cell().
        '''
        assert self.aligned, 'Try running .align_cells()'
        cell_images = []
        for box_idx in range(len(self.boxes)):
            cell_images.append(self.get_cell(box_idx, image=image, copy=copy))

        return cell_images

    def draw_boxes(self, image=None, cell_numbers=True):
        '''
        Draws pretty colours on a copy of the image
        Args:
            image - greyscale image to draw on. defaults to self.img
            cell_numbers - if True, draws on cell numbers
        Returns:
            img - drawn on image in BGR colourspace
        '''
        assert self.aligned, 'Try running .align_cells()'
        if image is None:
            img = cv2.cvtColor(self.img, cv2.COLOR_GRAY2BGR)
        else:
            img = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

        n_cells = len(self.boxes)

        hsv = np.ones((n_cells, 1, 3), dtype=np.uint8) * 255
        hsv[:, 0, 0] = np.linspace(0, 179, num=n_cells, dtype=np.uint8)
        colours = list(
                cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR).reshape((n_cells, 3)))

        for i in range(n_cells):
            c = colours[i]
            box = self.boxes[i]
            cv2.rectangle(img, (box[0], box[1]), (box[2], box[3]),
                          color=[int(c[i]) for i in (0, 1, 2)], thickness=3)
            if cell_numbers is True:
                cv2.putText(img, '%02d' % i, (box[0] + 10, box[3] - 10),
                            cv2.FONT_HERSHEY_SIMPLEX, 2,
                            [int(c[i]) for i in (0, 1, 2)], thickness=3)

        return img

    def get_boxes_mask(self):
        '''
        Returns a boolean array of shape self.img.shape, with True for pixels
        which lie inside at least one box and False elsewhere.
        Returns:
            np.ndarray
        '''
        mask = np.zeros(self.img.shape, dtype=bool)
        for box in self.boxes:
            mask[box[1]:box[3], box[0]:box[2]] = True

        return mask


def get_boxes(img):
    '''
    Detects and aligns cells, and returns bounding boxes array
    Args:
        img - passed to CombAligner()
    Returns:
        boxes - array with shape (n, 4), where n is the nuber of complete cells
    '''
    aligner = CombAligner(img)
    aligner.align_cells()

    return aligner.boxes


def sample():
    # path = '/home/jesse/incubator02/2016-03-31-11-10/' + \
    #         '2016-04-01-10-13-26-747529921.h264.larvae.jpg'
    path = '/home/jesse/Pictures/bg_img_example.png'
    outpath = '/home/jesse/honours/figures/img/'
    aligner = CombAligner()
    aligner.imread(path)
    aligner.process()
    cv2.imwrite(outpath + 'comb_alignment.png', aligner.draw_boxes())

    axis_titles = ['Horizontal axis', 'Vertical axis']
    means_fig = plt.figure()
    means_fig.suptitle('Normalised Mean Image Intensity')
    for i in (0, 1):
        ax = means_fig.add_subplot(2, 1, i + 1, title=axis_titles[i])
        ax.axhline(y=0, color='k')
        ax.plot(aligner.mean[i], label='Mean')
        ax.plot(hann(aligner.dim[i]), label='Hann Window')
        ax.plot(aligner.windowed[i], label='Windowed Mean')
        ax.legend()

    plt.savefig(outpath + 'comb_align_mean_intesity.pdf', format='pdf')

    autocorr_fig = plt.figure()
    autocorr_fig.suptitle('Autocorrelation')
    for i in (0, 1):
        ax = autocorr_fig.add_subplot(2, 1, i + 1, title=axis_titles[i])
        ax.plot(aligner.autocorr[i].real, label='Real')
        ax.plot(aligner.autocorr[i].imag, label='Imaginary')
        ax.axvline(x=aligner.period[i], color='r', label='Period')
        ax.axhline(y=0, color='k')
        ax.legend()

    plt.savefig(outpath + 'comb_align_autocorrelation.pdf', format='pdf')

    phase_fig = plt.figure()
    phase_fig.suptitle('Phase Calcualtion')
    for i in (0, 1):
        ax = phase_fig.add_subplot(2, 1, i + 1, title=axis_titles[i])
        ax.plot(aligner.phase_graph[i], label='Average Bin')
        ax.plot(aligner.phase_graph_smooth[i], label='Smoothed')
        ax.axvline(x=aligner.phase[i], label='Phase', color='r')
        ax.axhline(y=0, color='k')
        ax.legend()

    plt.savefig(outpath + 'comb_align_phase.pdf', format='pdf')

    vote_fig = plt.figure()
    vote_ax = vote_fig.add_subplot('111', title='Alignment Voting')
    vote_ax.fill_between(range(len(aligner.vote_template)), -1,
                         aligner.vote_template, label='template', color='Grey')
    vote_ax.fill_between(range(len(aligner.h_phase_votes)), 0,
                         aligner.h_phase_votes, label='votes', color='k')
    vote_ax.plot(aligner.even_cw_mean, label='even')
    vote_ax.plot(aligner.odd_cw_mean, label='odd')
    vote_ax.legend()

    plt.savefig(outpath + 'comb_align_vote.pdf', format='pdf')

    cells = aligner.get_cropped_images()
    boxes = aligner.draw_boxes()

    boxes_fig = plt.figure()
    boxes_ax = boxes_fig.add_subplot('111', title='Boxed Cells')
    boxes_ax.imshow(boxes)
    boxes_ax.get_xaxis().set_ticks([])
    boxes_ax.get_yaxis().set_ticks([])

    plt.savefig(outpath + 'comb_align_boxes.pdf', format='pdf')

    norm_fig = plt.figure()
    norm_ax = norm_fig.add_subplot('111', title='Normalised Image')
    norm_ax.imshow(aligner.norm, cmap='Greys_r')
    norm_ax.get_xaxis().set_ticks([])
    norm_ax.get_yaxis().set_ticks([])

    plt.savefig(outpath + 'comb_align_norm.pdf', format='pdf')

    cells_fig = plt.figure()
    cells_fig.suptitle('Cropped Cells')
    i = 1
    for cell in cells:
        ax = cells_fig.add_subplot(
            6, len(cells) // 6 + bool(len(cells) % 6), i)
        ax.imshow(cell, cmap='Greys_r')
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
        i += 1

    plt.savefig(outpath + 'comb_align_cropped.pdf', format='pdf')


def main():
    sample()

if __name__ == '__main__':
    main()
