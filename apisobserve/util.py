#!/usr/bin/env python

from multiprocessing import pool, BoundedSemaphore, util, get_context
import numpy as np
import queue
import os
import threading


class Pool(pool.Pool):
    '''
    Better version of multiprocessing.Pool which supports a maxsize argument
    passed to the Pool's _taskqueue.
    '''
    def __init__(self, processes=None, initializer=None, initargs=(),
                 maxtasksperchild=None, context=None, maxsize=None):
        self._ctx = context or get_context()
        self._setup_queues()
        self._taskqueue = queue.Queue(maxsize=maxsize)
        self._cache = {}
        self._state = pool.RUN
        self._maxtasksperchild = maxtasksperchild
        self._initializer = initializer
        self._initargs = initargs

        if processes is None:
            processes = os.cpu_count() or 1
        if processes < 1:
            raise ValueError("Number of processes must be at least 1")

        if initializer is not None and not callable(initializer):
            raise TypeError('initializer must be a callable')

        self._processes = processes
        self._pool = []
        self._repopulate_pool()

        self._worker_handler = threading.Thread(
            target=Pool._handle_workers,
            args=(self, )
            )
        self._worker_handler.daemon = True
        self._worker_handler._state = pool.RUN
        self._worker_handler.start()


        self._task_handler = threading.Thread(
            target=Pool._handle_tasks,
            args=(self._taskqueue, self._quick_put, self._outqueue,
                  self._pool, self._cache)
            )
        self._task_handler.daemon = True
        self._task_handler._state = pool.RUN
        self._task_handler.start()

        self._result_handler = threading.Thread(
            target=Pool._handle_results,
            args=(self._outqueue, self._quick_get, self._cache)
            )
        self._result_handler.daemon = True
        self._result_handler._state = pool.RUN
        self._result_handler.start()

        self._terminate = util.Finalize(
            self, self._terminate_pool,
            args=(self._taskqueue, self._inqueue, self._outqueue, self._pool,
                  self._worker_handler, self._task_handler,
                  self._result_handler, self._cache),
            exitpriority=15
            )

    def _join_exited_workers(self):
        """Cleanup after any worker processes which have exited due to reaching
        their specified lifetime.  Returns True if any workers were cleaned up.
        """


def abs_diff_uint(a, b):
    '''
    Calculate the absolute difference of two arrays
    Args:
        a, b - arrays of same shape with np.uint* dtype
    Returns:
        diff - array of same shape and dtype as a
    '''
    if a.shape != b.shape:
        raise ValueError(
            'a and b must have same shape (broadcasting not implemented)')

    mask = a > b
    diff = np.empty_like(a)

    diff[mask] = a[mask] - b[mask]
    diff[~mask] = b[~mask] - a[~mask]

    return diff
