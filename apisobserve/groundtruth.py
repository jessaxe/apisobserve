#!/usr/bin/env python
# Provides utilities for digitising video data to provide ground-truth data
# for assessing software

import cv2
import glob
import numpy as np

from apisobserve.tagid import ReferenceTagDrawer


class AnnotatedFrame:
    '''
    Provides methods for manually annotating a frame of video with tag info.
    '''
    def __init__(self, frame, fr_idx=0):
        '''
        Args:
            frame - numpy array containing image to annotate
        '''
        self.frame = frame
        self.fr_idx = fr_idx

        self.data = []
        self.vdata = []
        self.draw_frame()

    def add_data(self, x, y, r, angle, tag_id):
        self.data.append([self.fr_idx, x, y, r, angle, tag_id])

    def add_vdata(self, x, y, r, angle, tag_id, quality):
        self.vdata.append([self.fr_idx, x, y, r, angle, tag_id, quality])

    def undo(self):
        try:
            del self.data[-1]
        except IndexError:
            pass

    def remove_tag(self, selection_x, selection_y):
        for i in range(len(self.data)):
            fr_idx, x, y, r, angle, tag_id = self.data[i]
            if (x - selection_x) ** 2 + (y - selection_y) ** 2 <= r ** 2:
                del self.data[i]
                break

    def load_data(self, path):
        '''
        Load data from file
        '''
        with open(path, 'r') as f:
            for line in f:
                ll = line.split()
                try:
                    if int(ll[0]) == self.fr_idx:
                        self.add_data(int(ll[1]), int(ll[2]), int(ll[3]),
                                      float(ll[4]), int(ll[5]))
                except ValueError:
                    pass  # Skip line (header or wrong type of data)

    def load_vdata(self, path):
        '''
        Load validation data from file
        '''
        with open(path, 'r') as f:
            for line in f:
                ll = line.split()
                try:
                    if int(ll[0]) == self.fr_idx:
                        self.add_data(int(ll[1]), int(ll[2]), int(ll[3]),
                                      float(ll[4]), int(ll[5]), float(ll[6]))
                except ValueError:
                    pass  # Skip line (header or wrong type of data)

    def draw_frame(self):
        img = self.frame.copy()

        for fr_idx, x, y, r, angle, tag_id, quality in self.vdata:
            draw_tag(img, x, y, r, angle, tag_id, just_circle=True)

        for fr_idx, x, y, r, angle, tag_id in self.data:
            draw_tag(img, x, y, r, angle, tag_id)

        self.img = img

    def print_data(self, sep='\t', **kwargs):
        '''
        Outputs data, correctly formatted. Passes keyword arguments to print()
        '''
        for l in self.data:
            print(*l, sep=sep, **kwargs)


class GUI:
    def __init__(self, output_file=None):
        self.output_file = output_file

        cv2.namedWindow('tag', flags=cv2.WINDOW_NORMAL)
        cv2.namedWindow('frame', flags=cv2.WINDOW_NORMAL)

        cv2.resizeWindow('frame', 800, 600)

        cv2.setMouseCallback('frame', self.mouse_callback)
        cv2.setMouseCallback('tag', self.tag_mouse_callback)

        self.frame_mouse_active = True

        self.x = 0
        self.y = 0
        self.r = 50
        self.angle = 0
        self.tag_id = -1

        self.pf_data = []

        self.tag_img = np.zeros((256, 256, 3), dtype=np.uint8)
        self.undrawn_tag = self.tag_img.copy()
        self.reference_tag = ReferenceTagDrawer().get_mask(
                self.tag_img.shape[:2])

        pixel_idxs = []
        n_pixels = []
        for i in range(14):
            pixel_idxs.append(np.where(self.reference_tag == i))
            n_pixels.append(len(pixel_idxs[-1][0]))
        self.bsize = min(n_pixels)
        rr = np.concatenate([pixel_idxs[i][0][:self.bsize] for i in range(14)])
        cc = np.concatenate([pixel_idxs[i][1][:self.bsize] for i in range(14)])

        self.rrcc = rr, cc
        self.id_template = (2 ** (13 - np.arange(14))).astype(np.uint16)

    def __call__(self):
        while True:
            self.annotated_frame.draw_frame()
            self.frame = self.annotated_frame.img.copy()
            self.bound_tag()
            draw_tag(self.frame, self.x, self.y, self.r, self.angle,
                     self.tag_id)
            self.extract_tag()
            self.show_tag()
            cv2.imshow('frame', self.frame)
            cv2.imshow('tag', self.tag_img)
            k = cv2.waitKey(20)

            if k == 27:  # ESC
                return 27

            elif k == 10:  # ENTER
                self.annotated_frame.add_data(self.x, self.y, self.r,
                                              self.angle, self.tag_id)
                self.next_tag()
                self.frame_mouse_active = False

            elif k == 32:  # SPACE
                self.pf_data = self.annotated_frame.data.copy()
                self.annotated_frame.print_data(file=self.output_file)
                self.next_tag()
                break

            elif k == 92:  # \
                self.next_tag()

            elif k == ord('r'):
                self.annotated_frame.remove_tag(self.x, self.y)

            elif k == ord('u'):
                self.annotated_frame.undo()
                self.frame_mouse_active = True

            elif k == ord('n'):
                self.frame_mouse_active = False

            elif k == ord('m'):
                self.frame_mouse_active = True

            elif k == ord('p'):
                self.predict_id()

            elif k == ord('j'):
                self.r += 1

            elif k == ord('k'):
                self.r -= 1

            elif k == ord('h'):
                self.angle -= 1

            elif k == ord('H'):
                self.angle -= 10

            elif k == ord('l'):
                self.angle += 1

            elif k == ord('L'):
                self.angle += 10

            elif k == ord('d'):
                self.y -= 1

            elif k == ord('D'):
                self.y -= 10

            elif k == ord('f'):
                self.y += 1

            elif k == ord('F'):
                self.y += 10

            elif k == ord('s'):
                self.x -= 1

            elif k == ord('S'):
                self.x -= 10

            elif k == ord('g'):
                self.x += 1

            elif k == ord('G'):
                self.x += 10

            elif k == 0xFF:
                pass

            elif k == 225:
                pass

            else:
                print('Unbound keypress:', k, "{}".format(chr(k)))

    def next_tag(self):
        if len(self.pf_data) == 0:
            self.tag_id = -1
        else:
            i, self.x, self.y, self.r, self.angle, self.tag_id = \
                    self.pf_data.pop()

    def mouse_callback(self, event, x, y, flags, userdata):
        '''
        Mouse callback
        '''
        if self.frame_mouse_active is False:
            return None

        if event == cv2.EVENT_MOUSEMOVE:
            if flags & cv2.EVENT_FLAG_LBUTTON:
                angle = np.arctan2((x - self.x), (self.y - y)) * 180 / np.pi
                self.angle = angle

            elif flags & cv2.EVENT_FLAG_MBUTTON:
                self.r = self.r0 + (self.y - y) // 10
                if self.r < 1:
                    self.r = 1

            else:
                self.x = x
                self.y = y

        elif event == cv2.EVENT_LBUTTONDOWN:
            pass

        elif event == cv2.EVENT_RBUTTONDOWN:
            self.frame_mouse_active = False

        elif event == cv2.EVENT_MBUTTONDOWN:
            self.r0 = self.r

        elif event == cv2.EVENT_LBUTTONUP:
            pass

        elif event == cv2.EVENT_RBUTTONUP:
            pass

        else:
            pass

    def tag_mouse_callback(self, event, x, y, flags, userdata):
        '''
        '''
        if event == cv2.EVENT_LBUTTONDOWN:
            hex_idx = 13 - self.reference_tag[y, x]
            if 0 > hex_idx < 13:
                return None
            self.tag_id = self.tag_id | (2 ** hex_idx)

        elif event == cv2.EVENT_RBUTTONDOWN:
            hex_idx = 13 - self.reference_tag[y, x]
            if 0 > hex_idx < 13:
                return None
            self.tag_id = self.tag_id & ((2 ** 14 - 1) ^ (2 ** hex_idx))

    def predict_id(self):
        thr_val, thr_img = cv2.threshold(
            self.undrawn_tag[..., 0], 0, 255,
            cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        bin_proportions = np.mean(
            self.undrawn_tag[..., 0][self.rrcc].reshape((14, self.bsize)),
            axis=1)
        tag_id_bin = bin_proportions > 127
        if np.array_equal(tag_id_bin[:5], np.array([1, 0, 0, 1, 0],
                          dtype=bool)):
            id_number = np.sum(self.id_template[tag_id_bin], dtype=np.uint16)
        else:
            id_number = 0

        self.tag_id = id_number

    def set_frame(self, annotated_frame):
        self.annotated_frame = annotated_frame
        self.frame = self.annotated_frame.img.copy()

    def bound_tag(self):
        '''
        Make sure tag is completely visible
        '''
        if self.r >= min(self.annotated_frame.frame.shape[:2]) // 2:
            self.r = min(self.annotated_frame.frame.shape[:2]) // 2 - 1
        elif self.r < 1:
            self.r = 1

        if self.x < self.r:
            self.x = self.r
        elif self.x > self.annotated_frame.frame.shape[1] - self.r:
            self.x = self.annotated_frame.frame.shape[1] - self.r

        if self.y < self.r:
            self.y = self.r
        elif self.y > self.annotated_frame.frame.shape[0] - self.r:
            self.y = self.annotated_frame.frame.shape[0] - self.r

    def extract_tag(self):
        tag_img = self.annotated_frame.frame[self.y - self.r: self.y + self.r,
                                             self.x - self.r: self.x + self.r]
        m = cv2.getRotationMatrix2D(
            (tag_img.shape[1] / 2, tag_img.shape[0] / 2), self.angle, 1)
        self.small_tag_img = cv2.warpAffine(tag_img, m, tag_img.shape[:2],
                                            flags=cv2.INTER_LINEAR)

        self.tag_img[...] = cv2.resize(
            self.small_tag_img, self.tag_img.shape[:2],
            interpolation=cv2.INTER_NEAREST)
        self.undrawn_tag = self.tag_img.copy()

    def show_tag(self):
        self.tag_img[self.reference_tag == 254] = 0

        if self.tag_id >= 0:
            bin_id = '{:0>14s}'.format(bin(self.tag_id)[2:][-14:])
            for i in range(14):
                rr, cc = np.where(self.reference_tag == i)
                rr = rr[::5]
                cc = cc[::5]

                self.tag_img[rr, cc] = int(bin_id[i]) * 255

        return None


def draw_tag(img, x, y, r, angle, tag_id, just_circle=False):
    '''
    Draws a tag on a frame (in place)
    '''
    if just_circle is True:
        cv2.circle(img, (x, y), r, (0, 127, 127), thickness=3)
        return None

    cv2.circle(img, (x, y), r, (255, 0, 0), thickness=3)
    if np.isfinite(angle):
        cv2.line(img, (x, y),
                 (int(x + r * np.sin(np.pi * angle / 180)),
                  int(y - r * np.cos(np.pi * angle / 180))),
                 (0, 0, 255), thickness=3)
    cv2.putText(img, '%d' % tag_id, (x + r, y),
                cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), thickness=3)

    return None


def process_frames(frame_list, output_file, preload_data_file=None):
    '''
    Args:
        frame_list - list of paths to frame image files
        output_file - file to write output to (different to preload_data_file)
        preload_data_file - load previously recorded data
    '''
    gui = GUI(output_file=output_file)

    for i in range(len(frame_list)):
        frame = cv2.imread(frame_list[i])
        annotated_frame = AnnotatedFrame(frame, fr_idx=i)
        if preload_data_file is not None:
            annotated_frame.load_data(preload_data_file)

        gui.set_frame(annotated_frame)
        k = gui()
        if k == 27:
            print('7')
            break


if __name__ == '__main__':
    frame_list = glob.glob(
        '/home/jesse/apisobservedatavalidation/frames/incubator01/856200/*.png')
    frame_list.sort()
    with open(
        '/home/jesse/apisobservedatavalidation/groundtruth', 'w') as output_file:
        process_frames(frame_list, output_file)

    # cap = cv2.VideoCapture('/home/jesse/Videos/sample_tags.mp4')
    # for i in range(100):
    #     ret, fr = cap.read()

    # af = AnnotatedFrame(fr)
    # gui = GUI()
    # gui.set_frame(af)

    # gui()
