#!/usr/bin/env python

import cv2
import numpy as np
import os
from skimage import draw

from apisobserve import tagid


class BatchFrameProcessor():
    '''
    Processes batches of frames to extract background images and track bees.
    '''
    def __init__(self, outputdir, tag_detector=None, lbptag_detector=None,
                 farneback_args=(0.5, 3, 51, 1, 7, 1.5, 0), v_thresh=0.25,
                 smth_k=25, tag_dataset_dir='default'):
        '''
        Args:
            outputdir - directory where output is to be stored
            tag_detector - tagid.TagDetector or argument dictionary
            lbptag_detector - tagid.LBPTagDetector or argument dictionary
            farneback_args - args passed to cv2.calcOpticalFlowFarneback
            v_thresh - velocity threshold for Farneback motion
            smth_k - kernel size for smoothing lbd similarity images in
                background reconstruction
            tag_dataset_dir - directory where tag dataset is stored for fixing
                tag detections
        '''
        if os.path.isdir(outputdir):
            self.outputdir = outputdir
            self.bgdir = os.path.join(outputdir, 'bg')
            os.makedirs(self.bgdir, exist_ok=True)
            self.tagdir = os.path.join(outputdir, 'tag')
            os.makedirs(self.tagdir, exist_ok=True)
            os.makedirs(os.path.join(self.bgdir, 'weights'), exist_ok=True)
        else:
            raise ValueError('outputdir must be a real directory')

        if tag_dataset_dir == 'default':
            tag_dataset_dir = os.path.join(os.path.dirname(__file__),
                                           'detectioncorrection')

        if tag_detector is None:
            self.tag_detector = tagid.TagDetector(
                tag_dataset_dir=tag_dataset_dir)
        elif isinstance(tag_detector, dict):
            self.tag_detector = tagid.TagDetector(**tag_detector)
        elif isinstance(tag_detector, tagid.TagDetector):
            self.tag_detector = tag_detector
        else:
            raise ValueError('Invalid tag_detector argument.')

        if lbptag_detector is None:
            self.lbptag_detector = tagid.LBPTagDetector()
        elif isinstance(lbptag_detector, dict):
            self.lbptag_detector = tagid.LBPTagDetector(**lbptag_detector)
        elif isinstance(lbptag_detector, tagid.LBPTagDetector):
            self.lbptag_detector = lbptag_detector
        else:
            raise ValueError('Invalid lbptag_detector argument.')

        if len(farneback_args) != 7:
            raise ValueError(
                'Wrong number of farneback_args. Got %d expected 7' %
                len(farneback_args))

        self.farneback_args = farneback_args
        self.v_thresh = v_thresh

        self.smth_k = smth_k

    def process_frames(self, img_array, name_str):
        '''
        Args:
            img_array - array of video frames
            name_str - for file naming
        '''
        # Background Reconstruction
        lbp_array = np.empty(
            (img_array.shape[0], (img_array.shape[1] - 2) // 2,
             (img_array.shape[2] - 2) // 2), dtype=np.uint8)

        for i in range(img_array.shape[0]):
            lbp_array[i, ...] = tagid.fast_reducing_lbp(img_array[i, ...])

        eq_array = (lbp_array[:-1] == lbp_array[1:]).astype(np.uint8)
        eq_array = (eq_array[:-1] + eq_array[1:]) * 127

        weights_sum = np.zeros(img_array.shape[1:], dtype=np.float64)
        bg_img = np.zeros(img_array.shape[1:], dtype=np.float64)

        # Tag Detection and Identification
        tag_path = os.path.join(self.tagdir, name_str + '.csv')
        tag_fmt_str = '{}\t{}\t{}\t{}\t{}\t{}\t{}\n'
        lbp_path = os.path.join(self.tagdir, name_str + '.lbp.csv')
        lbp_fmt_str = '{}\t{}\t{}\n'

        with open(tag_path, 'w') as tag_f:
            with open(lbp_path, 'w') as lbp_f:
                frame_idx = -1
                for frame in img_array:
                    frame_idx += 1
                    tag_list = self.tag_detector.detect_and_identify(frame)
                    lbp_tags = self.lbptag_detector.detect(
                        frame, tag_list, lbp_img=lbp_array[frame_idx])

                    tag_f.writelines(tag_fmt_str.format(frame_idx, *tag)
                                     for tag in tag_list)

                    lbp_f.writelines(lbp_fmt_str.format(frame_idx, *tag)
                                     for tag in lbp_tags)

                    # Background Reconstruction
                    if frame_idx > 1 and frame_idx < eq_array.shape[0] - 1:
                        # Mask out tags
                        for tag in tag_list:
                            x, y, r = tag[:3]
                            rr, cc = draw.circle(y // 2, x // 2, r // 2,
                                                 shape=eq_array.shape[1:])
                            eq_array[frame_idx - 1, rr, cc] = 0

                        weights = tagid.resize_lbp(cv2.GaussianBlur(
                            eq_array[frame_idx - 1, ...],
                            (self.smth_k, self.smth_k), 0)) / 255
                        weights *= weights

                        weights_sum += weights
                        bg_img += img_array[frame_idx, ...] * weights

        weights_mask = weights_sum != 0
        bg_img[weights_mask] /= weights_sum[weights_mask]

        weights_sum[weights_sum > 255] = 255

        cv2.imwrite(os.path.join(self.bgdir, name_str + '.png'),
                    bg_img.astype(np.uint8))
        cv2.imwrite(os.path.join(self.bgdir, 'weights', name_str + '.png'),
                    weights_sum.astype(np.uint8))

        return None
