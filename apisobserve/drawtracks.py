#!/usr/bin/env python

import collections
import cv2
import numpy as np


class TagBuffer(dict):
    '''
    Class for storing tag location tails
    '''
    def add_tag(self, tag):
        '''
        Add a tag to the buffer
        Args:
            tag - a tracklets.tracklet_dtype value
        '''
        if tag['tag_id'] in self:
            self[tag['tag_id']].appendleft(tag)
        else:
            self[tag['tag_id']] = collections.deque([tag])

    def remove_old(self, fr_idx):
        '''
        Remove last tag in each track if it is older than fr_idx
        Args:
            fr_idx - int
        '''
        for tag_id in set(self):
            tag = self[tag_id].pop()
            if tag['fr_idx'] >= fr_idx:
                self[tag_id].append(tag)

            if len(self[tag_id]) == 0:
                self.pop(tag_id)


class TagDrawer():
    '''
    Class for drawing tag detections
    '''
    def __init__(self, cap, track_loader, out_path, tail_length=5, fps=5,
                 out_size=(1640, 1232)):
        '''
        Args:
            cap - binnedcap.ArrayVideoCapture instance
            track_loader - trackproc.FrTrackLoader instance
        '''
        self.cap = cap
        self.track_loader = track_loader
        self.tail_length = tail_length

        self.tag_buffer = TagBuffer()

        self.writer = cv2.VideoWriter(
            out_path, cv2.VideoWriter_fourcc(*'X264'), fps, out_size,
            isColor=True)
        self.scale_factor = min(out_size[0] / self.cap.width,
                                out_size[0] / self.cap.height)
        self.out_size = out_size

    def draw_tails(self, frame):
        '''
        Draw tails on frame based on information in self.tag_buffer
        Args:
            frame - image to draw on (with colour-depth 3)
        '''
        for tag_id in self.tag_buffer:
            tail = self.tag_buffer[tag_id]
            for i in range(len(tail) - 1):
                tag0 = tail[i]
                tag1 = tail[i + 1]
                cv2.line(frame,
                         (int(tag0['x']), int(tag0['y'])),
                         (int(tag1['x']), int(tag1['y'])),
                         (0, 255, 255), thickness=5)

        return None

    def draw_tags(self, frame, tags):
        '''
        Draws tag annotations on frame
        Args:
            frame - image to draw on
            tags - tracklets.tracklet_dtype array containing tags to draw
        '''
        for tag in tags:
            cv2.circle(frame, (tag['x'], tag['y']), tag['r'],
                       (255, 0, 0), thickness=3)
            if np.isfinite(tag['angle']):
                cv2.line(frame, (tag['x'], tag['y']),
                         (int(tag['x'] + tag['r'] *
                              np.sin(np.pi * tag['angle'] / 180)),
                          int(tag['y'] - tag['r'] *
                              np.cos(np.pi * tag['angle'] / 180))),
                         (0, 0, 255), thickness=3)
            cv2.putText(frame, '%d' % tag['tag_id'],
                        (tag['x'] + tag['r'], tag['y']),
                        cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), thickness=3)

        return None

    def next_frame(self):
        '''
        Load next frame, draw annotations on it, and reencode.
        Returns:
            ret - True if success, False if read failed (EOF)
        '''
        ret, frame = self.cap.read()
        if ret is False:
            return ret

        # Select track data for this frame
        tags = self.track_loader[self.cap.tot_fr_idx]

        # Update self.tag_buffer
        if self.tail_length > 0:
            for tag in tags:
                self.tag_buffer.add_tag(tag)

            self.tag_buffer.remove_old(self.cap.tot_fr_idx - self.tail_length)

        # Draw tails
        self.draw_tails(frame)

        # Draw tags
        self.draw_tags(frame, tags)

        # Resize and write frame
        resized = cv2.resize(frame, self.out_size, self.scale_factor,
                             self.scale_factor, interpolation=cv2.INTER_AREA)
        self.writer.write(resized)

        return ret

    def release(self):
        '''
        Releases self.cap and self.writer
        '''
        self.cap.release()
        self.writer.release()

        return None


def main():
    pass

if __name__ == '__main__':
    main()
