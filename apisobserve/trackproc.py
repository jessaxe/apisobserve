# Provides FrTrackLoader class

import datetime as dt
from multiprocessing import Pool
import os

import numpy as np


class FrTrackLoader():
    '''
    Provides methods for loading tag data from a series of track csv files
    into track arrays. Can be indexed by fr_idx. Previously loaded array is
    stored in a cache, making sequential indexing the most efficient.
    '''
    track_dtype = np.dtype([
            ('fr_idx', np.int64),
            ('x', np.int32),
            ('y', np.int32),
            ('r', np.uint8),
            ('angle', np.float32),
            ('tag_id', np.uint16),
            ('quality', np.float32),
            ('track_id', np.int64)
        ])

    def __init__(self, files, fr_step):
        '''
        Args:
            files - list of track files
            fr_step - number of frames in each file
        '''
        self.files = files
        self.fr_step = fr_step
        self._cached_file_idx = None
        self._cachedarr = None

    def __len__(self):
        return len(self.files) * self.fr_step

    def _load_file(self, file_idx):
        if file_idx != self._cached_file_idx:
            self._cachedarr = np.loadtxt(self.files[file_idx],
                                         dtype=self.track_dtype,
                                         skiprows=1)
            if np.any(self._cachedarr['fr_idx'] >= self.fr_step):
                raise IndexError('''{:s} contains a larger fr_idx than
                                 self.fr_step ({:d})'''.format(
                                     self.files[file_idx], self.fr_step))

            self._cachedarr['fr_idx'] += file_idx * self.fr_step
            self._cached_file_idx = file_idx

        return self._cachedarr

    def __getitem__(self, key):
        if isinstance(key, slice):
            if key.start is None:
                start_file_idx = 0
            else:
                start_file_idx = key.start // self.fr_step

            if key.stop is None:
                stop_file_idx = len(self.files) - 1
            else:
                stop_file_idx = key.stop // self.fr_step

            arr = np.concatenate(
                [self._load_file(idx) for idx in range(start_file_idx,
                                                       stop_file_idx + 1)])
            mask = (arr['fr_idx'] >= key.start) & (arr['fr_idx'] < key.end)

        else:
            file_idx = key // self.fr_step
            arr = self._load_file(file_idx)
            mask = arr['fr_idx'] == key

        return arr[mask]

    def __iter__(self):
        for i in range(len(self)):
            yield self[i]
