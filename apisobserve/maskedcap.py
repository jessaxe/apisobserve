#!/usr/bin/env python

from .binnedcap import BinnedVideoCapture
from .mvsegment import load_segmentation

import cv2
import numpy as np
from numpy import ma
import os
import warnings


class BinnedMaskedVideoCapture(BinnedVideoCapture):
    '''
    Loads masked frames into bins for batch processing
    '''
    def __init__(self, *filenames, **kwargs):
        '''
        Args:
            filenames - Paths to video files (sequential order). Videos are
                assumed to follow on from eachother without a gap.
            **kwargs - passed to BinnedVideoCapture().__init__()
        '''
        super().__init__(*filenames, **kwargs)
        self.segm_filenames = [os.path.splitext(filename)[0] + '.segm'
                               for filename in self.filenames]
        self.segm_file_idx = -1
        self._next_segm_file()
        self.awaiting_segmentation = True
        self.mask_array = np.empty(
            (self.bin_size,) + self.segm_array.shape[1:], dtype=np.uint8)
        self.fullres_mask = np.empty(self.img_array.shape, dtype=bool)

    def _check_segm_files(self):
        '''
        Check if each file in self.segm_filenames exists
        Raises:
            FileNotFoundError
        '''
        for segm_filename in self.segm_filenames:
            if os.path.isfile(segm_filename):
                pass
            else:
                raise FileNotFoundError('''Segmentation file %s does not exist.
                    make sure segmenation has been run.''' % segm_filename)

    def _next_segm_file(self):
        '''
        Loads next segmentation file.
        '''
        self.segm_file_idx += 1
        self.segm_fr_idx = 0
        self.current_segm_file = self.segm_filenames[self.segm_file_idx]
        self.segm_array = load_segmentation(self.current_segm_file)

        if self.segm_array.shape[0] < self.bin_size:
            raise NotImplementedError(
                'bin_size (%d) is greater than the number of frames in %s' %
                (self.bin_size, self.filenames[self.file_idx - 1]))

        return None

    def read_frames(self, return_array=True):
        '''
        Grabs, decodes, converts to greyscale and stores the next self.bin_size
        frames in the video sequence. Frames are stored in self.img_array.
        Sets self.awaiting_segmentation to True.
        Args:
            return_array - if False, None is returned
        Returns:
            [img_array] - numpy.ndarray, array of images (f, h, w)
        Raises:
            NotImplementedError - Cannot currently handle the case where an
                entire video is spanned by this method
        '''
        super().read_frames()

        self.awaiting_segmentation = True

        if return_array is True:
            return self.img_array
        else:
            return None

    def read_segmentation(self):
        '''
        Reads the segmentation for the current/next set of frames. Stores in
        self.mask_array. Sets self.awaiting_segmentation to False.
        '''
        assert self.running, 'End of file sequence has been reached.'
        if self.awaiting_segmentation is False:
            if self.file_fr_idx != self.segm_fr_idx:
                warnings.warn('Already have segmentation!')
                print('\nfile_fr_idx: %d, segm_fr_idx: %d' %
                      (self.file_fr_idx, self.segm_fr_idx))
                return None

            if self.segm_array.shape[0] - self.file_fr_idx > self.bin_size:
                # All in this file
                self.mask_array[...] = self.segm_array[
                    self.file_fr_idx:self.file_fr_idx + self.bin_size, ...]
                self.segm_fr_idx += self.bin_size
            elif self.segm_file_idx < len(self.segm_filenames) - 1:
                # Load next file
                end_segm = self.segm_array[self.file_fr_idx:, ...]
                remaining = self.bin_size - end_segm.shape[0]
                self._next_segm_file()
                self.mask_array[...] = np.concatenate(
                    (end_segm, self.segm_array[:remaining, ...]), axis=0)
                self.segm_fr_idx += self.bin_size - remaining
            else:
                # End of file sequence
                self.running = False
                return None

        else:  # self.awaiting_segmentation is True
            if self.file_fr_idx >= self.bin_size:
                # All in this file
                if self.file_fr_idx - self.bin_size != self.segm_fr_idx:
                    warnings.warn('Already have segmentation!')
                    print('\nfile_fr_idx: %d, segm_fr_idx: %d' %
                          (self.file_fr_idx, self.segm_fr_idx))
                    return None

                self.mask_array[...] = self.segm_array[
                    self.file_fr_idx - self.bin_size:self.file_fr_idx, ...]
                self.segm_fr_idx += self.bin_size

            else:
                # Load next file
                if self.bin_size - self.file_fr_idx != \
                        self.segm_array.shape[0] - self.segm_fr_idx:
                    warnings.warn('Already have segmentation!')
                    print('\nfile_fr_idx: %d, segm_fr_idx: %d' %
                          (self.file_fr_idx, self.segm_fr_idx))
                    return None

                remaining = self.bin_size - self.file_fr_idx
                end_segm = self.segm_array[-remaining:, ...]
                self._next_segm_file()
                self.mask_array[...] = np.concatenate(
                    (end_segm, self.segm_array[:self.file_fr_idx, ...]),
                    axis=0)
                self.segm_fr_idx += self.bin_size - remaining

        if self.segm_fr_idx == self.segm_array.shape[0]:
            self._next_segm_file()

        self.awaiting_segmentation = False

        return None

    def apply_segmentation(self):
        '''
        Resize self.mask_array, (store in self.fullres_mask) and apply
        segmentation to self.img_array.
        Returns:
            masked_img_array - a masked version of self.img_array
        '''
        assert self.running, 'End of file sequence has been reached.'
        for i in range(self.bin_size):
            self.fullres_mask[i, ...] = cv2.resize(
                self.mask_array[i, ...], (self.width, self.height),
                interpolation=cv2.INTER_NEAREST)

        masked_img_array = ma.masked_array(self.img_array,
                                           mask=self.fullres_mask)

        return masked_img_array

    def read_frames_and_segm(self, ret=False):
        '''
        Combines read_frames() and read_segmentation().
        Returns:
            [self.img_array]
            [self.mask_array]
        '''
        self.read_frames(return_array=False)
        if self.running:
            self.read_segmentation()

        if ret is True:
            return self.img_array, self.mask_array
        else:
            return None

    def read_and_segment(self):
        '''
        Combines read_frames_and_segm() and apply_segmentation()
        Returns:
            masked_img_array - a masked version of self.img_array
        '''
        self.read_frames_and_segm()
        if self.running:
            return self.apply_segmentation()
        else:
            return None


def apply_segmentation(img_array, mask_array):
    '''
    Resize mask_array and apply to img_array.
    Args:
        img_array - numpy.ndarray, array of images (f, h, w)
        mask_array - section of segmentation array
    Returns:
        masked_img_array - a masked version of self.img_array
    '''
    if img_array.shape[0] != mask_array.shape[0]:
        raise ValueError('Arrays must have same first dimension')
    fullres_mask = np.empty(img_array.shape, dtype=bool)
    for i in range(img_array.shape[0]):
            fullres_mask[i, ...] = cv2.resize(
                    mask_array[i, ...], img_array.shape[1:][::-1],
                    interpolation=cv2.INTER_NEAREST)

    masked_img_array = ma.masked_array(img_array, mask=fullres_mask)

    return masked_img_array


def get_bg_img(masked_img_array):
    '''
    Produce a background image from an array of masked images
    Args:
        masked_img_array - a masked array (f, h, w) of segmented images
    Returns:
        bg_img - a background image reconstructed from masked_img_array,
            with same dtype as masked_img_array
    '''
    if len(masked_img_array.shape) != 3:
        raise ValueError(
            'Expected a 3-dimensional array. Instead got an array of shape ' +
            str(masked_img_array.shape))

    bg_img = ma.mean(masked_img_array, axis=0).astype(masked_img_array.dtype)

    return bg_img.filled(0)


def main():
    pass

if __name__ == '__main__':
    main()
