from libc.math cimport sqrt
import numpy as np
cimport numpy as np
from numpy.math cimport INFINITY


XY_DTYPE = np.int32
T_DTYPE = np.int64
ID_DTYPE = np.int64

ctypedef np.int32_t XY_DTYPE_t
ctypedef np.int64_t T_DTYPE_t
ctypedef np.int64_t ID_DTYPE_t


cpdef double tracklet_distance(np.ndarray[XY_DTYPE_t, ndim=1] x_array,
                               np.ndarray[XY_DTYPE_t, ndim=1] y_array,
                               np.ndarray[T_DTYPE_t, ndim=1] t_array,
                               np.ndarray[ID_DTYPE_t, ndim=1] id_array) except *:
    '''
    Calculate the tracklet distance between two tracklets.
    Args:
        All arguments should be sorted in ascending t_array order.
        x_array - x values for joined tracklets
        y_array - y values for joined tracklets
        t_array - fr_idx values for joined tracklets
        id_array - track_id values for joined tracklets
    Returns:
        tr_dist - tracklet distance
    '''
    cdef imax = x_array.shape[0]
    cdef XY_DTYPE_t x0, x1, x2, y0, y1, y2
    cdef T_DTYPE_t t0, t1, t2
    cdef ID_DTYPE_t id0, id1, id2
    cdef int i
    cdef int n_boundary = 0
    cdef double t_interp, dx, dy, tr_dist
    cdef double d_sq_sum = 0.0

    # First element
    t1 = t_array[0]
    t2 = t_array[1]
    if t1 == t2:
        return INFINITY

    id1 = id_array[0]
    id2 = id_array[1]
    if id1 != id2:
        x1 = x_array[0]
        x2 = x_array[1]
        y1 = y_array[0]
        y2 = y_array[1]

        dx = x2 - x1
        dy = y2 - y1

        d_sq_sum += sqrt(dx * dx + dy * dy)
        n_boundary += 1

    # Middle elements
    for i in range(1, imax - 1):
        t0 = t1
        t1 = t2
        t2 = t_array[i + 1]
        if t1 == t2:
            return INFINITY

        id0 = id1
        id1 = id2
        id2 = id_array[i + 1]
        if id0 != id1 or id1 != id2:
            x0 = x_array[i - 1]
            x1 = x_array[i]
            x2 = x_array[i + 1]
            y0 = y_array[i - 1]
            y1 = y_array[i]
            y2 = y_array[i + 1]

            t_interp = (t1 - t0) / (t2 - t0)
            dx = x0 + (x2 - x1) * t_interp - x1
            dy = y0 + (y2 - y1) * t_interp - y1

            d_sq_sum += sqrt(dx * dx + dy * dy)
            n_boundary += 1
        else:
            continue

    # Last element
    t0 = t1
    t1 = t2

    id0 = id1
    id1 = id2
    if id0 != id1:
        x0 = x_array[imax - 2]
        x1 = x_array[imax - 1]
        y0 = y_array[imax - 2]
        y1 = y_array[imax - 1]

        dx = x1 - x0
        dy = y1 - y0

        d_sq_sum += sqrt(dx * dx + dy * dy)
        n_boundary += 1

    tr_dist = d_sq_sum / n_boundary

    return tr_dist
