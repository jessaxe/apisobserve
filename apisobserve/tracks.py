#!/usr/bin/env python

import glob
import itertools
import multiprocessing as mp
import numpy as np
import os
from scipy import sparse, misc
import time
from datetime import datetime, timedelta

from .tracklet_distance import tracklet_distance as tr_dist


def vprint(*args, **kwargs):
    pass
# vprint = print


def load_offsets(path):
    '''
    Load offsets from a file
    Args:
        path - path to file
    Returns:
        offsets - np.ndarray
    '''
    offsets = []
    with open(path, 'r') as f:
        for line in f:
            offset = [int(s) for s in line.split()[-2:]]
            offsets.append(offset)

    return np.array(offsets)


def file_chunks(l, chunksize):
    '''
    Yield successive chunks from l.
    Args:
        l - list
        chunksize - int size of chunks
    '''
    for i in range(0, len(l), chunksize):
        yield l[i:i + chunksize]


def track_distance(joint_track):
    '''
    Determines the tracklet distance between two tracklets
    Args:
        joint_track - array containing two tracks sorted in chronological order
    Returns:
        dist - np.float64
    '''
    dist = tr_dist(joint_track['x'], joint_track['y'],
                   joint_track['fr_idx'], joint_track['track_id'])

    if dist < 0:
        return np.inf
    else:
        return dist


class ExtendedCoo(sparse.coo_matrix):
    '''
    Extends sparse.coo_matrix to support limited indexing operations
    '''
    def __setitem__(self, key, value):
        '''
        Does not replace any values (allows duplicates)
        '''
        row, col = key
        keytype = type(row)
        if (type(col) != keytype) or (type(value) != keytype):
            raise TypeError('keys and value must be of same type')

        if keytype == np.ndarray or keytype == list:
            if (row.shape != col.shape) or (col.shape != value.shape):
                raise ValueError('keys and value must have same shape')
            else:
                self.row = np.concatenate((self.row, row))
                self.col = np.concatenate((self.col, col))
                self.data = np.concatenate((self.data, value))
        else:
                self.row = np.append(self.row, row)
                self.col = np.append(self.col, col)
                self.data = np.append(self.data, value)

        return None

    def __delitem__(self, key):
        '''
        Remove a row/column from the matrix (set it to zero)
        '''
        mask = (self.row != key) & (self.col != key)

        self.apply_mask(mask)

        return None

    def __iadd__(self, other):
        if isinstance(other, sparse.coo_matrix):
            self[other.row, other.col] = other.data
            return self
        elif isinstance(other, sparse.spmatrix):
            other_coo = sparse.coo_matrix(other)
            self[other_coo.row, other_coo.col] = other_coo.data
            return self
        else:
            return NotImplemented

    def __lt__(self, other):
        return self.data < other

    def __le__(self, other):
        return self.data <= other

    def __eq__(self, other):
        return self.data == other

    def __gt__(self, other):
        return self.data > other

    def __ge__(self, other):
        return self.data >= other

    def apply_mask(self, mask):
        '''
        Apply a boolean mask to the matrix
        Args:
            mask - np.ndarray (bool) with same shape as self.data.shape
        Returns:
            None (operates inplace)
        '''
        self.row = self.row[mask]
        self.col = self.col[mask]
        self.data = self.data[mask]
        return None

    def argmax(self, return_max=False):
        '''
        Returns the index of the maximum value (row, col), and the max value if
        return_max is True
        '''
        idx = self.data.argmax()
        if return_max is False:
            return self.row[idx], self.col[idx]
        elif return_max is True:
            return self.row[idx], self.col[idx], self.data[idx]
        else:
            raise TypeError('return_max must be True or False')


class TagDataError(Exception):
    pass


class TrackBuilder():
    '''
    Provides methods for loading and combining detection data into tracks
    '''
    detection_dtype = np.dtype([
            ('fr_idx', np.int64),
            ('x', np.int32),
            ('y', np.int32),
            ('r', np.uint8),
            ('angle', np.float32),
            ('tag_id', np.uint16),
            ('quality', np.float32)
        ])
    track_dtype = np.dtype([
            ('fr_idx', np.int64),
            ('x', np.int32),
            ('y', np.int32),
            ('r', np.uint8),
            ('angle', np.float32),
            ('tag_id', np.uint16),
            ('quality', np.float32),
            ('track_id', np.int64)
        ])

    def __init__(self, offsets, fr_step=300, max_step=10, time_diff_thresh=5,
                 distance_thresh=500):
        '''
        Args:
            offsets - array of camera offsets for each directory in tag_dirs,
                or string containing path to offsets file
            fr_step - number of frames processed for each file
            max_step - largest step spanning missing detections before a new
                tracklet is started
            time_diff_thresh - tracklets which are separated in time by more
                than this will be given a distance of np.inf
            distance_thresh - distance trheshold (only include distances
                less than this)
            tr_distance_thresh - tracklet distance trheshold (only include
                distances less than this)
            min_track_length - filter tracks by number of detections
        '''
        if isinstance(offsets, str):
            self.offsets = load_offsets(offsets)
        elif isinstance(offsets, np.ndarray):
            self.offsets = offsets
        else:
            raise TypeError('offsets parameter must be str or numpy.ndarray')

        if isinstance(fr_step, int):
            self.fr_step = fr_step
        else:
            raise TypeError('fr_step must be an integer')

        self.max_step = max_step
        self.time_diff_thresh = time_diff_thresh
        self.distance_thresh = distance_thresh

    def load_detections(self, file_list, fixed_fr_idx=None):
        '''
        Loads (unsorted) tag detection data from multiple cameras with multiple
        files.
        Args:
            file_list - list of lists of paths to tag detection datafiles
        Returns:
            None (Stores in self.detections)
        '''
        vprint('Loading detection files', end='')
        det_list = []
        tot = len(file_list[0])
        for i in range(tot):
            vprint('\rLoading detection files ({:d}/{:d})'.format(i, tot),
                   end='')
            dets = []
            for j in range(len(file_list)):
                det = np.loadtxt(file_list[j][i], dtype=self.detection_dtype)
                det['fr_idx'] += i * self.fr_step
                det['y'] += self.offsets[j, 0]
                det['x'] += self.offsets[j, 1]
                dets.append(det)

            det = np.concatenate(dets)
            det_list.append(det)

        vprint('\rLoading detection files ({:d}/{:d})'.format(tot, tot))
        vprint('Joining and sorting... ', end='')
        self.detections = np.concatenate(det_list)
        self.detections.sort(kind='mergesort',
                             order=['fr_idx', 'tag_id', 'y', 'x'])
        vprint('Done.')

    def filter_identical(self):
        '''
        Filters out any identical tag calls in the same frame to prevent
        possible downstream errors. Only keep the best quality detection
        of each tag in each frame.
        '''
        vprint('Filtering identical tags')
        id_fr_pairs, dup_idx_start, dup_counts = np.unique(
            self.detections[['fr_idx', 'tag_id']], return_index=True,
            return_counts=True)
        mask = dup_counts > 1
        dup_ids = id_fr_pairs['tag_id'][mask]
        dup_idx_start = dup_idx_start[mask]
        dup_counts = dup_counts[mask]

        for i in range(dup_ids.shape[0]):
            # Mark duplicate detections
            idx0 = dup_idx_start[i]
            idx1 = idx0 + dup_counts[i]
            self.detections['tag_id'][idx0:idx1] = 0

            # Restore best duplicate
            keep_idx = np.argmax(self.detections['quality'][idx0:idx1]) + idx0
            self.detections['tag_id'][keep_idx] = dup_ids[i]

        # Remove marked detections
        n_removed = np.count_nonzero(self.detections['tag_id'] == 0)
        tot = self.detections.shape[0]
        self.detections = self.detections[
            self.detections['tag_id'].astype(bool)]

        vprint('Removed {:d}/{:d} detections'.format(n_removed, tot))

        return None

    def construct_tracks(self):
        '''
        Links detections into tracks. Replaces self.detections with an array
        of dtype track_dtype.
        Returns:
            TagTracks object
        '''
        vprint('Constructing tracks')
        track_list = []
        tag_ids = np.unique(self.detections['tag_id'])
        prev_track_id = 0
        tags_done = 0
        for tag_id in tag_ids:
            vprint('\rBuilt {:d} tracks for {:d} tags'.format(
                prev_track_id, tags_done), end='')
            tags_done += 1
            tag_idx = np.where(self.detections['tag_id'] == tag_id)
            tag_detections = self.detections[tag_idx]

            # Calculate gaps in frame index
            fr_idx_diff = np.empty(tag_detections.shape, dtype=np.int64)
            fr_idx_diff[0] = self.max_step + 1
            fr_idx_diff[1:] = tag_detections['fr_idx'][1:] - \
                tag_detections['fr_idx'][:-1]

            # Calculate euclidean distance
            dist_array = np.empty(tag_detections.shape, dtype=np.float32)
            dist_array[0] = self.distance_thresh + 1
            dist_array[1:] = np.sqrt(
                (tag_detections['x'][1:] - tag_detections['x'][:-1]) ** 2 +
                (tag_detections['y'][1:] - tag_detections['y'][:-1]) ** 2)

            # Get track ids
            track_id_arr = np.cumsum((fr_idx_diff > self.max_step) |
                                     (dist_array > self.distance_thresh))

            # Produce tracks array
            tag_tracks = np.empty(tag_detections.shape, dtype=self.track_dtype)
            for name in self.detection_dtype.names:
                tag_tracks[name] = tag_detections[name]

            tag_tracks['track_id'] = track_id_arr + prev_track_id
            prev_track_id = tag_tracks['track_id'][-1]

            track_list.append(tag_tracks)

        vprint('\rBuilt {:d} tracks for {:d} tags'.format(prev_track_id,
                                                          tags_done))
        vprint('Joining and sorting... ', end='')
        tracks = np.concatenate(track_list)
        tracks.sort(kind='mergesort', order=['fr_idx', 'tag_id', 'y', 'x'])
        vprint('Done.')

        return TagTracks(tracks, time_diff_thresh=self.time_diff_thresh)

    def load_and_construct(self, file_list):
        '''
        Loads and filters detections, and converts to TagTracks object
        Args:
            file_list - list of lists of paths to tag detection datafiles
        Returns:
            TagTracks object
        '''
        self.load_detections(file_list)
        self.filter_identical()
        return self.construct_tracks()


class TagTracks():
    '''
    Provides methods for accessing and operating on track data
    '''
    track_dtype = TrackBuilder.track_dtype
    id_dtype = track_dtype['track_id']
    deleted_track = -1

    def __init__(self, tracks, time_diff_thresh=np.inf):
        '''
        Args:
            tracks - numpy array of TrackBuilder.track_dtype dtype
        '''
        if tracks.dtype == self.track_dtype:
            self.arr = tracks
        else:
            raise ValueError('tracks must be track_dtype')

        self.time_diff_thresh = time_diff_thresh

        self.distance_calculated = False
        self.__makeidx()

    def __makeidx(self):
        vprint('Making track index')
        self.__track_dict = {}
        track_ids = np.unique(self.arr['track_id'])
        tot = track_ids.shape[0]
        count = 0
        for track_id in track_ids:
            vprint('\rMade {:d}/{:d} indexes'.format(count, tot), end='')
            count += 1
            self.__track_dict[track_id] = \
                np.where(self.arr['track_id'] == track_id)[0]

        vprint('\rMade {:d}/{:d} indexes'.format(count, tot))

    def __getitem__(self, key):
        if isinstance(key, (np.int64, np.int32, np.int)):
            return self.arr[self.__track_dict[key]]
        elif isinstance(key, (list, np.ndarray)):
            idx = np.concatenate([self.__track_dict[i] for i in key])
            idx.sort(kind='mergesort')
            return self.arr[idx]
        elif isinstance(key, str):
            return self.arr[key]
        elif isinstance(key, tuple):
            if len(key) == 2 and isinstance(key[0],
                                            (np.int64, np.int32, np.int)):
                return self.arr[key[1]][self.__track_dict[key[0]]]
            else:
                raise IndexError('Index must be [int], [str] or [int, str]')
        else:
            raise IndexError('''Index must be [int], [str] or [int, str].
                             Instead, got {:s}'''.format(str(type(key))))

    def __setitem__(self, key, value):
        if isinstance(key, tuple):
            if len(key) == 2 and \
                    isinstance(key[0], (np.int64, np.int32, np.int)) and \
                    key[1] == 'tag_id':
                change_idx = self.__track_dict[key[0]][
                    self.arr['tag_id'][self.__track_dict[key[0]]] != value]
                self.arr['tag_id'][change_idx] = value
                self.arr['angle'][change_idx] = np.nan
                return None
            else:
                raise IndexError('Can only set "tag_id" with this method')

        if key not in self.__track_dict:
            raise IndexError('track_id {:d} not present'.format(key))
        if isinstance(value, (np.int64, np.int32, np.int)) is False:
            raise TypeError('track_id must be an integer. Got {:s}'.format(
                str(type(value))))

        self.arr['track_id'][self.__track_dict[key]] = value
        del self.__track_dict[key]
        self.__track_dict[value] = np.where(self.arr['track_id'] == value)[0]

        if self.distance_calculated is True:
            del self.dists[key]
            if value != self.deleted_track:
                self.recalculate_inverse_distances(value)

        return None

    def __delitem__(self, key):
        self[key] = self.deleted_track

    def __iter__(self):
        for track_id in self.__track_dict:
            if track_id == self.deleted_track:
                continue
            yield self[track_id]

    def del_tracks(self):
        '''
        When 'del self[track_id]' is called, the track_id is just set to a null
        value (-1), so that the index does not have to be recaclulated every
        time. Once you are done deleting tracks, call this to actually remove
        that data and recalculate the index
        '''
        vprint('Applying deletions... ', end='')
        self.arr = np.delete(self.arr, self.__track_dict[self.deleted_track])
        vprint('Done.')
        self.__makeidx()
        return None

    def filter_short_tracks(self, min_track_length):
        '''
        Removes short tracks from self.detections
        Args:
            min_track_length - minimum track length
        '''
        vprint('Removing short tracks')
        tot = len(self.__track_dict)
        count = 0
        for track_id in list(self.__track_dict):
            vprint('\rRemoved {:d}/{:d} tracks'.format(count, tot), end='')
            if len(self.__track_dict[track_id]) < min_track_length:
                del self[track_id]
                count += 1

        vprint('\rRemoved {:d}/{:d} tracks'.format(count, tot))
        self.del_tracks()

        return None

    def get_distance(self, track_id0, track_id1):
        '''
        Determines the track distance between two tracks
        Args:
            track_id0, track_id1 - IDs of two tracks to compare
        Returns:
            dist - np.float64
        '''
        track0 = self[track_id0]
        track1 = self[track_id1]

        fr_diff = max(
            track1['fr_idx'][0] - track0['fr_idx'][-1] > self.time_diff_thresh,
            track0['fr_idx'][0] - track1['fr_idx'][-1] > self.time_diff_thresh)

        if fr_diff > self.time_diff_thresh:
            return np.inf

        joint_track = np.concatenate((track0, track1))
        joint_track.sort(kind='mergesort', order=['fr_idx'])
        dist = tr_dist(joint_track['x'], joint_track['y'],
                       joint_track['fr_idx'], joint_track['track_id'])

        return dist

    def calculate_inverse_distances(self):
        '''
        Calculate all track distances between overlapping tracklets and
        store their inverse (ie. 1 / distance) as a sparse matrix. Inverse is
        stored instead of actual distances for implementation reasons.
        Stores in self.dists
        '''
        vprint('Calculating track distances')
        try:
            s = max(self.__track_dict) + 1
        except ValueError:
            s = 0
        inverse_distances = sparse.dok_matrix((s, s), dtype=np.float32)
        tot_tracks = len(self.__track_dict)
        tot = misc.comb(tot_tracks, 2, exact=True)
        count = 0
        for tr_id0, tr_id1 in itertools.combinations(self.__track_dict, 2):
            if tr_id0 == self.deleted_track or tr_id1 == self.deleted_track:
                tot -= 1
                continue

            if count % tot_tracks == 0:
                vprint('\rCalculated {:d}/{:d} track distances'.format(
                    count, tot), end='')
            count += 1

            distance = self.get_distance(tr_id0, tr_id1)

            if distance == np.inf:
                continue
            elif distance == 0:
                inverse_distances[tr_id0, tr_id1] = np.inf
            else:
                inverse_distances[tr_id0, tr_id1] = 1 / distance

        vprint('\rCalculated {:d}/{:d} track distances'.format(count, tot))
        vprint('Converting distance matrix format... ', end='')
        self.dists = ExtendedCoo(inverse_distances)
        self.distance_calculated = True
        vprint('Done.')

        return None

    def recalculate_inverse_distances(self, track_id):
        '''
        Recalculate inverse tracklet distances for a single track_id
        Args:
            track_id - id of track to recalculate distance for
        '''
        assert self.distance_calculated is True, '''calculate_inverse_distances
            must be called before this function'''

        inverse_distances = sparse.dok_matrix(self.dists.shape,
                                              dtype=np.float32)
        for tr_id1 in self.__track_dict:
            if track_id == tr_id1 or tr_id1 == self.deleted_track:
                continue

            distance = self.get_distance(track_id, tr_id1)

            try:
                inverse_distances[track_id, tr_id1] = 1 / distance
            except ZeroDivisionError:
                inverse_distances[track_id, tr_id1] = np.inf

        self.dists += inverse_distances

        return None

    def threshold_track_dists(self, threshold):
        '''
        Remove track distances from self.dists which exceed the threshold
        (inverse is less than the inverse of the threshold).
        Args:
            threshold - the distance threshold
        Returns:
            None (operates inplace)
        '''
        mask = self.dists >= 1 / threshold
        self.dists.apply_mask(mask)
        return None

    def merge(self, track_dist_thresh=None):
        '''
        Iteratively merge the two closest tracks
        Args:
            track_dist_thresh (optional) - threshold/stop criterion for
                merging tracks
        '''
        if self.distance_calculated is False:
            self.calculate_inverse_distances()

        if track_dist_thresh is None:
            inv_thresh = None
        else:
            self.threshold_track_dists(track_dist_thresh)
            inv_thresh = 1 / track_dist_thresh

        vprint('Merging close tracks')
        count = 0
        fmt_str = '\rMerged {:d}/{:d} tracks (prev dist/thresh: {:.5f}/{:.5f})'
        while True:
            tot = self.dists.count_nonzero()
            if tot == 0:
                break

            track_id0, track_id1, inv_dist = self.dists.argmax(return_max=True)
            if inv_dist < inv_thresh:
                break

            vprint(fmt_str.format(count, count + tot, inv_dist, inv_thresh),
                   end='')
            count += 1

            self[track_id1] = track_id0

        vprint(fmt_str.format(count, count + tot, inv_dist, inv_thresh))

        return None

    def id_vote(self):
        '''
        Infer the identity of merged tracks by voting
        '''
        vprint('Voting on tag IDs')
        tot = len(self.__track_dict)
        count = 0
        for track_id in self.__track_dict:
            if track_id == self.deleted_track:
                tot -= 1
                continue

            vprint('\rVoted on {:d}/{:d} of tracks'.format(count, tot), end='')
            track = self[track_id]
            tag_ids = np.unique(track['tag_id'])
            scores = np.zeros(tag_ids.shape, dtype=self.track_dtype['quality'])
            for i in range(tag_ids.shape[0]):
                tag_id = tag_ids[i]
                scores[i] = track['quality'][track['tag_id'] == tag_id].sum()

            best_tag_id = tag_ids[np.argmax(scores)]
            self[track_id, 'tag_id'] = best_tag_id

        vprint('\rVoted on {:d}/{:d} of tracks'.format(count, tot))
        return None

    def save_csv(self, path):
        '''
        Output self.arr to path
        Args:
            path - string containing path to file
        '''
        vprint('Writing to {:s}'.format(path))
        with open(path, 'w') as f:
            print(*(name for name in self.track_dtype.names), sep='\t', file=f)
            for e in self.arr:
                print(*(e[name] for name in self.track_dtype.names), sep='\t',
                      file=f)

        vprint('Done.')

        return None

    def process(self, outpath=None, min_track_length=3, track_dist_thresh=30):
        '''
        Process tracks to correct tag identifications
        Args:
            outpath - string containing path to output file (csv)
            min_track_length - minimum track length
            track_dist_thresh (optional) - threshold/stop criterion for
                merging tracks
        Returns:
            None - if outpath is set
            self - if outpath is unset
        '''
        self.filter_short_tracks(min_track_length)
        self.merge(track_dist_thresh=track_dist_thresh)
        self.id_vote()
        if outpath is not None:
            self.save_csv(outpath)
            return None
        else:
            return self


class Consumer():
    '''
    Processes tag detection files
    '''
    poison_pill = 'PP'

    def __init__(self, q, offsets, fr_step=300, max_step=10,
                 time_diff_thresh=5, distance_thresh=500,
                 min_track_length=3, track_dist_thresh=30, outdir=''):
        self.track_builder = TrackBuilder(
            offsets, fr_step=fr_step, max_step=max_step,
            time_diff_thresh=time_diff_thresh, distance_thresh=distance_thresh)

        self.q = q
        self.min_track_length = min_track_length
        self.track_dist_thresh = track_dist_thresh

        self.outdir = outdir

        self.run()

    def get_outpath(self, msg):
        '''
        Return the path of the output file
        '''
        basename = os.path.basename(msg[0][0])
        return os.path.join(self.outdir, basename)

    def run(self):
        while True:
            msg = self.q.get()
            if msg == self.poison_pill:
                self.q.task_done()
                break
            else:
                tag_tracks = self.track_builder.load_and_construct(msg)
                tag_tracks.process(
                    outpath=self.get_outpath(msg),
                    min_track_length=self.min_track_length,
                    track_dist_thresh=self.track_dist_thresh)
                self.q.task_done()


class FileProducer():
    '''
    Provides methods for selecting and grouping tag detection files
    '''
    def __init__(self, tag_dirs, q, file_glob='*.csv', chunksize=10):
        '''
        Args:
            tag_dirs - list of directories where detection data is stored
            file_glob - unix style glob of tag files in tag_dirs
            chunksize - number of files from each camera to process at once
        '''
        self.tag_dirs = tag_dirs
        self.file_glob = file_glob
        self.q = q

        if isinstance(chunksize, int):
            self.chunksize = chunksize
        else:
            raise TypeError('chunksize must be an integer')

        self.file_list = self.__load_file_lists()

    def __load_file_lists(self):
        '''
        Yield a generator of file lists
        '''
        file_lists = []
        for directory in self.tag_dirs:
            file_list = sorted(glob.glob(os.path.join(directory,
                                                      self.file_glob)))
            file_lists.append(file_list)
            if len(file_list) != len(file_lists[0]):
                raise TagDataError(
                    'Directories have a different number of tag files in them')
        self.n_chunks = len(file_lists[0]) // self.chunksize

        for i in range(0, len(file_lists[0]), self.chunksize):
            yield [file_list[i:i + self.chunksize] for file_list in file_lists]

    def run(self):
        prev_time = time.perf_counter()
        tot_time = 0.
        i = 0
        for msg in self.file_list:
            self.q.put(msg)
            curr_time = time.perf_counter()
            tot_time += curr_time - prev_time
            prev_time = curr_time
            elapsed = timedelta(seconds=tot_time)
            if i == 0:
                fmt_str = '\rFile batch: {:' + str(len(str(self.n_chunks))) + \
                    'd}/{:d} Elapsed: {:0>11s} Remain: {:0>11s}'
                remaining = 'N/A            '
            else:
                remaining = elapsed * (self.n_chunks / i - 1)
            print(fmt_str.format(i, self.n_chunks, str(elapsed)[:-4],
                str(remaining)[:-4]), end='')
            i += 1


def process_files(tag_dirs, out_dir, offsets_file, processes,
                  file_glob='*.csv', chunksize=10, consumer_kwargs={}):
    '''
    Organise a FileProducer and Consumers to process tag detections
    Args:
        tag_dirs - list of directories where detection data is stored
        out_dir - directory to store outputs
        offsets_file - file containing camera offsets
        processes - number of child processes to spawn
        file_glob - unix style glob of tag files in tag_dirs
        chunksize - number of files from each camera to process at once
        consumer_kwargs - keyword arguments passed to Consumer()
    '''
    q = mp.JoinableQueue(maxsize=1)
    vprint(out_dir)
    consumer_kwargs['outdir'] = out_dir
    processes = [mp.Process(target=Consumer, args=(q, offsets_file),
                            kwargs=consumer_kwargs)
                 for i in range(processes)]
    for process in processes:
        process.start()

    producer = FileProducer(tag_dirs, q, file_glob=file_glob,
                            chunksize=chunksize)
    producer.run()

    for process in processes:
        q.put(Consumer.poison_pill)

    q.join()
