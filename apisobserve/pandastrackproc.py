# Loading tag data to pandas.dataframe

import datetime as dt
from multiprocessing import Pool
import os

import numpy as np
import pandas


class TrackLoader():
    '''
    Provides methods for loading tag data from a series of track csv files
    into pandas dataframes
    '''
    track_dtype = np.dtype([
            ('fr_idx', np.int64),
            ('x', np.int32),
            ('y', np.int32),
            ('r', np.uint8),
            ('angle', np.float32),
            ('tag_id', np.uint16),
            ('quality', np.float32),
            ('track_id', np.int64)
        ])

    def __init__(self, files, date_format='%Y-%m-%d-%H-%M-%S.csv', fps=5.):
        '''
        Args:
            files - list of track files
            date_format - date encoding of filenames
            fps - frames per second of track files (to calculate datetimes
        '''
        self.files = files
        self._len = len(files)
        self.date_format = date_format
        self.file_datetimes = [
            dt.datetime.strptime(os.path.basename(f), self.date_format)
            for f in self.files
            ]
        self.framedelta = dt.timedelta(seconds=(1 / fps))

    def __getitem__(self, key):
        if isinstance(key, slice):
            raise TypeError('Can only be indexed by integers, not slices')

        try:
            key, labels = key
        except TypeError:
            pass

        f = self.files[key]
        datetime = self.file_datetimes[key]

        df = pandas.read_table(f, dtype=self.track_dtype)
        df['datetime'] = datetime + df.fr_idx * self.framedelta
        df.set_index('datetime', inplace=True)

        try:
            return df[labels]
        except NameError:
            return df

    def __len__(self):
        return self._len

    def timeseries(self, names=None, processes=None):
        '''
        Load all files, and return a pandas.DataFrame or pandas.Series
        containing desired data.
        Args:
            names - optional str or list of str which define the names of
                columns of the data to be loaded. If a single string is given,
                a pandas.Series will be returned instead of a panda.DataFrame.
                By default, a pandas.DataFrame of all the data will be returned
                (this will take a lot of memory)
            processes - optionally spawn child processes to speed up file
                parsing
        '''
        if processes is None:
            if names is None:
                ts_list = [self[i] for i in range(len(self))]
            else:
                ts_list = [self[i, names] for i in range(len(self))]

            return pandas.concat(ts_list)

        else:
            pool = Pool(processes=processes)
            if names is None:
                ts_list = pool.map(self.__getitem__, range(len(self)),
                                   chunksize=len(self) // processes)
            else:
                ts_list = pool.map(
                    self.__getitem__, ((i, names) for i in range(len(self))),
                    chunksize=len(self) // processes)

            pool.close()
            pool.join()
            return pandas.concat(ts_list)
