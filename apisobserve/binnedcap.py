#!/usr/bin/env python

import cv2
import numpy as np
import os.path
import warnings
import datetime as dt


def idx_from_datetime(datetime, starttime, fps=5):
    '''
    Calculates the frame index based on datetime, starttime and the fps
    Args:
        datetime - time of desired index (dt.datetime)
        starttime - time of start of videos (dt.datetime)
        fps - framerate of the underlying data (frames per second)
    '''
    delta = datetime - starttime
    idx = int(delta.total_seconds() * fps)

    return idx


class MultiVideoCapture():
    '''
    Extends cv2.VideoCapture to handle multiple sequential video files
    '''
    def __init__(self, *filenames, file_extension='.h264', log_file=None):
        '''
        Args:
            filenames - Paths to video files (sequential order). Videos are
                assumed to follow on from eachother without a gap.
            file_extension - all files in filenames must have this extension
            log_file - optional path to a output log file
        '''
        self.filenames = filenames
        self.file_extension = file_extension
        self.log_file = log_file
        if self.log_file is not None:
            with open(self.log_file, 'w') as log_file:
                log_file.write(dt.datetime.now().strftime(
                    'Started log at %Y-%m-%d %H:%M:%S\n'))
        self._check_filenames()
        self.current_file = self.filenames[0]
        self.cap = cv2.VideoCapture(self.current_file)
        self.file_idx = 0
        self.tot_fr_idx = 0  # 1-starting index -added to when first frame read
        self.file_fr_idx = 0  # 1-starting index
        self.width = int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    def _check_filenames(self):
        '''
        Check if each filename in self.filenames a real path to an extant file.
        Also check that each file has the correct extension
        (as per self.file_extension)
        Raises:
            FileNotFoundError - if a file doesn't exist
            ValueError - if a file has the wrong extension
        '''
        for filename in self.filenames:
            if os.path.isfile(filename):
                if filename[-len(self.file_extension):] == self.file_extension:
                    continue
                else:
                    raise ValueError(
                        "%s does not match the file extension '%s'" % (
                            filename, self.file_extension))
            else:
                raise FileNotFoundError('%s does not exist.' % filename)

        return None

    def _check_dimensions(self):
        '''
        Check that the dimensions of the current video match the rest.
        Raises:
            ValueError
        '''
        width = self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        if (self.width, self.height) == (width, height):
            pass
        else:
            raise ValueError(
                ('Dimensions of %s (%d, %d) do not match' +
                 'previous files (%d, %d).') % (
                    self.current_file, width, height, self.width, self.height))

        return None

    def _next_file(self):
        '''
        Releases self.cap and opens the next video file (if there is one)
        Returns:
            ret - bool, True if sequence has not yet ended
        '''
        if self.log_file is not None:
            with open(self.log_file, 'a') as log_file:
                log_file.write('%s\t%dframes\n' %
                               (self.current_file, self.file_fr_idx - 1))
        self.file_idx += 1
        self.file_fr_idx = 0
        if self.file_idx < len(self.filenames):
            self.current_file = self.filenames[self.file_idx]
            self.cap.open(self.current_file)
            self._check_dimensions()
            return True
        else:
            self.cap.release()
            return False

    def get(self, *args, **kwargs):
        return self.cap.get(*args, **kwargs)

    def read(self):
        '''
        Grabs, decodes and returns the next frame in the video sequence
        Returns:
            ret - bool indicating read success
            frame - numpy ndarray
        '''
        ret, frame = self.cap.read()
        if ret is False:
            ret = self._next_file()
            if ret is False:
                return False, None
            else:
                ret, frame = self.cap.read()

        self.tot_fr_idx += 1
        self.file_fr_idx += 1
        return ret, frame

    def release(self):
        '''
        Release the current VideoCapture
        '''
        self.cap.release()

        return None


class BinnedVideoCapture(MultiVideoCapture):
    '''
    Loads frames into bins for batch processing
    '''
    def __init__(self, *filenames, bin_size=300, **kwargs):
        '''
        Args:
            filenames - Paths to video files (sequential order). Videos are
                assumed to follow on from eachother without a gap.
            bin_size - number of frames to store at once
            **kwargs - passed to MultiVideoCapture().__init__
        '''
        if isinstance(bin_size, int) is False:
            raise ValueError('bin_size must be of type int')

        super().__init__(*filenames, **kwargs)
        self.bin_size = bin_size
        self.img_array = np.empty((self.bin_size, self.height, self.width),
                                  dtype=np.uint8)
        self.running = True

    def read(self):
        '''
        Do not use this function. It will cause unexpected results.
        '''
        warnings.warn(
            '''Do not directly call read() on BinnedMaskedVideoCapture
            objects. This will result in unexpected behaviour. Instead, use
            read_frames(), which calls super().read() instead.''')
        print()
        return super().read()

    def read_frames(self, return_array=True):
        '''
        Grabs, decodes, converts to greyscale and stores the next self.bin_size
        frames in the video sequence. Frames are stored in self.img_array.
        Args:
            return_array - if False, None is returned
        Returns:
            [img_array] - numpy.ndarray, array of images (f, h, w)
        Raises:
            NotImplementedError - Cannot currently handle the case where an
                entire video is spanned by this method
        '''
        assert self.running, 'End of file sequence has been reached.'
        for i in range(self.bin_size):
            ret, img = super().read()
            if ret is True:
                self.img_array[i, ...] = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            else:
                self.running = False
                break

        if return_array is True:
            return self.img_array
        else:
            return None


class IndexedMultiVideoCapture(MultiVideoCapture):
    '''
    Provides methods for revisiting specific locations in videos previously
    processed
    '''
    def __init__(self, log_file):
        '''
        Args:
            log_file - output log file of MultiVideoCapture instance containing
                video file paths and number of frames
        '''
        with open(log_file, 'r') as f:
            f.readline()
            filenames, frame_numbers = zip(
                *(l.rstrip('frames\n').split('\t') for l in f.readlines()))

        file_extension = os.path.splitext(filenames[0])[1]

        self.frame_numbers = np.array(
            [int(frame_num) for frame_num in frame_numbers]) + 1
        self.cumul_fr_n = np.cumsum(self.frame_numbers)

        super().__init__(*filenames, file_extension=file_extension,
                         log_file=None)

    def set_fr_idx(self, fr_idx):
        '''
        Jump to a specific frame index
        Args:
            fr_idx - int index to jump to
        '''
        if fr_idx < 0:
            raise IndexError('Index must be positive')
        elif fr_idx >= self.cumul_fr_n[-1]:
            raise IndexError('Index ({:d}) out of range ({:d})'.format(
                fr_idx, self.cumul_fr_n[-1]))

        # Open correct file
        file_idx = np.where(self.cumul_fr_n > fr_idx)[0][0]
        self.file_idx = file_idx
        self.current_file = self.filenames[self.file_idx]
        self.cap.open(self.current_file)
        self._check_dimensions()

        # Jump to correct frame
        self.tot_fr_idx = fr_idx
        if self.file_idx == 0:
            self.file_fr_idx = self.tot_fr_idx
        else:
            self.file_fr_idx = (self.tot_fr_idx -
                                self.cumul_fr_n[self.file_idx - 1] + 1)

        for i in range(self.file_fr_idx):
            self.cap.read()

        return None


class ArrayVideoCapture():
    '''
    Provides the same interface as IndexedMultiVideoCapture, but works on an
    array of cameras
    '''
    def __init__(self, offsets_file, log_files):
        '''
        Args:
            offsets_file - path to file containing offsets of each camera in
                the array
            log_files - list of paths to the log files of each camera
        '''
        offsets = []
        with open(offsets_file, 'r') as f:
            for line in f:
                offset = [int(s) for s in line.split()[-2:]]
                offsets.append(offset)

        self.offsets = np.array(offsets)

        if self.offsets.shape[0] != len(log_files):
            raise ValueError('Expected {:d} log files, got {:d}'.format(
                self.offsets.shape[0], len(log_files)))

        self.caps = [IndexedMultiVideoCapture(log_f) for log_f in log_files]
        widths = np.array([cap.width for cap in self.caps])
        heights = np.array([cap.height for cap in self.caps])

        self.width = np.amax(self.offsets[:, 1] + widths)
        self.height = np.amax(self.offsets[:, 0] + heights)

        self.tot_fr_idx = 0

    def set_fr_idx(self, fr_idx):
        '''
        Jump to a specific frame index
        Args:
            fr_idx - int index to jump to
        '''
        self.tot_fr_idx = fr_idx
        for cap in self.caps:
            cap.set_fr_idx(fr_idx)

        return None

    def read(self):
        '''
        Grabs, decodes and returns the next frame in each video sequence,
        then combines into a single image
        Returns:
            ret - bool indicating read success
            frame - numpy ndarray
        '''
        combined_frame = np.zeros((self.height, self.width, 3), dtype=np.uint8)
        i = 0
        for cap in self.caps:
            ret, frame = cap.read()
            if ret is False:
                return False, None

            idx00 = self.offsets[i, 0]
            idx01 = idx00 + frame.shape[0]
            idx10 = self.offsets[i, 1]
            idx11 = idx10 + frame.shape[1]
            combined_frame[idx00:idx01, idx10:idx11, :] = frame
            i += 1

        self.tot_fr_idx += 1

        return True, combined_frame

    def release(self):
        for cap in self.caps:
            cap.release()

        return None


def main():
    pass

if __name__ == '__main__':
    main()
