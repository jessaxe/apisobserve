#!/usr/bin/env python

import argparse
import cv2
import numpy as np
from scipy import sparse, stats
import time
import multiprocessing as mp

from .tracklet_distance import tracklet_distance as tr_dist

reduced_detection_dtype = np.dtype([
        ('fr_idx', np.int64),
        ('x', np.int32),
        ('y', np.int32),
        ('r', np.uint8),
        ('angle', np.float64),
        ('tag_id', np.uint16),
        ('quality', np.float64)
    ])

detection_dtype = np.dtype([
        ('fr_idx', np.int64),
        ('x', np.int32),
        ('y', np.int32),
        ('r', np.uint8),
        ('tag_id', np.uint16),
        ('quality', np.float64),
        ('x_trans', np.float64),
        ('y_trans', np.float64),
        ('angle', np.float64),
        ('scale', np.float64)
    ])

tracklet_dtype = np.dtype([
        ('fr_idx', np.int64),
        ('x', np.int32),
        ('y', np.int32),
        ('r', np.uint8),
        ('tag_id', np.uint16),
        ('quality', np.float64),
        ('x_trans', np.float64),
        ('y_trans', np.float64),
        ('angle', np.float64),
        ('scale', np.float64),
        ('track_id', np.int64)
    ])


def load_detections(filename):
    '''
    Loads and sorts tag detection data from file.
    Args:
        filename - path to tag detection datafile
    Returns:
        detections - sorted array of dtype detection_dtype
    '''
    detections = np.loadtxt(filename, dtype=detection_dtype, skiprows=1)
    detections.sort(order=['fr_idx', 'tag_id'])

    return detections


def load_reduced_detections(filename):
    return np.loadtxt(filename, dtype=reduced_detection_dtype)


def batch_load_detections(filenames, fr_step=300, n_processes=1):
    '''
    Loads and sorts tag detection data from multiple files.
    Args:
        filenames - list of paths to tag detection datafiles
        fr_step - number of frames processed for each file
    Returns:
        detections - sorted array of dtype detection_dtype
    '''
    if n_processes == 1:
        detections_list = []
        i = 0
        n = len(filenames)
        for filename in filenames:
            print('\rLoading file {:d}/{:d}'.format(i, n), end='')
            detections = np.loadtxt(filename, dtype=reduced_detection_dtype)
            detections['fr_idx'] += fr_step * i
            detections_list.append(detections)
            i += 1
    else:
        pool = mp.Pool(processes=n_processes)
        detections_list = []
        i = 0
        n = len(filenames)
        detections_iter = pool.imap(load_reduced_detections, filenames,
                                    chunksize=60)

        for detections in detections_iter:
            print('\rLoading file {:d}/{:d}'.format(i, n), end='')
            detections['fr_idx'] += fr_step * i
            detections_list.append(detections)
            i += 1

    print()
    pool.close()
    pool.join()

    return np.concatenate(detections_list)


def load_multicamera_detections(filenames_list, offsets, fr_step=300,
                                n_processes=1):
    '''
    Loads and sorts tag detection data from multiple cameras with multiple
    files.
    Args:
        filenames_list - list of lists of paths to tag detection datafiles
        offsets - offsets array with shape (len(filenames_list), 2)
        fr_step - number of frames processed for each file
    Returns:
        detections - sorted array of dtype detection_dtype
    '''
    assert offsets.shape[0] == len(filenames_list)

    det_list = []
    for i in range(len(filenames_list)):
        print('Loading camera {:d}'.format(i))
        camera_detections = batch_load_detections(filenames_list[i],
                                                  fr_step=fr_step,
                                                  n_processes=n_processes)
        camera_detections['y'] + offsets[i, 0]
        camera_detections['x'] + offsets[i, 1]

        det_list.append(camera_detections)

    print('Concatenating detections')
    detections = np.concatenate(det_list)
    print('Sorting detections')
    detections.sort(order=['fr_idx', 'y', 'x'])

    return detections


def load_tracklets(filename):
    '''
    Load a tracklet array from a file.
    Args:
        filename - path to tag detection datafile
    Returns:
        tracklets - array of dtype tracklet_dtype
    '''
    tracklets = np.loadtxt(filename, dtype=tracklet_dtype, skiprows=1)

    return tracklets


def filter_identical_tags(detections):
    '''
    Filters out anu identical tag calls in the same frame to prevent possible
    downstream errors.
    Args:
        detections - sorted detections array
    Returns:
        n - number of tag detections filtered.
            (sets filtered tag_id, quality and angle to 0 in place)
    '''
    duplicate_mask = np.empty(detections.shape, dtype=bool)
    duplicate_mask[-1] = False
    duplicate_mask[:-1] = detections[['fr_idx', 'tag_id']][:-1] == \
        detections[['fr_idx', 'tag_id']][1:]
    duplicate_mask[1:] += duplicate_mask[:-1]
    duplicates = np.where(duplicate_mask)

    detections['tag_id'][duplicates] = 0
    detections['quality'][duplicates] = 0.
    detections['angle'][duplicates] = np.nan

    return duplicates[0].shape


def det_to_tra(detections, track_id_init=None):
    '''
    Converts detection_dtype array to tracklet_dtype array.
    Args:
        detections - array of dtype detection_dtype
        track_id_init - fill 'track_id' field with this. If None, leave empty
    Returns:
        array of dtype tracklet_dtype with 'track_id' set to track_id_init.
    '''
    assert detections.dtype == reduced_detection_dtype
    tracklets = np.empty(detections.shape, dtype=tracklet_dtype)
    for name in reduced_detection_dtype.names:
        tracklets[name] = detections[name]

    if track_id_init is not None:
        tracklets['track_id'] = track_id_init

    return tracklets


def construct_tag_tracklets(tag_detections, max_step=10, dist_thresh=100,
                            prev_tracklet=0):
    '''
    Takes a series of detections of a tag and links them up into tracklets
    which span up to max_gap missing detections.
    Args:
        tag_detections - array containing the tag detections
        dist_thresh - maximum distance between consecutive detections to be
            considered part of same track
        max_step - largest step spanning missing detections before a new
            tracklet is started
    Returns:
        tracklets - array of type tracklet_dtype
    '''
    fr_idx_diff = np.empty(tag_detections.shape, dtype=np.int64)
    fr_idx_diff[0] = max_step + 1
    fr_idx_diff[1:] = tag_detections['fr_idx'][1:] - \
        tag_detections['fr_idx'][:-1]

    dist_array = np.empty(tag_detections.shape, dtype=np.float64)
    dist_array[0] = dist_thresh + 1
    dist_array[1:] = np.sqrt(
        (tag_detections['x'][1:] - tag_detections['x'][:-1]) ** 2 +
        (tag_detections['y'][1:] - tag_detections['y'][:-1]) ** 2)

    tracklet_array = np.cumsum((fr_idx_diff > max_step) |
                               (dist_array > dist_thresh))

    tracklets = det_to_tra(tag_detections)
    tracklets['track_id'] = tracklet_array + prev_tracklet

    return tracklets


def construct_tracklets(detections, max_step=10, dist_thresh=100):
    '''
    Links detections into tracklets
    Args:
        detections - array of dtype detection_dtype
        max_step - maximum number of frames with detections missing
        dist_thresh - maximum distance between consecutive detections to be
            considered part of same track
    Returns:
        n_tracklets - number of tracklets constructed
        tracklets - array of dtype tracklets with same shape as detections
            (sorted in track_id order)
    '''
    prev_tracklet = -1
    tracklets_list = []
    tag_ids = np.unique(detections['tag_id'])
    i = 0
    for tag_id in tag_ids:
        print('\rtag id {:d}/{:d}'.format(i, tag_ids.shape[0]), end='')
        i += 1
        tag_detections = detections[detections['tag_id'] == tag_id]
        tag_tracklets = construct_tag_tracklets(
            tag_detections, max_step=max_step, dist_thresh=dist_thresh,
            prev_tracklet=prev_tracklet)
        tracklets_list.append(tag_tracklets)

        prev_tracklet = tracklets_list[-1][-1]['track_id']

    print()

    tracklets = np.concatenate(tracklets_list)

    return prev_tracklet + 1, tracklets


def get_isolated(tracklets, search_width=5):
    '''
    Finds isolated tag detections
    Args:
        tracklets - output from construct_tracklets
    Returns:
        isolated_mask - tracklets which are isolated
    '''
    tracklets.sort(order=['track_id', 'fr_idx', 'tag_id'])
    isolated_mask = np.ones(tracklets.shape, dtype=bool)
    for i in range(1, search_width + 1):
        i_mask = tracklets['track_id'][:-i] != tracklets['track_id'][i:]
        isolated_mask[:-i] *= i_mask
        isolated_mask[i:] *= i_mask

    return isolated_mask


def deidentify_isolated(tracklets):
    '''
    Second filtering step. Deidentifies isolated tag detections by setting
    their fields ['tag_id', 'quality', 'angle', 'track_id'] to zero.
    Preserves detection coordinates.
    Args:
        tracklets - output from construct_tracklets. Should be sorted in
            ascending track_id order, followed by frame index
    Returns:
        n_deidentified - number of tracklets deidentified
            operation performed inplace.
    '''
    isolated = get_isolated(tracklets)

    tracklets['tag_id'][isolated] = 0
    tracklets['quality'][isolated] = 0.
    tracklets['angle'][isolated] = np.nan
    tracklets['track_id'][isolated] = 0

    return isolated[0].shape


def relink_deidentified(tracklets, search_width=5, distance_thresh=50):
    '''
    Relink previously deidentified tag detections to existing tracklets, while
    inferring their correct identity.
    Args:
        tracklets - output from deidentify_isolated.
        search_width - number of frames either side of each tracklet to search
        distance_thresh - distacne threshold for relinking de-identified tags
    Returns: n_relinked - number of relinked tag detections
    '''
    mask1 = tracklets['tag_id'] == 0
    n_unlinked = np.sum(mask1)
    unlinked = tracklets[mask1]
    unlinked_frames = unlinked['fr_idx']
    distances = np.empty(unlinked.shape, dtype=np.float64)
    distances[:] = np.inf
    new_track_ids = np.zeros(unlinked.shape, dtype=tracklets.dtype['track_id'])
    new_tag_ids = np.zeros(unlinked.shape, dtype=tracklets.dtype['tag_id'])
    indexes = np.arange(len(distances), dtype=np.int64)

    for track_id in np.unique(tracklets['track_id']):
        if track_id == 0:
            continue  # de-identifed

        # Select unlinked detections which are near the tracklet
        tracklet_idxs = np.where(tracklets['track_id'] == track_id)
        tracklet_fr_idxs = tracklets['fr_idx'][tracklet_idxs]
        mask2 = (unlinked_frames >= tracklet_fr_idxs[0] - search_width) * \
            (unlinked_frames <= tracklet_fr_idxs[-1] + search_width)
        search_unlinked = unlinked[mask2]

        # Do not search in frames which already have an identified detection:
        mask3 = np.in1d(search_unlinked['fr_idx'], tracklet_fr_idxs,
                        assume_unique=True, invert=True)
        search_unlinked = search_unlinked[mask3]

        # Interpolate position and calculate distances
        x_interp = np.interp(search_unlinked['fr_idx'], tracklet_fr_idxs,
                             tracklets['x'][tracklet_idxs])
        y_interp = np.interp(search_unlinked['fr_idx'], tracklet_fr_idxs,
                             tracklets['y'][tracklet_idxs])
        track_distances = np.sqrt((search_unlinked['x'] - x_interp) ** 2 +
                                  (search_unlinked['y'] - y_interp) ** 2)

        # Link deidentified tag detections
        closer_mask = (distances[mask2][mask3] > track_distances) * \
            (track_distances <= distance_thresh)
        masked_indexes = indexes[mask2][mask3][closer_mask]
        distances[masked_indexes] = track_distances[closer_mask]
        new_track_ids[masked_indexes] = track_id
        new_tag_ids[masked_indexes] = tracklets['tag_id'][tracklet_idxs[0][0]]

    tracklets['track_id'][mask1] = new_track_ids
    tracklets['tag_id'][mask1] = new_tag_ids
    tracklets['quality'][mask1] = 1 / (distances / distance_thresh + 1)

    return n_unlinked - np.sum(tracklets['tag_id'] == 0)


def link_isolated(tracklets, search_width=5, distance_thresh=50):
    '''
    Link isolated tag detections to existing tracklets.
    Args:
        tracklets - output from deidentify_isolated.
        search_width - number of frames either side of each tracklet to search
        distance_thresh - distacne threshold for relinking de-identified tags
    Returns: n_linked - number of linked tag detections
    '''
    mask1 = get_isolated(tracklets, search_width)
    unlinked = tracklets[mask1]
    unlinked_frames = unlinked['fr_idx']
    distances = np.empty(unlinked.shape, dtype=np.float64)
    distances[:] = np.inf
    new_track_ids = np.zeros(unlinked.shape, dtype=tracklets.dtype['track_id'])
    indexes = np.arange(len(distances), dtype=np.int64)

    for track_id in np.unique(tracklets['track_id']):
        if track_id == 0:
            continue  # de-identifed

        # Select unlinked detections which are near the tracklet
        tracklet_idxs = np.where(tracklets['track_id'] == track_id)
        tracklet_fr_idxs = tracklets['fr_idx'][tracklet_idxs]
        mask2 = (unlinked_frames >= tracklet_fr_idxs[0] - search_width) * \
            (unlinked_frames <= tracklet_fr_idxs[-1] + search_width)
        search_unlinked = unlinked[mask2]

        # Do not search in frames which already have an identified detection:
        mask3 = np.in1d(search_unlinked['fr_idx'], tracklet_fr_idxs,
                        assume_unique=True, invert=True)
        search_unlinked = search_unlinked[mask3]

        # Interpolate position and calculate distances
        x_interp = np.interp(search_unlinked['fr_idx'], tracklet_fr_idxs,
                             tracklets['x'][tracklet_idxs])
        y_interp = np.interp(search_unlinked['fr_idx'], tracklet_fr_idxs,
                             tracklets['y'][tracklet_idxs])
        track_distances = np.sqrt((search_unlinked['x'] - x_interp) ** 2 +
                                  (search_unlinked['y'] - y_interp) ** 2)

        # Link deidentified tag detections
        closer_mask = (distances[mask2][mask3] > track_distances) * \
            (track_distances <= distance_thresh)
        masked_indexes = indexes[mask2][mask3][closer_mask]
        distances[masked_indexes] = track_distances[closer_mask]
        new_track_ids[masked_indexes] = track_id

    tracklets['track_id'][mask1] = new_track_ids

    n_linked = np.sum(new_track_ids > 0)
    return n_linked


def iteratively_link(tracklets, search_width=5, distance_thresh=50):
    '''
    Iteratively calls link_isolated until no more isolated tags can be linked
    Args:
        tracklets - output from deidentify_isolated.
        search_width - number of frames either side of each tracklet to search
        distance_thresh - distance threshold for relinking de-identified tags
    Returns:
        n_list - list n_linked
    '''
    n_list = []
    first = True
    while True:
        n_linked = link_isolated(tracklets, search_width, distance_thresh)

        if first:
            n_list.append(n_linked)
            first = False
            continue

        if n_linked == n_list[-1]:
            break

        n_list.append(n_linked)

    tracklets.sort(order=['fr_idx', 'track_id', 'tag_id', 'quality'])
    return n_list


def remove_worse_duplicates(tracklets):
    '''
    Perform after relink_deidentified. Removes all but the best relinking tag
    detections for each frame/tracklet. Another side-effect is sorting into
    chronological order
    Args:
        tracklets - which has been relinked
    Returns:
        n_removed - number of tag detections discarded
        filtered_tracklets - a copy of tracklets with fewer entries
    '''
    tracklets.sort(order=['fr_idx', 'track_id', 'tag_id', 'quality'])

    mask = np.empty(tracklets.shape, dtype=bool)
    mask[-1] = True
    mask[:-1] = tracklets[['fr_idx', 'tag_id', 'track_id']][:-1] != \
        tracklets[['fr_idx', 'tag_id', 'track_id']][1:]

    filtered_tracklets = tracklets[mask]
    n_removed = tracklets.shape[0] - filtered_tracklets.shape[0]

    return n_removed, filtered_tracklets


def iteratively_relink(tracklets, search_width=5, distance_thresh=50):
    '''
    Iteratively calls relink_deidentified then remove_worse_duplicates until
    no more deidentified tags can be linked.
    Args:
        tracklets - output from deidentify_isolated.
        search_width - number of frames either side of each tracklet to search
        distance_thresh - distance threshold for relinking de-identified tags
    Returns:
        n_list - list of tuples n_relinked, n_removed
    '''
    n_list = []
    while True:
        n_relinked = relink_deidentified(tracklets, search_width,
                                         distance_thresh)
        if n_relinked == 0:
            n_list.append((0, 0))
            break

        n_removed, tracklets = remove_worse_duplicates(tracklets)
        n_list.append((n_relinked, n_removed))

    return n_list


def old_tracklet_distance(track0, track1):
    '''
    Determines the tracklet distance between two tracklets, defined as the mean
    euclidean distance between the linear interpolations of of the tracklets,
    in the timespan where they overlap.
    Args:
        track0, track1 - tracklets to compare, of dtype tracklet_dtype. Assumed
            to be sorted into chronological order (fr_idx inreasing)
    Returns:
        distance - np.float64
    '''
    if track0[0]['fr_idx'] <= track1[0]['fr_idx']:
        first = track0
        secnd = track1
    else:
        first = track1
        secnd = track0

    if secnd[0]['fr_idx'] > first[-1]['fr_idx']:
        return np.inf

    start_fr = secnd[0]['fr_idx']
    end_fr = min(secnd[-1]['fr_idx'], first[-1]['fr_idx'])

    overlap = np.arange(start_fr, end_fr + 1)

    x_first = np.interp(overlap, first['fr_idx'], first['x'])
    y_first = np.interp(overlap, first['fr_idx'], first['y'])
    x_secnd = np.interp(overlap, secnd['fr_idx'], secnd['x'])
    y_secnd = np.interp(overlap, secnd['fr_idx'], secnd['y'])

    distances = np.sqrt((x_first - x_secnd) ** 2 + (y_first - y_secnd) ** 2)

    return np.mean(distances)


def tracklet_distance(tracklets, track_mask0, track_mask1, time_diff_thresh=5):
    '''
    Determines the tracklet distance between two tracklets
    Args:
        tracklets - tracklet_array
        track_mask0, track_mask1 - masks of tracks to compare
        time_diff_thresh - tracklets which are separated in time by more than
            this will be given a distance of np.inf
    Returns:
        distance - np.float64
    '''
    joint_track = tracklets[track_mask0 + track_mask1]

    dist = tr_dist(joint_track['x'], joint_track['y'],
                   joint_track['fr_idx'], joint_track['track_id'])

    if dist < 0:
        return np.inf
    else:
        return dist


def calculate_inverse_tracklet_distances(tracklets, time_diff_thresh=5,
                                         tracklet_distance_thresh=np.inf):
    '''
    Calculate all tracklet distances between overlapping tracklets and return
    their inverse (ie. 1 / distance) as a sparse matrix. Inverse is returned
    instead of actual distances for implementation reasons.
    Args:
        tracklets - tracklets array of dtype tracklet_dtype, must be sorted
            in ascending chronological order within each tracklet
        time_diff_thresh - tracklets which are separated in time by more than
            this will be given a distance of np.inf
        tracklet_distance_thresh - distance trheshold (only include distances
            less than this)
    Returns:
        inverse_distances - sparse upper triangular matrix of inverse tracklet
            distances in coo format
        track_ids - unique track ids
        start_idx - array with start fr_idx of each track
        end_idx - array with end fr_idx of each track
        track_id_mask_dict
    '''
    track_ids = np.unique(tracklets['track_id'])
    start_idx = np.empty(track_ids.shape, dtype=tracklets.dtype['fr_idx'])
    end_idx = np.empty_like(start_idx)
    track_id_mask_dict = {}
    for i in range(len(track_ids)):
        track_id_mask = tracklets['track_id'] == track_ids[i]
        track_id_mask_dict[track_ids[i]] = track_id_mask
        track_id_idx = np.where(track_id_mask)[0]
        start_idx[i] = tracklets[track_id_idx[0]]['fr_idx']
        end_idx[i] = tracklets[track_id_idx[-1]]['fr_idx']

    inverse_distances = sparse.dok_matrix(
        (track_ids[-1] + 1, track_ids[-1] + 1), dtype=np.float64)
    mask = np.empty(track_ids.shape, dtype=bool)
    for i in range(len(track_ids)):
        track_id0 = track_ids[i]
        if track_id0 == 0:
            continue

        mask[:i + 1] = False
        mask[i + 1:] = (end_idx[i + 1:] >= start_idx[i] - time_diff_thresh) * \
            (start_idx[i + 1:] <= end_idx[i] + time_diff_thresh)

        for track_id1 in track_ids[mask]:
            distance = tracklet_distance(
                tracklets, track_id_mask_dict[track_id0],
                track_id_mask_dict[track_id1], time_diff_thresh)

            if distance < tracklet_distance_thresh:
                try:
                    inverse_distances[track_id0, track_id1] = 1 / distance
                except ZeroDivisionError:
                    inverse_distances[track_id0, track_id1] = np.inf

    return (inverse_distances.asformat('coo'), track_ids, start_idx, end_idx,
            track_id_mask_dict)


def coo_argmax(a):
    '''
    Get the (row, column) index of the maximum value in a
    Args:
        a - coo matrix
    Returns:
        row, column - index
    '''
    assert isinstance(a, sparse.coo_matrix)
    index = a.data.argmax()
    row = a.row[index]
    col = a.col[index]

    return row, col


def coo_remove_rowcol(a, rowcols):
    '''
    Remove a row and column from a sparse matrix in coo format.
    Args:
        a - coo_matrix
        rowcols- rows/columns to remove (int or array of ints)
    Returns:
        filtered_array - a with rows/columns set to zero
    '''
    try:
        m = np.ones(a.data.shape, dtype=bool)
        for rowcol in rowcols:
            row_mask = a.row != rowcol
            col_mask = a.col != rowcol
            m *= row_mask * col_mask

    except TypeError:
        row_mask = a.row != rowcols
        col_mask = a.col != rowcols
        m = row_mask * col_mask

    return sparse.coo_matrix((a.data[m], (a.row[m], a.col[m])), shape=a.shape)


def recalculate_inverse_distances(tracklets, track_id, time_diff_thresh,
                                  tracklet_distance_thresh, inv_d,
                                  track_ids, start_idx, end_idx,
                                  track_id_mask_dict):
    '''
    Recalculate inverse tracklet distances for a single track_id
    Args:
        tracklets - tracklets array of dtype tracklet_dtype, must be sorted
            in ascending chronological order within each tracklet
        track_id - int ID of tracklet to calculate distances to
        time_diff_thresh - tracklets which are separated in time by more than
            this will be given a distance of np.inf
        tracklet_distance_thresh - distance trheshold (only include distances
            less than this)
        inv_d - inverse_distance matrix in coo format
        track_ids
        start_idx
        end_idx
        track_id_mask_dict
    Returns:
        inv_d_recalculated
    '''
    try:
        track0_mask = track_id_mask_dict[track_id]
    except KeyError:
        track0_mask = tracklets['track_id'] == track_id
        track_id_mask_dict[track_id] = track0_mask

    track0 = tracklets[track0_mask]
    track_start = track0[0]['fr_idx']
    track_end = track0[-1]['fr_idx']
    track_ids_mask = track_ids == track_id
    start_idx[track_ids_mask] = track_start
    end_idx[track_ids_mask] = track_end

    mask = (end_idx >= track_start - time_diff_thresh) * \
        (start_idx <= track_end + time_diff_thresh) * \
        (~track_ids_mask)

    data_list, col_list = [], []
    for track_id1 in track_ids[mask]:
        try:
            track1_mask = track_id_mask_dict[track_id1]
        except KeyError:
            track1_mask = tracklets['track_id'] == track_id1
            track_id_mask_dict[track_id1]

        distance = tracklet_distance(tracklets, track0_mask, track1_mask,
                                     time_diff_thresh)
        if distance < tracklet_distance_thresh:
            data_list.append(1 / distance)
            col_list.append(track_id1)
            # inv_d[track_id, track_id1] = 1 / distance

    data = np.concatenate([inv_d.data, np.array(data_list,
                                                dtype=inv_d.data.dtype)])
    cols = np.concatenate([inv_d.col, np.array(col_list,
                                               dtype=inv_d.col.dtype)])
    rows = np.concatenate([inv_d.row,
                           np.ones((len(data_list),),
                                   dtype=inv_d.row.dtype) * track_id])

    return sparse.coo_matrix((data, (rows, cols)), shape=inv_d.shape)


def merge_tracklets(tracklets, tracklet_distance_thresh=30,
                    time_diff_thresh=5):
    '''
    Merge close overlapping tracklets
    Args:
        tracklets - relinked tracklets array
        tracklet_distance_thresh - distance threshold for merging tracklets
        time_diff_thresh - tracklets which are separated in time by more than
            this will be given a distance of np.inf
    Returns:
        n_merged - number of old tracklets - number of new tracklets
    '''
    tracklets.sort(order=['fr_idx', 'track_id'])

    print('Calculating tracklet distance matrix')
    inverse_distances, track_ids, start_idx, end_idx, track_id_mask_dict = \
        calculate_inverse_tracklet_distances(tracklets, time_diff_thresh)

    old_unique_ids = np.unique(tracklets['track_id'])

    i = 0
    while inverse_distances.data.shape[0] > 0:
        print('\r%d/%d merged' % (i, inverse_distances.data.shape[0] + i),
              end='')
        i += 1
        track_id0, track_id1 = coo_argmax(inverse_distances)
        tracklets['track_id'][tracklets['track_id'] == track_id1] = track_id0
        track_id_mask_dict[track_id0] += track_id_mask_dict.pop(track_id1)

        inverse_distances = coo_remove_rowcol(inverse_distances,
                                              (track_id0, track_id1))
        remove_track_mask = track_ids != track_id1
        track_ids = track_ids[remove_track_mask]
        start_idx = start_idx[remove_track_mask]
        end_idx = end_idx[remove_track_mask]

        inverse_distances = recalculate_inverse_distances(
            tracklets, track_id0, time_diff_thresh, tracklet_distance_thresh,
            inverse_distances, track_ids, start_idx, end_idx,
            track_id_mask_dict)
    print('\r', end='')

    return old_unique_ids.shape[0] - np.unique(tracklets['track_id']).shape[0]


def iteratively_merge(tracklets, tracklet_distance_thresh=30,
                      time_diff_thresh=5):
    '''
    Iteratively calls merge_tracklets until no more tracklets can be linked
    '''
    n_list = []
    while True:
        n_merged = merge_tracklets(tracklets, tracklet_distance_thresh,
                                   time_diff_thresh)
        n_list.append(n_merged)
        if n_merged == 0:
            break

    return n_list


def tracklet_identity_vote(tracklets):
    '''
    Infer the identity of merged tracklets by voting
    Args:
        tracklets - merged tracklets array
    Returns:
        n_changed - number of tag detection identities changed
    '''
    n_changed = 0
    for track_id in np.unique(tracklets['track_id']):
        if track_id == 0:
            continue
        track_mask = tracklets['track_id'] == track_id
        tag_id_votes = tracklets['tag_id'][track_mask]

        vote_mode, count = stats.mode(tag_id_votes, axis=None)
        assert vote_mode.shape[0] == 1, \
            'Tag ID ambiguous. Dealing with this is not yet implemented.'
        inferred_tag_id = vote_mode[0]
        change_mask = track_mask * (tracklets['tag_id'] != inferred_tag_id)
        tracklets['tag_id'][change_mask] = inferred_tag_id
        tracklets['angle'][change_mask] = np.nan
        n_changed += np.sum(change_mask)

    return n_changed


def remap_track_ids(tracklets):
    '''
    Remaps track_ids to their minimal representation. New track_ids will go
    from 1 up to the number of unique tracklets.
    Args:
        tracklets - tracklet array
    Returns:
        None - operates inplace
    '''
    track_ids, inverse_idx = np.unique(tracklets['track_id'],
                                       return_inverse=True)
    new_idx = np.arange(track_ids.shape[0], dtype=track_ids.dtype)

    tracklets['track_id'] = new_idx[inverse_idx]

    return None


def select_subset(tracklets, fr_idx_min=None, fr_idx_max=None,
                  fr_idx_exact=None, tag_ids=None,
                  track_ids=None):
    '''
    Returns a view of tracklets according to the filter parameters
    Args:
        tracklets - tracklet array
        fr_idx_min - first fr_idx to include
        fr_idx_max - last fr_idx to include
        fr_idx_exact - exact fr_idx to include (exclude all others)
        tag_ids - set of tag ids to include (numpy array)
        track_ids - set of track ids to include (numpy array)
    Returns:
        tracklets_subset - tracklet array
    '''
    mask = np.ones(tracklets.shape, dtype=bool)

    if fr_idx_min is not None:
        mask *= tracklets['fr_idx'] >= fr_idx_min

    if fr_idx_max is not None:
        mask *= tracklets['fr_idx'] <= fr_idx_max

    if fr_idx_exact is not None:
        mask *= tracklets['fr_idx'] == fr_idx_exact

    if tag_ids is not None:
        mask *= np.in1d(tracklets['tag_id'], tag_ids)

    if track_ids is not None:
        mask *= np.in1d(tracklets['track_id'], track_ids)

    return tracklets[mask]


def annotate_frame(img, tracklets, fr_idx):
    '''
    Annotate video frame with tag ids
    Args:
        img - greyscale (or depth 3) video frame
        tracklets - tracklet array
        fr_idx - frame index of img with respect to the video it came from
    Returns:
        annotated_img - BGR image, same size as img.
    '''
    if img.shape[2] == 1:
        annotated_img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    elif img.shape[2] == 3:
        annotated_img = img.copy()

    detections = select_subset(tracklets, fr_idx_exact=fr_idx)

    for det in detections:
        cv2.circle(annotated_img, (det['x'], det['y']), det['r'],
                   (0, 0, 255), thickness=3)
        if np.isfinite(det['angle']):
            cv2.line(annotated_img, (det['x'], det['y']),
                     (int(det['x'] + det['r'] *
                          np.sin(np.pi * det['angle'] / 180)),
                      int(det['y'] - det['r'] *
                          np.cos(np.pi * det['angle'] / 180))), (0, 255, 0),
                     thickness=3)
        cv2.putText(annotated_img, '%d' % det['tag_id'],
                    (det['x'] + det['r'], det['y']), cv2.FONT_HERSHEY_SIMPLEX,
                    2, (255, 0, 0), thickness=3)
        cv2.putText(annotated_img, '%d' % det['track_id'],
                    (det['x'] + det['r'], det['y'] + det['r']),
                    cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), thickness=3)

    return annotated_img


def annotate_sequence(video_path, tracklets, fr_idx_start, fr_idx_end):
    '''
    Annotates a sequence of frames for visual inspection
    Args:
        video_path - path of video
        tracklets - tracklet array to use for annotation
        fr_idx_start - first frame to include in sequence
        fr_id_end - one after last frame to include in sequence
    Returns:
        (fr_idx_end - fr_idx_start)xNxMx3 array containing annotated images
    '''
    n_frames = fr_idx_end - fr_idx_start
    cap = cv2.VideoCapture(video_path)
    annotated_imgs = np.empty((0, 0, 0, 0), dtype=np.uint8)

    for fr_idx in range(fr_idx_end):
        ret, frame = cap.read()
        if ret is False:
            break

        if fr_idx == fr_idx_start:
            annotated_imgs = np.empty((n_frames,) + frame.shape,
                                      dtype=frame.dtype)
            ann_idx = 0

        if fr_idx >= fr_idx_start:
            annotated_imgs[ann_idx] = annotate_frame(frame, tracklets, fr_idx)
            ann_idx += 1

    cap.release()
    return annotated_imgs


def annotate_tracklet_sequence(video_path, tracklets, track_id):
    '''
    Select a tracklet and annotated the sequence of frames which contain that
    tracklet.
    Args:
        video_path - path of video
        tracklets - tracklet_array (assumed to be sorted into chronological
                                    order)
        track_id - track(s) to view
    '''
    idx = np.where(np.in1d(tracklets['track_id'], track_id))[0]
    fr_idx_start = tracklets['fr_idx'][idx].min()
    fr_idx_end = tracklets['fr_idx'][idx].max()

    return annotate_sequence(video_path, tracklets, fr_idx_start, fr_idx_end)


def view_sequence(annotated_sequence):
    '''
    Open a OpenCV window and view an annotated sequence
    Args:
        annotated_sequence - LxMxNx3 array
    Controls:
        <space> - pause/unpause
        <left_arrow>
        <=>/<+> - increase speed
        <-> - decrease speed
        <q> - quit
        <r> - replay
    Returns:
        None
    '''
    cv2.namedWindow('annotated', cv2.WINDOW_FULLSCREEN)
    paused = False
    target_fps = 5
    idx = 0
    time.sleep(1 / target_fps)
    while True:
        img = annotated_sequence[idx, ...].copy()
        cv2.putText(img, '%.2ffps' % target_fps,
                    (0, annotated_sequence.shape[1]),
                    cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 255))
        cv2.imshow('annotated', img)
        wait_time = int(1000 / target_fps)
        k = cv2.waitKey(wait_time) & 0xFF
        if k == ord(' '):
            paused = not paused
        elif k == 81:
            if idx > 0:
                idx -= 1
        elif k == 83:
            if idx < annotated_sequence.shape[0] - 1:
                idx += 1
        elif k == ord('='):
            target_fps *= 1.25
        elif k == ord('-'):
            target_fps *= 0.8
        elif k == ord('r'):
            idx = 0
        elif k == ord('q'):
            break

        if not paused:
            if idx < annotated_sequence.shape[0] - 1:
                idx += 1

    cv2.destroyAllWindows()
    for i in range(5):
        cv2.waitKey(1)

    return None


def annotate_video(video_in, video_out, tracklets, view=False):
    '''
    Re-encode video with annotation overlay.
    Args:
        video_in - path to input video
        video_out - path of output video
        tracklets - tracklet array
    Returns:
        None
    '''
    cap = cv2.VideoCapture(video_in)
    ret, frame = cap.read()
    assert ret is True, 'Failed to read. Check video_in'
    writer = cv2.VideoWriter(video_out, cv2.VideoWriter_fourcc(*'X264'),
                             5, (frame.shape[1], frame.shape[0]), isColor=True)

    if view is True:
        cv2.namedWindow('annotated', cv2.WINDOW_NORMAL)
    n_frames = tracklets['fr_idx'].max() + 1
    fr_idx = 0
    prev_time = time.clock()
    try:
        while True:
            annotated = annotate_frame(frame, tracklets, fr_idx)
            writer.write(annotated)
            if view is True:
                cv2.imshow('annotated', annotated)
                k = cv2.waitKey(1) & 0xFF
                if k == ord('q'):
                    break
            fr_idx += 1
            if fr_idx % 50 == 1:
                current_time = time.time()
                fps = 50 / (current_time - prev_time)
                prev_time = current_time
                print('\r%3.0f%%\t%5.2ffps' % (100 * fr_idx / n_frames, fps),
                      end='')
            ret, frame = cap.read()
            if not ret:
                break

    except KeyboardInterrupt:
        print('\nAborted by user')

    finally:
        cap.release()
        writer.release()
        if view is True:
            cv2.destroyAllWindows()
            for i in range(5):
                k = cv2.waitKey(1)

    return None


def write_file(path, array):
    '''
    Write a 1D array with fieldnames to a file.
    Args:
        path - path of file to write (overwrite)
        array - array to write
    '''
    assert len(array.shape) == 1, 'Array must be 1D'
    with open(path, 'w') as f:
        print(*(name for name in array.dtype.names), sep='\t', file=f)
        for e in array:
            print(*(e[name] for name in array.dtype.names), sep='\t', file=f)

    return None


def filter_short_tracks(tracklets, min_track_length):
    '''
    Removes short tracklets from track array
    Args:
        tracklets - tracklet array
        min_track_length - tracks with fewer detections will be removed
    Returns:
        n_removed
    '''
    track_ids, counts = np.unique(tracklets['track_id'], return_counts=True)
    tracks_to_keep = track_ids[counts >= min_track_length]
    mask = np.in1d(tracklets['track_id'], tracks_to_keep)
    tracklets = tracklets[mask]

    return track_ids.shape[0] - tracks_to_keep.shape[0]


def process_detections(detections, maxstep, timediffthresh, distancethresh,
                       trdistthresh, min_track_length):
    '''
    '''
    print('Removing same-frame identical tag IDs')
    n_filtered = filter_identical_tags(detections)
    print('%d detections filtered' % n_filtered)

    print('Constructing initial tracklets (max_step=%d)' % maxstep)
    n_tracklets, tracklets = construct_tracklets(detections, max_step=maxstep,
                                                 dist_thresh=distancethresh)
    print('%d tracklets constructed' % n_tracklets)

    #print('Linking isolated detections (search_width=%d, distance_thresh=%d)' %
    #      (timediffthresh, distancethresh))
    #n_linked_list = iteratively_link(tracklets, search_width=timediffthresh,
    #                                 distance_thresh=distancethresh)
    #n_iterations, tot_linked = 0, 0
    #for n_linked in n_linked_list:
    #    n_iterations += 1
    #    tot_linked += n_linked
    #    print('%d detections linked' % n_linked)
    #print('%d detections linked in %d iterations' % (tot_linked, n_iterations))

    print('Removing isolated detections min_track_length={:d}'.format(
          min_track_length))
    n_isolated = filter_short_tracks(tracklets, min_track_length)
    print('{:d} removed'.format(n_isolated))


    print('Merging tracklets (tracklet_distance_thresh=' +
          '%d, time_diff_thresh=%d)' % (trdistthresh, timediffthresh))
    n_merged = merge_tracklets(
        tracklets, tracklet_distance_thresh=trdistthresh,
        time_diff_thresh=timediffthresh)
    print('%d tracklets merged' % n_merged)

    print('Tracklet identity voting')
    n_changed = tracklet_identity_vote(tracklets)
    print('%d tag identities changed' % n_changed)

    print('Remapping track IDs... ', end='')
    remap_track_ids(tracklets)
    print('Done')

    return tracklets


def process_file(inputfile, outputfile, maxstep, timediffthresh,
                 distancethresh):
    '''
    Process tag detection file.
    Args:
        inputfile - path to file to process
        outputfile - output path
        maxstep -
        timediffthresh -
        distancethresh -
    '''
    print('Loading detections from %s' % inputfile)
    detections = load_detections(inputfile)
    detections.sort(order=['fr_idx', 'tag_id'])
    print('%d tag detections' % detections.shape[0])

    tracklets = process_detections(detections, maxstep,
                                   timediffthresh, distancethresh)

    print('Saving output to %s... ' % outputfile, end='')
    write_file(outputfile, tracklets)
    print('Done')


def parse_args():
    parser = argparse.ArgumentParser(description='''Processes detection file
                                     to increase reliablility of tag
                                     detections''')

    parser.add_argument('-i', '--inputfile', required=True, help='''Input tag
                        detection file.''')
    parser.add_argument('-o', '--outputfile', required=True, help='''Output
                        text file. (Will be overwritten).''')
    parser.add_argument('-m', '--maxstep', required=False, type=int,
                        default=10, help='''Maximum step size (gap) for joining
                        detections into tracklets.''')
    parser.add_argument('-t', '--timediffthresh', required=False, type=int,
                        default=5, help='''Maximum time gap to join tracklets
                        together.''')
    parser.add_argument('-d', '--distancethresh', required=False, type=int,
                        default=30, help='''Maximum distance to join tracklets
                        together.''')

    return parser.parse_args()


def main():
    '''
    Load, process and output processed tracklet file
    '''
    args = parse_args()
    process_file(args.inputfile, args.outputfile, args.maxstep,
                 args.timediffthresh, args.distancethresh)

if __name__ == '__main__':
    main()
