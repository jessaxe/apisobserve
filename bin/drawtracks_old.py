#!/usr/bin/env python

import argparse
from apisobserve import tracklets
import collections
import cv2
import numpy as np


class TagBuffer(dict):
    '''
    Class for storing tag location tails
    '''
    def add_tag(self, tag):
        '''
        Add a tag to the buffer
        Args:
            tag - a tracklets.tracklet_dtype value
        '''
        if tag['tag_id'] in self:
            self[tag['tag_id']].appendleft(tag)
        else:
            self[tag['tag_id']] = collections.deque([tag])

    def remove_old(self, fr_idx):
        '''
        Remove last tag in each track if it is older than fr_idx
        Args:
            fr_idx - int
        '''
        for tag_id in set(self):
            tag = self[tag_id].pop()
            if tag['fr_idx'] >= fr_idx:
                self[tag_id].append(tag)

            if len(self[tag_id]) == 0:
                self.pop(tag_id)


class TrackDrawer():
    '''
    Class for drawing tracks on a video sequence
    '''
    def __init__(self, vid_in, vid_out, track_array, tail_length, fps,
                 show_tags=True):
        '''
        Args:
            vid_in - path to video in file (or VideoCapture like object)
            vid_out - path to output reencoded video
            track_array - tag detections tracklet array
            tail_length - length of tails (in number of frames)
            fps - frames per second of video
            show_tags - if False, tags will not be drawn
        '''
        if isinstance(vid_in, str):
            self.cap = cv2.VideoCapture(vid_in)
            self.writer = cv2.VideoWriter(
                vid_out, cv2.VideoWriter_fourcc(*'X264'), fps,
                (int(self.cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
                 int(self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT))), isColor=True)
        else:
            self.cap = vid_in
            self.writer = cv2.VideoWriter(
                vid_out, cv2.VideoWriter_fourcc(*'X264'), fps,
                (self.cap.width, self.cap.height), isColor=True)

        if track_array.dtype == tracklets.tracklet_dtype:
            self.track_array = track_array
        else:
            raise ValueError('Wrong datatype for track_array')

        if isinstance(tail_length, int):
            self.tail_length = tail_length
        else:
            raise ValueError('tail_length must be an integer')

        def nullfn(*args, **kwargs):
            pass

        self.fr_idx = 0
        if self.tail_length == 0:
            self.draw_tails = nullfn
        elif self.tail_length > 0:
            self.tag_buffer = TagBuffer()
        else:
            raise ValueError('tail_length must be non-negative')

        if show_tags is False:
            self.draw_tags = nullfn

    def draw_tails(self, frame):
        '''
        Draw tails on frame based on information in self.tag_buffer
        Args:
            frame - image to draw on (with colour-depth 3)
        '''
        for track_id in self.tag_buffer:
            tail = self.tag_buffer[track_id]
            for i in range(len(tail) - 1):
                tag0 = tail[i]
                tag1 = tail[i + 1]
                cv2.line(frame,
                         (int(tag0['x']), int(tag0['y'])),
                         (int(tag1['x']), int(tag1['y'])),
                         (0, 255, 255), thickness=5)

        return None

    def draw_tags(self, frame, tags):
        '''
        Draws tag annotations on frame
        Args:
            frame - image to draw on
            tags - tracklets.tracklet_dtype array containing tags to draw
        '''
        for tag in tags:
            cv2.circle(frame, (tag['x'], tag['y']), tag['r'],
                       (255, 0, 0), thickness=3)
            if np.isfinite(tag['angle']):
                cv2.line(frame, (tag['x'], tag['y']),
                         (int(tag['x'] + tag['r'] *
                              np.sin(np.pi * tag['angle'] / 180)),
                          int(tag['y'] - tag['r'] *
                              np.cos(np.pi * tag['angle'] / 180))),
                         (0, 0, 255), thickness=3)
            cv2.putText(frame, '%d' % tag['tag_id'],
                        (tag['x'] + tag['r'], tag['y']),
                        cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), thickness=3)

        return None

    def next_frame(self):
        '''
        Load next frame, draw annotations on it, and reencode.
        Returns:
            ret - True if success, False if read failed (EOF)
        '''
        ret, frame = self.cap.read()
        if ret is False:
            return ret

        # Select track data for this frame
        tags = tracklets.select_subset(self.track_array,
                                       fr_idx_exact=self.fr_idx)

        # Update self.tag_buffer
        if self.tail_length > 0:
            for tag in tags:
                self.tag_buffer.add_tag(tag)

            self.tag_buffer.remove_old(self.fr_idx - self.tail_length)

        # Draw tails
        self.draw_tails(frame)

        # Draw tags
        self.draw_tags(frame, tags)

        # Write frame
        self.writer.write(frame)

        self.fr_idx += 1

        return ret

    def release(self):
        '''
        Releases self.cap and self.writer
        '''
        self.cap.release()
        self.writer.release()

        return None


def annotate_video(vid_in, vid_out, track_datafile, tail_length=10, fps=5,
                   n_frames=None, show_tags=True):
    '''
    Decode a video, draw on it and reencode.
    Args:
        vid_in - path to video in file
        vid_out - path to output reencoded video
        track_datafile - path to track data
        tail_length - length of tails (in number of frames)
        fps - frames per second of video
        n_frames (optional) - stop processing after n_frames
        show_tags - if False, tags will not be drawn
    '''
    track_array = tracklets.load_tracklets(track_datafile)
    track_array = track_array[track_array['track_id'] != 0]
    track_drawer = TrackDrawer(vid_in, vid_out, track_array, tail_length, fps,
                               show_tags)

    while True:
        ret = track_drawer.next_frame()

        if ret is False or track_drawer.fr_idx == n_frames:
            break

    track_drawer.release()

    return None


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input', required=True, nargs=2, type=str,
                        help='''Paths to raw video and track data''')
    parser.add_argument('output', type=str,
                        help='''Path to output video file''')

    parser.add_argument('-n', '--framenumber', type=int, default=None,
                        help='''Number of video frames to process''')
    parser.add_argument('-f', '--fps', type=float, default=5,
                        help='''Frames Per Second''')
    parser.add_argument('-l', '--taillength', type=int, default=10,
                        help='''Length of tag tails (in frames)''')
    parser.add_argument('-t', '--hidetags', action='store_false',
                        help='''Don't draw tags on frames (only tails)''')

    return parser.parse_args()


def main():
    args = parse_args()

    annotate_video(args.input[0], args.output, args.input[1],
                   tail_length=args.taillength, fps=args.fps,
                   n_frames=args.framenumber, show_tags=args.hidetags)


if __name__ == '__main__':
    main()
