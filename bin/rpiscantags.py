#!/usr/bin/env python
# Scan tags in FOV of Raspberry Pi camera

from apisobserve.tagid import TagDetector
from picamera import PiCamera
from picamera.array import PiRGBArray
from multiprocessing import Process, Queue
from queue import Full
import numpy as np
import time
import cv2
import Adafruit_CharLCD as LCD


POISON_PILL = 'POISON_PILL'


class PiCapture(object):
    def __init__(self, resolution=(1648, 1232), framerate=15):
        self.camera = PiCamera()
        self.camera.resolution = resolution
        self.camera.framerate = framerate
        self.capture = PiRGBArray(self.camera)
        self.camera.exposure_mode = 'snow'
        time.sleep(4)
        self.camera.shutter_speed = self.camera.exposure_speed
        self.camera.exposure_mode = 'off'
        awb = self.camera.awb_gains
        self.camera.awb_mode = 'off'
        self.camera.awb_gains = awb

    def __iter__(self):
        for frame in self.camera.capture_continuous(self.capture, format='bgr',
                                                    use_video_port=True):
            image = frame.array
            self.capture.truncate(0)
            yield image

    def close(self):
        self.camera.close()
        self.capture.close()


def child(queue):
    lcd = LCD.Adafruit_CharLCDPlate()
    lcd.message('Initialising...')
    print('Initialising tag detector')
    tag_detector = TagDetector(minRadius=20, maxRadius=50)
    processed = 0
    while True:
        msg = queue.get()
        if msg == POISON_PILL:
            break

        tag_list = tag_detector.detect_and_identify(msg[..., 2])
        tag_ids = [tag[4] for tag in tag_list]
        tag_ids.sort()
        print(tag_list)
        if len(tag_ids) == 0:
            lcd.set_color(1.0, 0.0, 0.0)
            lcd.clear()
        elif len(tag_ids) == 1:
            lcd.set_color(0.0, 1.0, 0.0)
            lcd.clear()
            lcd.message(str(tag_ids[0]))
        elif len(tag_ids) <= 3:
            lcd.set_color(0.0, 0.0, 1.0)
            lcd.clear()
            lcd.message(('{:04d}  ' * len(tag_ids)).format(*tag_ids))
        elif len(tag_ids) <= 6:
            lcd.set_color(0.0, 0.0, 1.0)
            lcd.clear()
            lcd.message(('{:04d}  ' * 3).format(*tag_ids[:3]) + '\n' + \
                    ('{:04d}  ' * len(tag_ids[3:])).format(*tag_ids[3:]))
        else:
            lcd.set_color(1.0, 0.0, 0.0)
            lcd.clear()
            lcd.message('Too many tags\nMaximum 6')

        print('Processed: {:d} frames'.format(processed))
        processed += 1

    lcd.clear()
    lcd.set_backlight(0.0)


def main():
    print('Making queue')
    q = Queue(1)
    print('Starting child process')
    child_p = Process(target=child, args=(q,))
    child_p.start()
    print('Making capture object')
    cap = PiCapture()
    i = 0

    print('Entering for loop')
    for img in cap:
        try:
            q.put_nowait(img)
        except Full:
            pass

        #if queue.empty():
        #    queue.put(img) 

        print(i)
        i += 1
        if i > 100:
            break

    print('\nExiting for loop')

    q.put(POISON_PILL)
    cap.close()
    child_p.join()


if __name__ == '__main__':
    main()
