#!/usr/bin/env python

import argparse
import datetime

from apisobserve import binnedcap, drawtracks, trackproc


def parse_args():
    dt = lambda s: datetime.datetime.strptime(s, '%Y-%m-%d-%H-%M-%S')

    parser = argparse.ArgumentParser(
        description = '''Extract, combine, and annotate frames with tag
            tracking data. This can be useful for investigating a particular
            time of interest in an experiment, or for validating the detection
            of a particular behaviour.''')

    parser.add_argument("commencement", type=dt, help='''Commencement time of
        experiment. Used to calculate start and stop frames of video encoding.
        ''')

    parser.add_argument("start", type=dt, help='''Time of first frame to
        process. Frame number is calculated based on fps, and the time of
        experiment commencement.''')

    parser.add_argument("stop", type=dt, help='''Time of last frame to
        process. Frame number is calculated based on fps, and the time of
        experiment commencement.''')

    parser.add_argument("outfile", help='''Path to output file. File will be
        encoded with h264 encoding by OpenCV. Suggestions for file extension
        are .mp4 or .avi depending on platform.''')

    parser.add_argument("--offsetsfile", required=True, help='''File containing
        offsets of each camera in the array of cameras.''')

    parser.add_argument("--videologs", nargs='+', help='''Paths to the video
        log files of each camera, which contain the paths to the video files
        along with the frame count of each file.''')

    parser.add_argument("--trackfiles", nargs='*', help='''Track data files in
        consecutive order.''')

    parser.add_argument("-s", "--framestep", default=300, type=int, help='''
        Number of frames in each track file. Default 300.''')

    parser.add_argument("-l", "--taillength", default=5, type=int, help='''
        Length of tails (in number of frames) to display when drawing tracks.
        Default 5.''')

    parser.add_argument("-f", "--fps", default=5, type=float, help='''Frames
        per second. Default 5.''')

    parser.add_argument("-r", "--resolution", default=[1640, 1232], type=int,
                        nargs=2, help='''Resolution of output video. Default
        1640 x 1232.''', metavar=("width", "height"))

    return parser.parse_args()


def main():
    args = parse_args()

    # Calculate frame indeces
    start_idx = binnedcap.idx_from_datetime(args.start, args.commencement,
                                            fps=args.fps)
    stop_idx = binnedcap.idx_from_datetime(args.stop, args.commencement,
                                           fps=args.fps)

    # Initialise video capture and seek to start frame
    cap = binnedcap.ArrayVideoCapture(args.offsetsfile, args.videologs)
    cap.set_fr_idx(start_idx)

    # Load track data
    track_loader = trackproc.FrTrackLoader(args.trackfiles, args.framestep)

    # Initialise tag drawer
    tag_drawer = drawtracks.TagDrawer(cap, track_loader, args.outfile,
                                      tail_length=args.taillength,
                                      fps=args.fps,
                                      out_size=args.resolution)

    # Run
    while cap.tot_fr_idx <= stop_idx:
        ret = tag_drawer.next_frame()
        if not ret:
            break

    tag_drawer.release()

if __name__ == '__main__':
    main()
