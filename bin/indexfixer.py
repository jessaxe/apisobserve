#!/usr/bin/env python

from datetime import datetime
import numpy as np
import os


def get_fixed_fr_idx(log_file, dt_fmt_str='%Y-%m-%d-%H-%M-%S-%f', fps=5.):
    '''
    Load a log file containing names of video files and the number of frames
    (-1) and infer the presence of dropped frames (won't be perfect) to get an
    array of frame index offsets.
    Args:
        log_file - path to video log file
        dt_fmt_str - format string for datetime.strptime describing how times
            are encoded in the filenames present in log_file
        fps - frames per second of the video files in log_file (default: 5.)
    Returns:
        fixed_fr_idx - an array with fixed index for every frame in the
            series of videos
    '''
    with open(log_file, 'r') as f:
        f.readline()
        filenames, frame_numbers = zip(
            *(l.rstrip('frames\n').split('\t') for l in f.readlines()))

    datetimes = [os.path.splitext(os.path.basename(f))[0] for f in filenames]
    datetimes = [datetime.strptime(dt, dt_fmt_str) for dt in datetimes]
    secs = np.array([(datetimes[i + 1] - datetimes[i]).total_seconds() for
                     i in range(len(datetimes) - 1)])

    actual_cumul = np.cumsum(
            np.array([int(f_num) for f_num in frame_numbers]) + 1)
    diff = (np.cumsum(secs * fps) - actual_cumul[:-1]).astype(int)

    fixed_fr_idx = np.arange(actual_cumul[-1], dtype=np.int64)
    for i in range(1, actual_cumul.shape[0]):
        fixed_fr_idx[actual_cumul[i]:actual_cumul[i + 1]] += diff[i - 1]

    return fixed_fr_idx


def main():
    pass

if __name__ == '__main__':
    main()
