#!/usr/bin/env python

import argparse
from ast import literal_eval
import glob
import numpy as np
import scipy as sp
from os import path, makedirs

from apisobserve.batch_frame_proc import BatchFrameProcessor
from apisobserve import groundtruth


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('-i', '--input-directory', type=str, help='''Path to
                        directory containing frame images.''', required=True)

    parser.add_argument('-e', '--file-extension', type=str, default='.png',
                        help='''File extension of frames in input-directory.
                        Defaults to ".png".''', required=False)

    parser.add_argument('-o', '--output', type=str, help='''Path to output
                        directory''', required=True)

    parser.add_argument('-p', '--preload-file', type=str, default=None,
                        help='''Optional. Path to previously used output file,
                        preloaded.''', required=False)

    parser.add_argument('-l', '--learnedparams', type=str, default=None,
                        help='''Optional path to learnedparams.log file used
                        for identifying tags quickly.''')

    parser.add_argument('-s', '--skip-autodetection', action='store_true',
                        help='''Skip automated detection step.''')

    return parser.parse_args()


def main():
    args = parse_args()
    print(args.skip_autodetection)

    makedirs(args.output, exist_ok=True)

    ## Load tag identification parameters
    if args.learnedparams is not None:
        with open(args.learnedparams, 'r') as f:
            tag_detector_params = literal_eval(f.readline())
    else:
        tag_detector_params = {}

    ## Load frame image files
    file_glob = '*'.join((args.input_directory, args.file_extension))

    frame_list = glob.glob(file_glob)
    frame_list.sort()

    ## Automated processing
    if args.skip_autodetection is False:
        img_array = np.array(
            [sp.misc.imread(frame, mode='L') for frame in frame_list],
            dtype=np.uint8)

        batch_processor = BatchFrameProcessor(args.output,
                                              tag_detector=tag_detector_params)
        batch_processor.process_frames(img_array, 'automatic-annotation')

    ## Manual processing
    with open(path.join(args.output, 'manual-annotation'), 'w') as output_file:
        groundtruth.process_frames(frame_list, output_file,
                                   preload_data_file=args.preload_file)

if __name__ == '__main__':
    main()
