#!/usr/bin/env python

import cv2
import numpy as np
import os
import skimage.io

try:
    from apisobserve import combalign
except ImportError:
    pass


class BgImgViewer():
    '''
    Provides a GUI for viewing a collection of background images.
    '''
    def __init__(self, imgs, frame_title='Background Image', wait_dur=50,
                 out_file=None, img_outdir=None):
        '''
        Args:
            imgs - skimage.io.ImageCollection object or str path to directory
            frame_title - title of window
            wait_dur - number of milliseconds between screen refreshes
            out_file - path to save observations in
            img_outdir - directory to save images in
        '''
        if isinstance(imgs, str):
            self.imgs = imreaders(imgs, weights=False)
        else:
            self.imgs = imgs

        self.shape = self.imgs[0].shape

        self.frame_title = frame_title
        self.wait_dur = wait_dur

        self.out_file = out_file
        self.img_outdir = img_outdir

        self.framebarname = 'minute'

        cv2.namedWindow(self.frame_title, cv2.WINDOW_NORMAL)
        cv2.createTrackbar(self.framebarname, self.frame_title, 0,
                           len(self.imgs) - 1, self._nothing)
        self.idx = None
        self.set_idx()

        self.fn_dict = {}

        self.add_command('0', self.zero_time)
        self.add_command('?', self.print_keybindings)

        self.add_command('f', self.next_time)
        self.add_command('d', self.prev_time)

        self.add_command('F', self.next_time, n=100)
        self.add_command('D', self.prev_time, n=100)

        if self.img_outdir is not None:
            self.add_command('s', self.save_img)

    def _nothing(self, *args):
        pass

    def add_command(self, key, fn, **kwargs):
        '''
        Add a keypress command to the viewer.
        Args:
            key - str of length 1 with keybinding
            fn - function to call on keypress detection
            kwargs passed to fn
        '''
        if isinstance(key, str) and len(key) == 1:
            self.fn_dict[ord(key)] = fn, kwargs
        else:
            raise ValueError('key must be a string of length 1')

    def zero_time(self):
        '''
        Jump to minute 0
        '''
        cv2.setTrackbarPos(self.framebarname, self.frame_title, 0)

    def next_time(self, n=1):
        '''
        Step forward 1min (shift - 10min)
        '''
        cv2.setTrackbarPos(self.framebarname, self.frame_title, self.idx + n)

    def prev_time(self, n=1):
        '''
        Step backward 1min (shift - 10min)
        '''
        cv2.setTrackbarPos(self.framebarname, self.frame_title, self.idx - n)

    def print_keybindings(self):
        '''
        Print keybindings
        '''
        print('keybindings:')
        print('q: quit')
        for key in sorted(list(self.fn_dict)):
            print('{:s}: {:s}'.format(chr(key),
                                      self.fn_dict[key][0].__doc__.strip()))

    def save_img(self):
        '''
        Save current image to img_outdir
        '''
        outpath = os.path.join(self.img_outdir, '{:06d}.png'.format(self.idx))
        print('Saving to {:s}'.format(outpath))
        skimage.io.imsave(outpath, self.img)

    def reset_img(self):
        self.img = self.imgs[self.idx]

    def set_idx(self, idx=None):
        '''
        Sets the frame index based on the trackbar.
        Args:
            idx - optionally parse index manually instead of querying trackbar
        '''
        if idx is None:
            idx = cv2.getTrackbarPos(self.framebarname, self.frame_title)

        if self.idx != idx:
            self.idx = idx
            self.reset_img()

        return None

    def display(self):
        '''
        Display image and return ASCII code of keypress
        Returns:
            int
        '''
        self.set_idx()
        cv2.imshow(self.frame_title, self.img)

        return cv2.waitKey(self.wait_dur) & 0xFF

    def run(self):
        '''
        Run GUI
        '''
        while True:
            k = self.display()
            if k in self.fn_dict:
                self.fn_dict[k][0](**self.fn_dict[k][1])
            elif k == ord('q'):
                break

        cv2.destroyAllWindows()
        for i in range(5):
            cv2.waitKey(1)


class CellImgViewer(BgImgViewer):
    '''
    Provides a GUI for viewing cells in a collection of background images.
    '''
    def __init__(self, imgs, boxes,
                 frame_title='Cell Image <?> - help <q> - quit', wait_dur=50,
                 out_file=None, **kwargs):
        '''
        Args:
            imgs - skimage.io.ImageCollection object or str path to directory
            boxes - array containing bounding boxes of cells (n, 4) shape
            frame_title - title of window
            wait_dur - number of milliseconds between screen refreshes
        '''
        self.box = boxes[0]
        super().__init__(imgs, frame_title=frame_title, wait_dur=wait_dur,
                         out_file=out_file, **kwargs)
        self.boxes = boxes

        self.cellbarname = 'cell'
        cv2.createTrackbar(self.cellbarname, self.frame_title, 0,
                           len(self) - 1, self._nothing)
        self.cell_idx = None
        self.set_cell_idx()

        self.add_command('h', self.move_box_left)
        self.add_command('j', self.move_box_down)
        self.add_command('k', self.move_box_up)
        self.add_command('l', self.move_box_right)

        self.add_command('n', self.next_cell)
        self.add_command('p', self.prev_cell)

    def __len__(self):
        return self.boxes.shape[0]

    def move_box_up(self):
        '''
        Move current box up one pixel
        '''
        if self.box[1] > 0:
            self.box[1] -= 1
            self.box[3] -= 1

        self.reset_img()

    def move_box_left(self):
        '''
        Move current box left one pixel
        '''
        if self.box[0] > 0:
            self.box[0] -= 1
            self.box[2] -= 1

        self.reset_img()

    def move_box_down(self):
        '''
        Move current box down one pixel
        '''
        if self.box[3] < self.shape[0]:
            self.box[1] += 1
            self.box[3] += 1

        self.reset_img()

    def move_box_right(self):
        '''
        Move current box right one pixel
        '''
        if self.box[2] < self.shape[1]:
            self.box[0] += 1
            self.box[2] += 1

        self.reset_img()

    def next_cell(self):
        '''
        Switch to next cell
        '''
        cv2.setTrackbarPos(self.cellbarname, self.frame_title,
                           self.cell_idx + 1)

    def prev_cell(self):
        '''
        Switch to previous cell
        '''
        cv2.setTrackbarPos(self.cellbarname, self.frame_title,
                           self.cell_idx - 1)

    def save_img(self):
        '''
        Save current image to img_outdir
        '''
        outpath = os.path.join(self.img_outdir, '{:02d}-{:06d}.png'.format(
            self.cell_idx, self.idx))
        print('Saving to {:s}'.format(outpath))
        skimage.io.imsave(outpath, self.img)

    def reset_img(self):
        self.img = self.imgs[self.idx][self.box[1]:self.box[3],
                                       self.box[0]:self.box[2]]

    def set_cell_idx(self, cell_idx=None):
        '''
        Sets the cell index based on the trackbar.
        Args:
            idx - optionally parse index manually instead of querying trackbar
        '''
        if cell_idx is None:
            cell_idx = cv2.getTrackbarPos(self.cellbarname, self.frame_title)

        if self.cell_idx != cell_idx:
            self.cell_idx = cell_idx
            self.box = self.boxes[self.cell_idx]
            self.reset_img()

        return None

    def set_idx(self, idx=None):
        '''
        Sets the frame index based on the trackbar.
        Args:
            idx - optionally parse index manually instead of querying trackbar
        '''
        if idx is None:
            idx = cv2.getTrackbarPos(self.framebarname, self.frame_title)

        if self.idx != idx:
            self.idx = idx
            self.img = self.imgs[self.idx][self.box[1]:self.box[3],
                                           self.box[0]:self.box[2]]

        return None

    def display(self):
        '''
        Display image and return ASCII code of keypress
        Returns:
            int
        '''
        self.set_cell_idx()
        return super().display()


class ManualCellAnnotator(CellImgViewer):
    '''
    Provides a GUI for viewing and annotating cells in a collection of
    background images.
    '''
    def __init__(self, *args, offset=[0, 0], **kwargs):
        '''
        Args:
            offset - offset of camera (for boxes output)
        '''
        super().__init__(*args, **kwargs)

        if isinstance(offset, np.ndarray):
            offset = list(offset)
        assert isinstance(offset, list) and len(offset) == 2
        self.offset = np.array(offset * 2, dtype=int)

        self.hatch_points = [-1 for i in range(len(self))]
        self.kill_points = [-1 for i in range(len(self))]
        self.cap_points = [[-1, -1] for i in range(len(self))]
        self.turning = ['n' for i in range(len(self))]

        self.add_command('H', self.hatch_command)
        self.add_command('K', self.kill_command)
        self.add_command('C', self.cap_command)
        self.add_command('P', self.print_tois)
        self.add_command('w', self.save_tois)

        self.add_command('L', self.turning_left_command)
        self.add_command('R', self.turning_right_command)
        self.add_command('S', self.turning_change_command)

    def hatch_command(self):
        '''
        Indicate hatch point for current cell
        '''
        self.hatch_points[self.cell_idx] = self.idx
        print('Setting hatch time of cell {:d} to {:d}min'.format(
            self.cell_idx, self.idx))

    def kill_command(self):
        '''
        Indicate larval death for current cell
        '''
        self.kill_points[self.cell_idx] = self.idx
        print('Setting death time of cell {:d} to {:d}min'.format(
            self.cell_idx, self.idx))

    def cap_command(self):
        '''
        Indicate start/end of capping
        '''
        if self.cap_points[self.cell_idx][0] == -1:
            self.cap_points[self.cell_idx][0] = self.idx
            print('Setting cap start time of cell {:d} to {:d}min'.format(
                self.cell_idx, self.idx))
        elif self.cap_points[self.cell_idx][1] == -1:
            self.cap_points[self.cell_idx][1] = self.idx
            print('Setting cap end time of cell {:d} to {:d}min'.format(
                self.cell_idx, self.idx))
        else:
            self.cap_points[self.cell_idx][0] = -1
            self.cap_points[self.cell_idx][1] = -1
            print('Capping for cell {:d} deleted. Please re-enter'.format(
                self.cell_idx))

    def turning_left_command(self):
        '''
        Indicate that larva monotonously turns left
        '''
        print("Setting turning of larva {:d} to 'l'".format(self.cell_idx))
        self.turning[self.cell_idx] = 'l'

    def turning_right_command(self):
        '''
        Indicate that larva monotonously turns right
        '''
        print("Setting turning of larva {:d} to 'r'".format(self.cell_idx))
        self.turning[self.cell_idx] = 'r'

    def turning_change_command(self):
        '''
        Indicate that larva does not monotonously turn in one direction
        '''
        print("Setting turning of larva {:d} to 's'".format(self.cell_idx))
        self.turning[self.cell_idx] = 's'

    def print_tois(self, out_file=None, cell_idx=None):
        '''
        Print times of interest for current cell
        '''
        if cell_idx is None:
            cell_idx = self.cell_idx

        print('cell #{:d}'.format(cell_idx), file=out_file)
        print('box: {:d} {:d} {:d} {:d}'.format(
            *(self.boxes[cell_idx] + self.offset)), file=out_file)
        if self.hatch_points[cell_idx] is not None:
            print('hatchpoint: {:d}min'.format(
                self.hatch_points[cell_idx]), file=out_file)

        if self.kill_points[cell_idx] is not None:
            print('killpoint: {:d}min'.format(
                self.kill_points[cell_idx]), file=out_file)

        if self.cap_points[cell_idx][0] is not None:
            print('capstartpoint: {:d}min'.format(
                self.cap_points[cell_idx][0]), file=out_file)

        if self.cap_points[cell_idx][1] is not None:
            print('capendpoint: {:d}min'.format(
                self.cap_points[cell_idx][1]), file=out_file)

    def save_tois(self):
        '''
        Write times of interest for each cell to a file
        '''
        print('Saving to {:s}'.format(self.out_file))
        with open(self.out_file, 'w') as f:
            f.write('cell\tb0\tb1\tb2\tb3\thatch\tkill\tcapst\tcapen\tturn\n')
            for cell_idx in range(len(self)):
                b0, b1, b2, b3 = self.boxes[cell_idx] + self.offset
                f.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(
                    cell_idx, b0, b1, b2, b3, self.hatch_points[cell_idx],
                    self.kill_points[cell_idx], self.cap_points[cell_idx][0],
                    self.cap_points[cell_idx][1], self.turning[cell_idx]))


class MultiCameraArray():
    '''
    Provides interface for dealing with multiple concurrent
    skimage.io.ImageCollection instances
    '''
    def __init__(self, dirs, offsets=None):
        '''
        Args:
            dirs - list of directories from which to load image collections
        '''
        self.img_collections = [imreaders(directory, weights=False)
                                for directory in dirs]
        if offsets is None:
            self.offsets = np.zeros((len(dirs), 2), dtype=int)
        else:
            self.offsets = offsets
        self.order = np.arange(len(dirs))

    def __len__(self):
        return min((len(collection) for collection in self.img_collections))

    def __getitem__(self, key):
        if isinstance(key, int):
            imgs = [collection[key] for collection in self.img_collections]
            height = max((imgs[i].shape[0] + self.offsets[i, 0]
                          for i in range(len(self.img_collections))))
            width = max((imgs[i].shape[1] + self.offsets[i, 1]
                         for i in range(len(self.img_collections))))

            img = np.zeros((height, width), dtype=np.uint8)
            for i in self.order:
                end0 = imgs[i].shape[0] + self.offsets[i, 0]
                end1 = imgs[i].shape[1] + self.offsets[i, 1]
                img[self.offsets[i, 0]:end0, self.offsets[i, 1]: end1] = \
                    imgs[i]

        else:
            raise ValueError('Must be indexed by integer')

        return img

    def fix_offsets(self):
        '''
        Changes origin for offsets. Call after every change to self.offsets
        '''
        min_coords = np.amin(self.offsets, axis=0)
        min_coords.shape = (1,) + min_coords.shape
        self.offsets -= min_coords

    def reorder(self):
        '''
        Change the display order of the images
        '''
        self.order = np.roll(self.order, 1)


class MultiCameraViewer(BgImgViewer):
    '''
    Provides GUI for combining multiple tessallating cameras
    '''
    def __init__(self, dirs, frame_title='Multi Background', wait_dur=50,
                 out_file=None, offsets='tiled', **kwargs):
        '''
        Args:
            dirs - list of str paths to directories
            frame_title - title of window
            wait_dur - number of milliseconds between screen refreshes
            offsets - passed to MultiCameraArray
        '''
        imgs = MultiCameraArray(dirs, offsets=offsets)
        super().__init__(imgs, frame_title=frame_title, wait_dur=wait_dur,
                         out_file=out_file, **kwargs)
        self.prev_x = 0
        self.prev_y = 0

        self.add_command('r', self.imgs.reorder)

        self.add_command('h', self.move_left)
        self.add_command('j', self.move_down)
        self.add_command('k', self.move_up)
        self.add_command('l', self.move_right)

        self.add_command('w', self.save_offsets)

        cv2.setMouseCallback(self.frame_title, self.mouse_callback)

    def move_up(self):
        '''
        Move selected image up one pixel
        '''
        self.imgs.offsets[self.imgs.order[-1], 0] -= 1
        self.imgs.fix_offsets()

    def move_down(self):
        '''
        Move selected image down one pixel
        '''
        self.imgs.offsets[self.imgs.order[-1], 0] += 1
        self.imgs.fix_offsets()

    def move_left(self):
        '''
        Move selected image left one pixel
        '''
        self.imgs.offsets[self.imgs.order[-1], 1] -= 1
        self.imgs.fix_offsets()

    def move_right(self):
        '''
        Move selected image right one pixel
        '''
        self.imgs.offsets[self.imgs.order[-1], 1] += 1
        self.imgs.fix_offsets()

    def mouse_callback(self, event, x, y, flags, userdata=None):
        '''
        Move the uppermost image with the mouse
        '''
        if event == cv2.EVENT_MOUSEMOVE:
            if flags & cv2.EVENT_FLAG_LBUTTON:
                self.imgs.offsets[self.imgs.order[-1], 0] += y - self.prev_y
                self.imgs.offsets[self.imgs.order[-1], 1] += x - self.prev_x
                self.imgs.fix_offsets()

        elif event == cv2.EVENT_LBUTTONDBLCLK:
            self.imgs.reorder()

        self.prev_x = x
        self.prev_y = y
        self.reset_img()

    def save_offsets(self):
        '''
        Save offsets to file
        '''
        print('Saving to {:s}'.format(self.out_file))
        if self.out_file is not None:
            with open(self.out_file, 'w'):
                pass

        for i in range(self.imgs.offsets.shape[0]):
            s = 'incubator{:02d}: {:d} {:d}'.format(
                i + 1, *self.imgs.offsets[i])
            if self.out_file is None:
                print(s)
            else:
                with open(self.out_file, 'a') as f:
                    f.write(s + '\n')


class BGSequencer():
    '''
    Produces a sequence of refined background images
    '''
    def __init__(self, directory, frames_per_image=60):
        '''
        Args:
            directory - str containing directory of of background images
            frames_per_image - the number of frames to use for background
                refinement. Defaults to 60
        '''
        self.imgs, self.weights = imreaders(directory, weights=True)
        self.frames_per_image = frames_per_image
        self.curr_idx = 0
        self.scale_factor = 1 / 255
        self.done = False

    def get_next(self):
        '''
        Load images, calculate weighted average and return the next refined
        background image.
        Returns:
            bg_img
        '''
        bg = np.zeros(self.imgs[self.curr_idx].shape, dtype=np.float64)
        wt = bg.copy()
        for i in range(self.frames_per_image):
            try:
                weight = self.scale_factor * self.weights[self.curr_idx]
                wt += weight
                bg += weight * self.imgs[self.curr_idx]
                self.curr_idx += 1
            except IndexError:
                self.done = True
                break

        return (bg / wt).astype(np.uint8)


def imreaders(directory, weights=True):
    '''
    Initialise two ImageCollection objects, one for bg images, and one for
    their respective weights images.
    Args:
        directory - bg directory containing png files, with a 'weights' sub-
            directory containing the same number of png files
        weights - load weights as well as bg images (default True)
    Returns:
        bg_imgs
        [wt_imgs]
    '''
    bg_imgs = skimage.io.imread_collection(
        os.path.join(directory, '*.png'), conserve_memory=True)

    if weights is True:
        wt_imgs = skimage.io.imread_collection(
            os.path.join(directory, 'weights/*.png'), conserve_memory=True)

        assert len(bg_imgs) == len(wt_imgs), 'Differing number of images!'

        return bg_imgs, wt_imgs

    elif weights is False:
        return bg_imgs

    else:
        raise ValueError('weights must be True or False')


def progressive_bg(directory, display=False, tol=5):
    '''
    View background reconstruction for comb alignment
    Args:
        directory - bg directory containing png files, with a 'weights' sub-
            directory containing the same number of png files
        display - display progress of background image
        tol - once this many 'weighted minutes' of data is reached, end
    Returns:
        bg_img - reconstruction of background
    '''
    bg_imgs, wt_imgs = imreaders(directory)

    bg_img = np.zeros(bg_imgs[0].shape, dtype=np.float64)
    scale_factor = 1 / 255
    wt_img = np.ones(bg_imgs[0].shape, dtype=np.float64) * scale_factor

    if display is True:
        cv2.namedWindow('Background Reconstruction', cv2.WINDOW_NORMAL)

    for i in range(len(bg_imgs)):
        weight = scale_factor * wt_imgs[i]
        wt_img += weight
        prop_weight = weight / wt_img
        bg_img = bg_img * (1 - prop_weight) + bg_imgs[i] * prop_weight

        if np.min(wt_img) > tol:
            break

        if display is True:
            cv2.imshow('Background Reconstruction', bg_img.astype(np.uint8))
            k = cv2.waitKey(1) & 0xFF

            if k == ord('q'):
                break

    if display is True:
        cv2.destroyAllWindows()
        for i in range(10):
            cv2.waitKey(1)

    return bg_img.astype(np.uint8)


def load_offsets(path):
    '''
    Load offsets from a file
    Args:
        path - path to file
    Returns:
        offsets - np.ndarray
    '''
    offsets = []
    with open(path, 'r') as f:
        for line in f:
            offset = [int(s) for s in line.split()[-2:]]
            offsets.append(offset)

    return np.array(offsets)


def refined_bg_sequence(in_dir, frames_per_image=60):
    '''
    Produce refined background images from pngs in in_dir, and save them to
    a subdirectory of in_dir called refined
    '''
    bg_refiner = BGSequencer(in_dir, frames_per_image=frames_per_image)
    out_dir = os.path.join(in_dir, 'refined')
    os.makedirs(out_dir)
    out_path = os.path.join(out_dir, '{:05d}.png')

    i = 0
    while bg_refiner.done is False:
        bg_img = bg_refiner.get_next()
        skimage.io.imsave(out_path.format(i), bg_img)
        i += 1


def array_sequence(dirs, offsets, out_dir, out_shape=(720, 1280)):
    '''
    '''
    imgs = MultiCameraArray(dirs, offsets=offsets)
    os.makedirs(out_dir)
    out_path = os.path.join(out_dir, '{:05}.png')
    f = min(out_shape[0] / imgs[0].shape[0], out_shape[1] / imgs[0].shape[1])

    for i in range(len(imgs)):
        img = cv2.resize(imgs[i], None, fx=f, fy=f,
                         interpolation=cv2.INTER_AREA)
        out_img = np.zeros(out_shape, dtype=np.uint8)
        r0 = (out_img.shape[0] - img.shape[0]) // 2
        r1 = r0 + img.shape[0]
        c0 = (out_img.shape[1] - img.shape[1]) // 2
        c1 = c0 + img.shape[1]
        out_img[r0:r1, c0:c1] = img
        skimage.io.imsave(out_path.format(i), out_img)

    return None



def main():
    # directory = '/home/jesse/dataoutput/2016-09-14-15-53/incubator01/bg'

    # viewer = BgImgViewer(directory)

    dirs = ['/home/jesse/dataoutput/2016-09-14-15-53/incubator01/bg',
            '/home/jesse/dataoutput/2016-09-14-15-53/incubator02/bg',
            '/home/jesse/dataoutput/2016-09-14-15-53/incubator03/bg',
            '/home/jesse/dataoutput/2016-09-14-15-53/incubator04/bg']

    print('Loading images')
    offsetfile = '/home/jesse/dataoutput/2016-09-14-15-53/camoffsets'
    img_outdir = '/home/jesse/dataoutput/2016-09-14-15-53/sampleimgs'
    os.makedirs(img_outdir, exist_ok=True)
    viewer = MultiCameraViewer(
        dirs,
        offsets=load_offsets(offsetfile),
        out_file=offsetfile,
        img_outdir=img_outdir)
    viewer.print_keybindings()
    viewer.run()
    offsets = viewer.imgs.offsets

    for i in range(len(dirs)):
        print('Processing directory {:d}'.format(i))
        directory = dirs[i]
        offset = list(offsets[i])

        print('Combining background images')
        bg = progressive_bg(directory)
        print('Detecting cells')
        boxes = combalign.get_boxes(bg)

        print('Opening cell annotator')
        viewer = ManualCellAnnotator(
            directory, boxes,
            out_file='/home/jesse/dataoutput/2016-09-14-15-53/incubator{:02d}/celldata.tab'.format(i + 1),
            offset=offset,
            img_outdir=img_outdir)
        if i == 0:
            viewer.print_keybindings()

        viewer.run()

if __name__ == '__main__':
    main()
