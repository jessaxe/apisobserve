#!/usr/bin/env python

import argparse
import cv2
import io
import numpy as np
import os
import socket
import struct
import subprocess
import sys
import time
from tkinter import Tk


def get_window_location(i, grid_tup):
    assert i < grid_tup[0] * grid_tup[1]

    root = Tk()
    screen_w = root.winfo_screenwidth()
    screen_h = root.winfo_screenheight()

    h_pos = (i % grid_tup[0]) * screen_w // grid_tup[0]
    v_pos = ((i // grid_tup[0]) % grid_tup[1]) * screen_h // grid_tup[1]

    return h_pos, v_pos


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', type=str, required=False, help='''ip address or
                        domain name of server (this computer)''',
                        default='192.168.1.2')
    parser.add_argument('-p', metavar='startport', type=int, required=False,
                        default=48620,
                        help='start port of server to connect to')
    parser.add_argument('-fw', metavar='width', type=int, required=False,
                        default=640, help='Frame width')
    parser.add_argument('-fh', metavar='height', type=int, required=False,
                        default=480, help='Frame height')
    parser.add_argument('-fr', metavar='framerate', type=float, required=False,
                        default=25., help='Frame rate')
    parser.add_argument('-iso', type=int, required=False, default=0,
                        help='ISO value')
    parser.add_argument('ip', nargs='+', type=int, help='''End of ip address of
                        raspberry pi to stream from (accepts multiple entries)
                        (concatenated to 192.168.1.)''')
    return parser.parse_args()


def main():
    args = parse_args()
    i = -1
    for ip in args.ip:
        i += 1
        pid = os.fork()
        if pid == 0:
            break
    if pid !=0:
        sys.exit(0)
    # Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
    # all interfaces)
    port = args.p + i
    server_socket = socket.socket()
    server_socket.bind(('0.0.0.0', port))
    server_socket.listen(0)

    # Start stream
    subprocess.call(
        ("ssh jesse@192.168.1.10 'ssh pi@192.168.1.%d " + \
         "/home/pi/picamera_network_stream.py " + \
         "-a %s -p %d -fw %d -fh %d -fr %d -iso %d' &") % \
        (ip, args.a, port, args.fw, args.fh, args.fr, args.iso), shell=True)
    time.sleep(2)

    # Accept a single connection and make a file-like object out of it
    connection = server_socket.accept()[0].makefile('rb')
    cv2.namedWindow('stream%d' % ip, cv2.WINDOW_KEEPRATIO)
    x, y = get_window_location(i, (4, 3))
    cv2.moveWindow('stream%d' % ip, x, y)
    try:
        while True:
            # Read the length of the image as a 32-bit unsigned int. If the
            # length is zero, quit the loop
            image_len = struct.unpack(
                '<L', connection.read(struct.calcsize('<L')))[0]
            if not image_len:
                break
            # Construct a stream to hold the image data and read the image
            # data from the connection
            image_stream = io.BytesIO()
            image_stream.write(connection.read(image_len))
            # Rewind the stream, open it with openCV and display
            image_stream.seek(0)
            # Construct a numpy array from the stream
            data = np.fromstring(image_stream.getvalue(), dtype=np.uint8)
            image = data[: args.fw * args.fh].reshape((args.fh, args.fw))
            cv2.imshow('stream%d' % ip, image)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('q'):
                break
    finally:
        connection.close()
        server_socket.close()

if __name__ == '__main__':
    main()
