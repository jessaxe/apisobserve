#!/usr/bin/env python

from apisobserve import tagid, binnedcap
import cv2
import numpy as np
import os


def run_gui(cap, out_dir):
    '''
    Run through file, detecting tags. Save tags to out_dir/tagimg/ or
    out_dir/nontagimg/ depending on user input.
    Args:
        cap - VideoCapture like object
        out_dir - directory to store output tags
    Controls:
        j - tag
        k - nontag
        q - quit
    '''
    tag_path = os.path.join(out_dir, 'tagimg')
    nontag_path = os.path.join(out_dir, 'nontagimg')
    os.makedirs(tag_path, exist_ok=True)
    os.makedirs(nontag_path, exist_ok=True)

    prog_path = os.path.join(out_dir, 'classifiedtags')
    try:
        with open(prog_path, 'r') as prog_file:
            tags = int(prog_file.read())
    except FileNotFoundError:
        tags = 0
        with open(prog_path, 'w') as prog_file:
            prog_file.write(str(tags))

    image_size = (50, 50)
    tag_detector = tagid.TagDetector()
    cv2.namedWindow('tag detection j-tag k-nontag q-quit', cv2.WINDOW_NORMAL)
    cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
    print('{:4d} tags'.format(tags), end='')
    run_flag = True
    pre_processed_tags = 0
    while pre_processed_tags < tags:
        ret, frame = cap.read()
        if ret is False:
            break

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        tags_detected, locs, tag_imgs = tag_detector.detect(gray)
        pre_processed_tags += locs.shape[0]

    tags = pre_processed_tags

    while run_flag:
        ret, frame = cap.read()
        if ret is False:
            break

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        tags_detected, locs, tag_imgs = tag_detector.detect(gray)
        locs = locs.astype(int)

        for tag_idx in range(len(tag_imgs)):
            tag_img = tag_imgs[tag_idx]
            tag_img_resized = cv2.resize(tag_img, image_size,
                                         interpolation=cv2.INTER_AREA)
            drawimg = frame.copy()
            cv2.line(drawimg, tuple(locs[tag_idx, :2] - np.array([0, 100])),
                     tuple(locs[tag_idx, :2] + np.array([0, 100])),
                     (0, 0, 255), thickness=3)
            cv2.line(drawimg, tuple(locs[tag_idx, :2] - np.array([100, 0])),
                     tuple(locs[tag_idx, :2] + np.array([100, 0])),
                     (0, 0, 255), thickness=3)
            cv2.circle(drawimg, tuple(locs[tag_idx, :2]), locs[tag_idx, 2],
                       (255, 0, 0), thickness=3)

            cv2.imshow('tag detection j-tag k-nontag q-quit', tag_img_resized)
            cv2.imshow('frame', drawimg)

            k = cv2.waitKey(0) & 0xFF

            if k == ord('j'):
                filepath = os.path.join(tag_path, '{:04d}.png'.format(tags))
                cv2.imwrite(filepath, tag_img_resized)
                tags += 1

            elif k == ord('k'):
                filepath = os.path.join(nontag_path,
                                        '{:04d}.png'.format(tags))
                cv2.imwrite(filepath, tag_img_resized)
                tags += 1

            elif k == ord('q'):
                run_flag = False
                break

        print('\r{:4d} tags'.format(tags), end='')

    print()

    with open(prog_path, 'w') as prog_file:
        prog_file.write(str(tags))


def main():
    cap = binnedcap.MultiVideoCapture(
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-14-15-54-00-930274.h264',
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-15-14-53-50-206154.h264',
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-16-09-53-40-653421.h264',
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-17-16-53-23-895610.h264',
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-19-07-53-00-832770.h264',
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-20-07-52-48-279026.h264',
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-20-16-52-44-206258.h264',
        '/home/jesse/incubator01/2016-09-14-15-53/2016-09-21-08-52-35-457395.h264'
	)
    out_dir = '/home/jesse/honours/apisobserve/detectioncorrection'

    run_gui(cap, out_dir)

    cap.release()

if __name__ == '__main__':
    main()
