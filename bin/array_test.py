#!/usr/bin/env python

from glob import glob

from apisobserve.binnedcap import ArrayVideoCapture
from apisobserve.drawtracks import TagDrawer
from apisobserve.trackproc import FrTrackLoader


def main():
    log_files = ['/home/jessewallace/bio/honoursexperiments/incubator0{:d}/2016-09-14-15-53/out/videofile.log'.format(i) for i in [1, 2, 3, 4]]
    offsets_file = '/home/jessewallace/bio/honoursexperiments/camoffsets'
    out_str0 = '/home/jessewallace/bio/honoursexperiments/test0.mp4'
    out_str1 = '/home/jessewallace/bio/honoursexperiments/test1.mp4'
    out_str2 = '/home/jessewallace/bio/honoursexperiments/test2.mp4'
    out_str3 = '/home/jessewallace/bio/honoursexperiments/test3.mp4'

    cap = ArrayVideoCapture(offsets_file, log_files)
    track_files = sorted(glob('/home/jessewallace/bio/honoursexperiments/tracks/*.csv'))
    track_loader = FrTrackLoader(track_files, 300)

    drawer = TagDrawer(cap, track_loader, out_str0)
    for i in range(1000):
        print('\rFrame: {:d}'.format(i), end='')
        drawer.next_frame()

    print('\rFrame: {:d}'.format(i))
    drawer.release()

    drawer = TagDrawer(cap, track_loader, out_str1)
    frame_idx = 200000
    cap.set_fr_idx(frame_idx)
    for i in range(frame_idx, frame_idx + 1000):
        print('\rFrame: {:d}'.format(i), end='')
        drawer.next_frame()

    print('\rFrame: {:d}'.format(i))

    drawer.release()

    drawer = TagDrawer(cap, track_loader, out_str2)
    frame_idx = 2000000
    cap.set_fr_idx(frame_idx)
    for i in range(frame_idx, frame_idx + 1000):
        print('\rFrame: {:d}'.format(i), end='')
        drawer.next_frame()

    print('\rFrame: {:d}'.format(i))

    drawer.release()

    drawer = TagDrawer(cap, track_loader, out_str3)
    frame_idx = 3000000
    cap.set_fr_idx(frame_idx)
    for i in range(frame_idx, frame_idx + 1000):
        print('\rFrame: {:d}'.format(i), end='')
        drawer.next_frame()

    print('\rFrame: {:d}'.format(i))

    cap.release()
    drawer.release()

if __name__ == '__main__':
    main()
