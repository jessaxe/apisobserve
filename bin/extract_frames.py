#!/usr/bin/env python

import sys
import os
import numpy as np
import cv2


def load_video_log(path, directory=None):
    '''
    Loads a videofile.log. Returns video_files, frame_counts. If directory is
    specified, the directory of each video file is changed to directory
    (if the videos are stored in a different directory to what is stored in
    videofile.log)
    '''
    with open(path, 'r') as f:
        header = f.readline()
        lines = f.readlines()

    video_files = []
    frame_counts = []

    for line in lines:
        vidpath, fr_str = line.split()
        if directory is not None:
            vidpath = os.path.join(directory, os.path.basename(vidpath))

        fr_int = int(fr_str.rstrip('frames'))

        video_files.append(vidpath)
        frame_counts.append(fr_int)

    return video_files, frame_counts


def get_video_capture(video_files, frame_counts, start_frame):
    '''
    Loads appropriate video, gets to the desired frame, and returns a
    cv2.VideoCapture object.

    Args:
        video_files - list of paths to video files
        frame_counts - list of integer frame counts corresponding to
            video_files
        start_frame - desired start frame index
    '''
    assert len(video_files) == len(frame_counts)

    cumul_fr_idx = 0
    file_idx = 0
    for frame_count in frame_counts:
        cumul_fr_idx += frame_count
        if cumul_fr_idx > start_frame:
            cumul_fr_idx -= frame_count
            break
        file_idx += 1

    cap = cv2.VideoCapture(video_files[file_idx])
    while cumul_fr_idx < start_frame:
        cap.read()
        cumul_fr_idx += 1

    return cap


def extract_frames(cap, n_frames, output_dir):
    '''
    '''
    out_path = os.path.join(output_dir, '{:04d}.png')

    for i in range(n_frames):
        ret, fr = cap.read()
        if not ret:
            break

        cv2.imwrite(out_path.format(i), fr)


def main():
    logfile = sys.argv[1]
    start_frame = int(sys.argv[2])
    frame_count = int(sys.argv[3])
    out_dir = sys.argv[4]

    os.makedirs(out_dir, exist_ok=True)

    video_dir = os.path.dirname(logfile)

    print('Loading video log')
    video_files, frame_counts = load_video_log(logfile, directory=video_dir)

    print('Loading video and seeking')
    cap = get_video_capture(video_files, frame_counts, start_frame)

    print('Extracting frames')
    extract_frames(cap, frame_count, out_dir)
    cap.release()

if __name__ == '__main__':
    main()
