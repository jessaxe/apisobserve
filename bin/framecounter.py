#!/usr/bin/env python

import argparse
from .mvsegment import load_mvs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', type=str, help='''Path to motion file''')
    args = parser.parse_args()

    motion_data = load_mvs(args.path, (1232, 1640))
    print('Motion data shape: ', motion_data.shape)


if __name__ == '__main__':
    main()

