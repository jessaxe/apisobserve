#!/usr/bin/env python

import argparse
from apisobserve import tracklets
import glob
import os.path as op
import numpy as np


def build_tracklets(in_files, outfile, fr_step=300, maxstep=10,
                    timediffthresh=5, distancethresh=100, trdistthresh=30,
                    mintracklength=3, offsets=None, n_processes=1,
                    batch_size=60):
    '''
    Load detection files, and convert into single tracklets file
    '''
    i = batch_size
    maxi = len(in_files)
    while i < maxi:
        print('Loading detection files')
        if offsets is None:
            detections = tracklets.batch_load_detections(
                    in_files[i - batch_size: i], fr_step=fr_step,
                    n_processes=1)
        else:
            detections = tracklets.load_multicamera_detections(
                in_files, offsets, fr_step=fr_step, n_processes=1)

        track_array = tracklets.process_detections(
            detections, maxstep, timediffthresh, distancethresh, trdistthresh,
            mintracklength)

        i += batch_size

    print('Saving output to %s... ' % outfile, end='')
    tracklets.write_file(outfile, track_array)
    print('Done')


def get_filelists(dirs, fileglob='*0.csv'):
    '''
    Returns list of filelists to be passed to
    tracklets.load_multicamera_detections
    Args:
        dirs - directories to look in (one for each camera)
        fileglob - gob of files to load in directory
    '''
    filelist = []
    for directory in dirs:
        filelist.append(sorted(glob.glob(op.join(directory, fileglob))))

    return filelist


def load_offsets(path):
    '''
    Load offsets from a file
    Args:
        path - path to file
    Returns:
        offsets - np.ndarray
    '''
    offsets = []
    with open(path, 'r') as f:
        for line in f:
            offset = [int(s) for s in line.split()[-2:]]
            offsets.append(offset)

    return np.array(offsets)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('inputfiles', nargs='+', type=str,
                        help='''Input detection files''')
    parser.add_argument('-o', '--outputfile', type=str, required=True,
                        help='''Output tracklets file''')
    parser.add_argument('-O', '--offsetsfile', type=str, help='''Path to
                        offsets file (if this is set, then inputfiles are
                        assumed to be the tag directories of each camera''')
    parser.add_argument('-p', '--processes', default=1, type=int)

    parser.add_argument('-f', '--fr_step', default=300, type=int)
    parser.add_argument('-m', '--maxstep', default=10, type=int)
    parser.add_argument('-t', '--timediffthresh', default=5, type=int)
    parser.add_argument('-d', '--distancethresh', default=100, type=int)
    parser.add_argument('-D', '--trdistthresh', default=30, type=int)
    parser.add_argument('-l', '--mintracklength', default=3, type=int)

    return parser.parse_args()


def main():
    args = parse_args()

    if args.offsetsfile is None:
        build_tracklets(args.inputfiles, args.outputfile, args.fr_step,
                        args.maxstep, args.timediffthresh, args.distancethresh,
                        args.trdistthresh, args.mintracklength,
                        n_processes=args.processes)
    else:
        inputfiles = get_filelists(args.inputfiles)
        offsets = load_offsets(args.offsetsfile)
        build_tracklets(inputfiles, args.outputfile, args.fr_step,
                        args.maxstep, args.timediffthresh, args.distancethresh,
                        args.trdistthresh, args.mintracklength,
                        offsets=offsets, n_processes=args.processes)

if __name__ == '__main__':
    main()
