#!/usr/bin/env python

import argparse
import cv2


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('video', type=str, help='''Path to video file''')
    args = parser.parse_args()

    cap = cv2.VideoCapture(args.video)

    cv2.namedWindow('Video', cv2.WINDOW_NORMAL)

    paused = False
    while True:
        if paused is False:
            ret, frame = cap.read()
        if ret is False:
            break

        cv2.imshow('Video', frame)
        k = cv2.waitKey(1) & 0xFF
        if k == ord('q'):
            break
        elif k == ord(' '):
            paused = not paused
        elif k == ord('n'):
            ret, frame = cap.read()
            continue

    cap.release()
    cv2.destroyAllWindows()
    for i in range(5):
        k = cv2.waitKey(1) & 0xFF

if __name__ == '__main__':
    main()

