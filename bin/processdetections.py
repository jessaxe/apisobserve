#!/usr/bin/env python

import argparse
import os
import shutil
import sys

from apisobserve import tracks


def parse_args():
    parser = argparse.ArgumentParser(
        description='''Processes tag detection files to fix tag ids by
            constructing tracks.'''
        )

    io_group = parser.add_argument_group('IO Settings', '''Input/output
        arguments''')
    io_group.add_argument('-i', '--input', nargs='+', type=str,
                          help='''Input directories (one for each camera).
                          These should be given in the order that each camera
                          appears in the offsets file.''')
    io_group.add_argument('-I', '--offsets', type=str,
                          help='''Path to offsets file, containing offsets
                          for each camera. If not given, an offsets file
                          will be searched for in the common directory of
                          -i/--input.''')
    io_group.add_argument('-c', '--chunksize', type=int, default=10,
                          help='''(default=10) Sets the number of files from
                          each camera to process at a time.''')
    io_group.add_argument('-g', '--fileglob', type=str, default='*.csv',
                          help='''Unix style glob of tag files. By default,
                          "*.csv", which loads all csv files found in the
                          the top level of directories set by -i/--input.''')
    io_group.add_argument('-p', '--processes', type=int, default=1,
                          help='''Number of child processes to spawn.''')
    io_group.add_argument('-o', '--outputdir', type=str,
                          help='''Directory to store output (series of track
                          files). Content will be overwritten.''')
    io_group.add_argument('-y', action='store_true',
                          help='''Skip confirmation prompt''')

    pr_group = parser.add_argument_group('Process Settings', '''Optional
        processing parameters''')
    pr_group.add_argument('-f', '--frameinterval', type=int, default=300,
                          help='''(default=300) Number of frames each input
                          file spans.''')
    pr_group.add_argument('-t', '--maxstep', type=int, default=10,
                          help='''(default=10) Maximum gap between detections
                          in order to join them into a track (in number of
                          frames).''')
    pr_group.add_argument('-T', '--timediffthresh', type=int, default=5,
                          help='''(default=5) Maximum gap between tracks
                          in order to merge them into a single track (in
                          number of frames).''')
    pr_group.add_argument('-d', '--distthresh', type=float, default=500,
                          help='''(default=500) Maximum gap between detections
                          in order to merge them into a track (in pixels).''')
    pr_group.add_argument('-D', '--trdistthresh', type=float, default=30,
                          help='''(default=30) Maximum track distance between
                          tracks in order to merge them into a single
                          track.''')
    pr_group.add_argument('-m', '--mintracklength', type=int, default=3,
                          help='''(default=3) Tracks with fewer detections than
                          this will be removed.''')

    return parser.parse_args()


def main():
    args = parse_args()

    common_path = os.path.dirname(os.path.commonprefix(args.input))

    # Default offsets path
    if args.offsets is None:
        offsets = os.path.join(common_path, 'camoffsets')
        if os.path.isfile(offsets) is False:
            print('''Could not find offsets file in common directory.
            Please pass -I/--offsets parameter. -h/--help for more info.''')
            sys.exit()
    else:
        offsets = args.offsets

    # Default output directory
    if args.outputdir is None:
        outputdir = os.path.join(common_path, 'tracks')

        if args.y is True:
            response = 'y'
        else:
            response = input(
                'Operation will overwrite contents of %s\nProceed? (y/N) ' %
                outputdir) + 'N'

        if response[0] in 'yY':
            try:
                os.makedirs(outputdir)
            except OSError:
                shutil.rmtree(outputdir)
                os.makedirs(outputdir)
        else:
            print('Aborted')
            sys.exit()
    else:
        outputdir = args.outputdir

    consumer_kwargs = {
        'fr_step': args.frameinterval,
        'max_step': args.maxstep,
        'time_diff_thresh': args.timediffthresh,
        'distance_thresh': args.distthresh,
        'min_track_length': args.mintracklength,
        'track_dist_thresh': args.trdistthresh
        }

    tracks.process_files(args.input, outputdir, offsets, args.processes,
                         file_glob=args.fileglob, chunksize=args.chunksize,
                         consumer_kwargs=consumer_kwargs)
    print()

if __name__ == '__main__':
    main()
