#!/usr/bin/env python

import cv2
import multiprocessing as mp
import numpy as np


def child(q):
    frames = q.get()
    print('before')
    for i in range(frames.shape[0] - 1):
        print(i)
        flow = cv2.calcOpticalFlowFarneback(frames[i, ...], frames[i + 1, ...],
                                            None, 0.5, 3, 51, 1, 7, 1.5, 0)
    print('after')
    q.task_done()


def main():
    #frame0 = np.random.randint(0, high=256, size=(1232, 1640), dtype=np.uint8)
    #frame1 = np.roll(frame0, 10, axis=0)

    #cap = cv2.VideoCapture('/home/jesse/incubator01/2016-09-07-17-42/2016-09-08-05-43-39-849119.h264')
    n_frames = 10
    frames = np.empty((n_frames, 1232, 1640), dtype=np.uint8)

    for i in range(n_frames):
        #ret, frame = cap.read()
        frame = np.random.randint(0, high=256, size=(1232, 1640, 3),
                                  dtype=np.uint8)

        # cv2.cvtColor makes cv2.calcOpticalFlowFarneback hang in child process
        #grey = cv2.cvtColor(frame.copy(), cv2.COLOR_BGR2GRAY)
        grey = cv2.cvtColor(np.random.randint(0, high=256, size=(10,10,3), dtype=np.uint8), cv2.COLOR_BGR2GRAY)
        frames[i, ...] = np.mean(frame, axis=-1).astype(frame.dtype)


    #cap.release()

    q = mp.JoinableQueue()
    p = mp.Process(target=child, args=(q,))
    p.start()

    q.put(frames)
    q.join()

if __name__ == '__main__':
    main()

