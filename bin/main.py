#!/usr/bin/env python
# Track comb and adults in a video sequence

import argparse
from apisobserve import binnedcap, tagid
import datetime
import glob
import multiprocessing as mp
import os
import shutil
import sys
import time

from apisobserve.batch_frame_proc import BatchFrameProcessor


class Consumer():
    '''
    Class for processing frames in child processes
    '''
    def __init__(self, q, batch_processor):
        '''
        Args:
            q -  mp.JoinableQueue instance
            batch_processor - BatchFrameProcessor instance
        '''
        self.q = q
        self.batch_processor = batch_processor
        self.poison_pill = 'PP'
        self.run()

    def run(self):
        while True:
            msg = self.q.get()
            if msg == self.poison_pill:
                self.q.task_done()
                break
            else:
                self.batch_processor.process_frames(*msg)
                self.q.task_done()

        return None


class Producer():
    '''
    Class which reads frames and sends them to consumers
    '''
    def __init__(self, q, cap, initial_datetime,
                 time_step=datetime.timedelta(minutes=1),
                 dt_fmt='%Y-%m-%d-%H-%M-%S'):
        '''
        Args:
            q - mp.JoinableQueue instance
            cap - binnedcap.BinnedVideoCapture instance
            initial_datetime - initial datetime (for file naming)
            time_step - timedelta for each cap.read_frames call
            dt_fmt - format string for file naming
        '''
        self.q = q
        self.cap = cap
        self.dt = initial_datetime
        self.time_step = time_step
        self.dt_fmt = dt_fmt

    def run(self):
        tot_files = len(self.cap.filenames)
        t0 = time.perf_counter()
        fmt_str = '\rf:{f:4d} | F:{fr:10d} | FPS:{fps:7.1f} | ' + \
            'fph:{fph:5.2f} | rem.files:{rf:4d} | ' + \
            'est.completion:{eta:4.0f}h | ' + \
            'elapsed: {h:02d}h{m:02d}m{s:02d}s'
        eta = 0.
        while True:
            img_array = self.cap.read_frames(return_array=True)
            if self.cap.running is False:
                print()
                break

            name_str = self.dt.strftime(self.dt_fmt)
            self.q.put((img_array, name_str))
            elapsed = time.perf_counter() - t0
            fps = (self.cap.tot_fr_idx - self.cap.bin_size) / elapsed
            fph = 3600 * self.cap.file_idx / elapsed
            rf = tot_files - self.cap.file_idx
            if fph > 0:
                eta = rf / fph
            s = int(elapsed)
            m, s = s // 60, s % 60
            h, m = m // 60, m % 60
            print(fmt_str.format(
                f=self.cap.file_idx, tf=tot_files, fr=self.cap.tot_fr_idx,
                fps=fps, fph=fph, rf=rf, eta=eta, h=h, m=m, s=s), end='')
            self.dt += self.time_step

        return None


class input_directory(argparse.Action):
    '''
    Confirms that input directory is a real directory
    '''
    def __call__(self, parser, namespace, values, option_string=None):
        if os.path.isdir(values):
            setattr(namespace, 'inputdir', values)
        else:
            raise argparse.ArgumentError(self, 'Input directory not found')


class output_directory(argparse.Action):
    '''
    Checks that output directory doesn't already exist
    '''
    def __call__(self, parser, namespace, values, option_string=None):
        if values != '':
            try:
                os.makedirs(values)
            except:
                raise argparse.ArgumentError(self, '''Could not create output
                    directory. (Check that it doesn't already exist)''')
        setattr(namespace, 'outputdir', values)


def parse_args():
    parser = argparse.ArgumentParser()

    vid_group = parser.add_argument_group('Video Capture', '''Options for
        setting binning of frames, subprocesses, etc.''')

    vid_group.add_argument(
        'inputdir',
        type=str,
        action=input_directory,
        help='''Directory contatining input videos. Must contain a '.segm' file
        for each video file present. Video files should have the '.h264' file
        extension, but this can be overwritten with the -f/--filetype
        option.'''
        )

    vid_group.add_argument(
        '-o', '--outputdir',
        required=False,
        type=str,
        default='',
        action=output_directory,
        help='''Directory to store output files. By default, an out/
        subdirectory is created in inputdir. Output directory must either not
        exist, or be empty.'''
        )

    vid_group.add_argument(
        '-b', '--binlength',
        required=False,
        type=int,
        default=300,
        help='''Number of frames to read and process at once. Also determines
        the rate of reconstructed background images. Default is 300 (1min for
        5fps footage).'''
        )

    vid_group.add_argument(
        '-p', '--processes',
        required=False,
        type=int,
        default=1,
        help='''Number of sub-processes to spawn. By default, 1.'''
        )

    vid_group.add_argument(
        '-e', '--extension',
        required=False,
        type=str,
        default='.h264',
        help='''File extension for video files in inputdir. '.h264' by
        default.'''
        )

    vid_group.add_argument(
        '-y',
        action='store_true',
        help='''Skip confirmation prompt'''
        )

    learn_group = parser.add_argument_group('Tag Detection Learning')

    learn_group.add_argument('-T', '--tagstolearn', type=int, default=1000,
                             help='''Number of tags to learn detection
                             parameters from. Default 1000.''')

    return parser.parse_args()


def main():
    args = parse_args()

    # Default output directory
    if args.outputdir == '':
        args.outputdir = os.path.join(args.inputdir, 'out')

        if args.y is True:
            response = 'y'
        else:
            response = input(
                'Operation will overwrite contents of %s\nProceed? (y/N) ' %
                args.outputdir) + 'N'

        if response[0] in 'yY':
            try:
                os.makedirs(args.outputdir)
            except OSError:
                shutil.rmtree(args.outputdir)
                os.makedirs(args.outputdir)
        else:
            print('Aborted')
            sys.exit()

    print('Loading filenames')
    filenames = glob.glob(os.path.join(args.inputdir, '*' + args.extension))
    filenames.sort()

    print('Learning tag ID detection parameters')
    cap = binnedcap.MultiVideoCapture(*filenames,
                                      file_extension=args.extension)
    slope, intercept, lbp_hist = tagid.learn_scale_params(
        cap, n_tags=args.tagstolearn, n_processes=args.processes, ret_lbp=True,
        TEMP_FILE=os.path.join(args.outputdir, '_scale_learn_temp'))
    tag_detector_kwargs = {'scale_fn_params': (slope, intercept)}
    lbp_kwargs = {'tag_mean_hist': lbp_hist, 'learning_rate': 0}
    with open(os.path.join(args.outputdir, 'learnedparams.log'), 'w') as f:
        f.write(str(tag_detector_kwargs) + '\n' + str(lbp_kwargs))

    print('Starting subprocesses')
    batch_processor = BatchFrameProcessor(
        args.outputdir, tag_detector=tag_detector_kwargs,
        lbptag_detector=lbp_kwargs)
    q = mp.JoinableQueue(maxsize=1)
    processes = [mp.Process(target=Consumer, args=(q, batch_processor))
                 for i in range(args.processes)]
    for process in processes:
        process.start()

    print('Setting up output directory')
    cap = binnedcap.BinnedVideoCapture(
        *filenames, bin_size=args.binlength, file_extension=args.extension,
        log_file=os.path.join(args.outputdir, 'videofile.log'))
    initial_datetime = datetime.datetime.strptime(
        os.path.basename(filenames[0][:-len(args.extension)]),
        '%Y-%m-%d-%H-%M-%S-%f')
    print(initial_datetime.strftime(
        'Processing {n} files, starting at %Y-%m-%d-%H-%M-%S'.format(
            n=len(filenames))))

    producer = Producer(q, cap, initial_datetime)
    producer.run()

    for process in processes:
        q.put('PP')

    q.join()


if __name__ == '__main__':
    main()
